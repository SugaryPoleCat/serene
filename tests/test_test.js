const assert = require('chai').assert;
const expect = require('chai').expect;
const app = require('../commands/admin/test.js');
const dbUser = {
    balance: 42,
};
let STRING_SENT = '';
const message = {
    channel: {
        send: async (string) => {
            STRING_SENT = string;
        },
    },
    author: {
        id: '12345',
    },
};
describe('Test', async function(){
    it('Should work', async function(){
        await app.fox(message, null, null);
        expect(STRING_SENT).to.equal('42 - The amount of cubes');
    });
});