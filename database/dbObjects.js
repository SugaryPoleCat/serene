require('pg').defaults.parseInt8 = true;
const Sequelize = require('sequelize');
const botconfig = require('../config/botconfig.json');

// const database = new Sequelize('database', 'username', 'password', {
//     host: 'localhost',
//     dialect: 'sqlite',
//     logging: false,
//     storage: 'database.sqlite',
// });

try{
    const database = new Sequelize(`postgres://${botconfig.username}:${botconfig.password}@localhost/${botconfig.dataBase}?sslmode=disable`);
    // ^ FOR RELEASE

    const ServerSettings = database.import('./models/server/Settings.js');
    const ServerChannels = database.import('./models/server/Channels.js');
    const ServerModules = database.import('./models/server/Modules.js');

    const Counters = database.import('./models/Counters.js');
    const Currencies = database.import('./models/Currencies.js');
    const Dailies = database.import('./models/Dailies.js');
    const Levels = database.import('./models/Levels.js');
    const Marriage = database.import('./models/Marriage.js');
    const Relations = database.import('./models/Relations.js');
    const Warnings = database.import('./models/Warnings.js');
    const Reputation = database.import('./models/Reputation.js');
    const Reports = database.import('./models/Reports.js');
    const Sucrons = database.import('./models/Sucrons.js');
    const Work = database.import('./models/Work.js');
    const Banks = database.import('./models/Banks.js');

    const RPGCharacterProfile = database.import('./models/rpg/RPGCharacterProfile.js');

    const EmpireTreasury = database.import('./models/empire/EmpireTreasury.js');
    const EmpireResources = database.import('./models/empire/EmpireResources.js');
    const EmpireDonors = database.import('./models/empire/EmpireDonors.js');

    const UserBank = database.import('./models/users/UserBank.js');
    const UserResources = database.import('./models/users/UserResources.js');
    const UserInventory = database.import('./models/users/UserInventory.js');
    const UserCrystals = database.import('./models/users/UserCrystals.js');

    const Mining = database.import('./models/Mining.js');

    module.exports = { ServerSettings, ServerChannels, ServerModules, Counters, Currencies, Dailies, Levels, Marriage, Relations, Warnings, Reputation, Reports, Sucrons, EmpireTreasury, EmpireResources, EmpireDonors, Work, Banks, UserBank, UserResources, UserInventory, RPGCharacterProfile, Mining, UserCrystals };
}
catch(err){
    throw new Error(err);
}