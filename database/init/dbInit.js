require('pg').defaults.parseInt8 = true;
const Sequelize = require('sequelize');
const utils = require('../../utils');
const botconfig = require('../../config/botconfig.json');
const Users = require('./Users');

// const database = new Sequelize('database', 'username', 'password', {
//     host: 'localhost',
//     dialect: 'sqlite',
//     // logging: true,
//     storage: 'database.sqlite',
// });
try{
    const database = new Sequelize(`postgres://${botconfig.username}:${botconfig.password}@localhost/${botconfig.dataBase}?sslmode=disable`);
    // ^ FOR RELEASE
    database.import('../models/server/Settings.js');
    database.import('../models/server/Modules.js');
    database.import('../models/server/Channels.js');

    database.import('../models/Counters.js');
    database.import('../models/Currencies.js');
    database.import('../models/Dailies.js');
    database.import('../models/Levels.js');
    database.import('../models/Marriage.js');
    database.import('../models/Relations.js');
    database.import('../models/Warnings.js');
    database.import('../models/Reputation.js');
    database.import('../models/Reports.js');
    database.import('../models/Sucrons.js');
    database.import('../models/Work.js');
    database.import('../models/Mining.js');

    database.import('../models/rpg/RPGCharacterProfile.js');

    database.import('../models/empire/EmpireTreasury.js');
    database.import('../models/empire/EmpireResources.js');
    database.import('../models/empire/EmpireDonors.js');

    database.import('../models/users/UserBank.js');
    database.import('../models/users/UserResources.js');
    database.import('../models/users/UserInventory.js');
    database.import('../models/users/UserCrystals.js');

    const force = process.argv.includes('--force') || process.argv.includes('-f');

    database.sync({ force }).then(async () => {
        // Some code here once shop and otehr things are ready
        await utils.info('Database synced');
        database.close();
    }).catch(console.error)
    .finally(() => {
        database.close();
    });
}
catch(err){
    throw new Error(err);
}