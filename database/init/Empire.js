require('pg').defaults.parseInt8 = true;
const Sequelize = require('sequelize');
const utils = require('../../utils');
const botconfig = require('../../config/botconfig.json');

// const database = new Sequelize('database', 'username', 'password', {
//     host: 'localhost',
//     dialect: 'sqlite',
//     // logging: true,
//     storage: 'database.sqlite',
// });

const database = new Sequelize(`postgres://${botconfig.username}:${botconfig.password}@localhost/${botconfig.dataBase}?sslmode=disable`);
// ^ FOR RELEASE

const EmpireBuildings = database.import('../modles/empire/EmpireBuildings.js');
database.import('../models/empire/EmpireDonors.js');
database.import('../models/empire/EmpireResources.js');
database.import('../models/empire/EmpireTreasury.js');

const force = process.argv.includes('--force') || process.argv.includes('-f');

database.sync({ force }).then(async () => {
    const empireBuildings = [
        await EmpireBuildings.upsert({ bankID: 1, name: 'Muffin Cakes', openFee: 1500, upgradeCost: 0, interestRate: 1, security: 10 }),
        await EmpireBuildings.upsert({ bankID: 2, name: 'Jorge Trusted Bank', openFee: 4000, upgradeCost: 2000, interestRate: 2, security: 12 }),
    ];
    await Promise.all(empireBuildings);
    await utils.info('Database synced');
    database.close();
}).catch(console.error)
.finally(() => {
    database.close();
});