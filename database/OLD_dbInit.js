const Sequelize = require('sequelize');

const sequelize = new Sequelize('database', 'username', 'password', {
	host: 'localhost',
	dialect: 'sqlite',
	logging: false,
	storage: 'database.sqlite',
});

const CurrencyShop = sequelize.import('models/CurrencyShop');
sequelize.import('models/Users');
sequelize.import('models/UserItems');
// sequelize.import('mosels/UserBansWarnsReports');
sequelize.import('models/EmpireTreasury.js');
sequelize.import('models/UserFunActions.js');
// sequelize.import('models/Rules.js');
sequelize.import('models/Dailies.js');

const force = process.argv.includes('--force') || process.argv.includes('-f');

sequelize.sync({ force }).then(async () => {
	const shop = [
		await CurrencyShop.upsert({ name: 'Muffin', cost: 10 }),
		await CurrencyShop.upsert({ name: 'Cookie', cost: 25 }),
		await CurrencyShop.upsert({ name: 'Cake', cost: 50 }),
		await CurrencyShop.upsert({ name: 'Sugar\'s famous chocolate milk', cost: 100 }),
		await CurrencyShop.upsert({ name: 'Sugar plushie', cost: 500 }),
		await CurrencyShop.upsert({ name: 'Sketch plushie', cost: 500 }),
		await CurrencyShop.upsert({ name: 'Wolf plushie', cost: 500 }),
		await CurrencyShop.upsert({ name: 'Lifesized Sugar Plushie', cost: 1000 }),
	];
	await Promise.all(shop);
	console.log('Database synced');
	sequelize.close();
}).catch(console.error);
