const Sequelize = require('sequelize');

const sequelize = new Sequelize('database', 'username', 'password', {
	host: 'localhost',
	dialect: 'sqlite',
	logging: false,
	storage: 'database.sqlite',
});

// Get stuff
const Users = sequelize.import('models/Users');
const UserStanding = sequelize.import('models/UserBansWarnsReports.js');
const CurrencyShop = sequelize.import('models/CurrencyShop');
const UserItems = sequelize.import('models/UserItems');
const Dailies = sequelize.import('models/Dailies.js');
// const Rules = sequelize.import('models/Rules.js');
// const EmpireTreasury = sequelize.import('models/EmpireTreasury.js');
const UserFunActions = sequelize.import('models/UserFunActions.js');

// Get who it belongs to
UserItems.belongsTo(CurrencyShop, { foreignKey: 'item_id', as: 'item' });

// Add items
Users.prototype.addItem = async function(item) {
	const userItem = await UserItems.findOne({
		where: { user_id: this.user_id, item_id: item.id },
	});

	// Add one item.
	if (userItem) {
		userItem.amount += 1;
		return userItem.save();
	}

	return UserItems.create({ user_id: this.user_id, item_id: item.id, amount: 1 });
};

Users.prototype.getItems = function() {
	return UserItems.findAll({
		where: { user_id: this.user_id },
		include: ['item'],
	});
};

Users.prototype.addThing2 = async function addThing2(change_obj) {
    const properties = ['balance', 'reputation', 'warns', 'level', 'exp', 'maxexp', 'ship', 'reports'];
    for (const prop of properties) {
        if (change_obj.hasOwnProperty(prop)) {
            // the property is in change_obj, so let's change it accordingly!
            // Here check if its typeOfINT. Otherwise, dont add, but replace
            if(isNaN(this[prop])){
                this[prop] = change_obj[prop];
            }
            else{
                this[prop] += Number(change_obj[prop]);
            }
        }
    }
    await this.save();
};

Users.prototype.getThing = async function(what){
	return this[what];
};

// Obviously, adds the action like boop.
Users.prototype.addAction = async function(type, target){
	const [action] = await UserFunActions.findOrCreate({
		where: { user_id: this.user_id, type: type, target_id: target },
		defaults: { user_id: this.user_id, type: type, target_id: target },
	});
	action.amount++;
	await action.save();
	return action.amount;
};

// This gets how much a user has BOOPED and how much someone has been BOOPED?
Users.prototype.getAction = async function(type, target){
	if(target != undefined){
		const [action] = await UserFunActions.findOrCreate({
			where: { user_id: this.user_id, type: type, target_id: target },
			defaults: { user_id: this.user_id, type: type, target_id: target },
		});
		return action.amount;
	}
	else{
		const [action] = await UserFunActions.findOrCreate({
			where: { user_id: this.user_id, type: type },
			defaults: { user_id: this.user_id, type: type },
		});
		return action.amount;
	}
};

// Wait. what is this again?
Users.prototype.getActionTarget = async function(type){
	const rows = await UserFunActions.findAll({
		where: { target_id: this.user_id, type: type },
	});
	let sum = 0;
	for(const row of rows){
		sum += row.amount;
	}
	return sum;
};

Users.prototype.addDaily = async function(streak){
	const [dailyYay] = await Dailies.findOrCreate({
		where: { user_id: this.user_id, streak: streak },
		defaults: { user_id: this.user_id, streak: streak },
	});
	dailyYay.streak += await streak;
	await dailyYay.save();
	return dailyYay.streak;
};

// Users.prototype.addStanding = async function(){

// }

module.exports = { Users, CurrencyShop, UserItems, UserFunActions, UserStanding, Dailies };
