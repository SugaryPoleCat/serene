module.exports = (sequelize, DataTypes) => {
    return sequelize.define('levels', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        level: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
        },
        exp: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        maxExp: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 100,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID'],
        }],
        timestamps: false,
    });
};