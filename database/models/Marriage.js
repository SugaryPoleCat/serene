module.exports = (sequelize, DataTypes) => {
    return sequelize.define('marriage', {
        marriageID:{
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
        },
        userID1: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        userID2: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        date: {
            type: DataTypes.BIGINT,
            allowNull: false,
            // defaultValue: '',
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID1', 'marriageID', 'userID2'],
        }],
        timestamps: false,
    });
};