module.exports = (sequelize, DataTypes) => {
	return sequelize.define('sucrons', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        sucrons: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        eaten: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
	}, {
        indexes: [{
            unique: true,
            fields: ['userID'],
        }],
        timestamps: false,
    });
};
