module.exports = (sequelize, DataTypes) => {
    return sequelize.define('work', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        started: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        workLength: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID'],
        }],
        timestamps: false,
    });
};