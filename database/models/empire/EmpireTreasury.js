module.exports = (sequelize, DataTypes) => {
    return sequelize.define('empire_treasury', {
        empireID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        strawberrycubes: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrosium: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrons: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['empireID'],
        }],
        timestamps: false,
    });
};