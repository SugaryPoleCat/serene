module.exports = (sequelize, DataTypes) => {
    return sequelize.define('empire_donors', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        empireID: {
            // Which empire you donated to.
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        strawberrycubes: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrosium: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrons: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: false,
            fields: ['userID', 'empireID'],
        }],
        timestamps: false,
    });
};