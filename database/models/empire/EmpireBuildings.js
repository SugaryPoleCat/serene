module.exports = (sequelize, DataTypes) => {
    return sequelize.define('empire_buildings', {
        empireID: {
            // The empire ID.
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        ID: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
        },
        level: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
        },
    }, {
        indexes:[
            {
                unique: false,
                fields: ['empireID', 'ID'],
            },
        ],
        timestamps: false,
    });
};

// THE IDEA IS THAT USERS CAN CRAFT SHIT USING THIS AND SOME OTHER THINGS.
// these things ar emostly used for houses and such.