module.exports = (sequelize, DataTypes) => {
    return sequelize.define('empire_resources', {
        empireID: {
            // The empire ID.
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        stone: {
            // Used for structures.
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        wood: {
            // How much wood we have.
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
                unique: true,
                fields: ['empireID'],
            }],
        timestamps: false,
    });
};

// THE IDEA IS THAT USERS CAN CRAFT SHIT USING THIS AND SOME OTHER THINGS.
// these things ar emostly used for houses and such.