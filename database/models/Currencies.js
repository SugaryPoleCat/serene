module.exports = (sequelize, DataTypes) => {
    return sequelize.define('currencies', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        strawberrycubes: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrosium: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID'],
        }],
        timestamps: false,
    });
};