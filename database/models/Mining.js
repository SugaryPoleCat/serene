module.exports = (sequelize, DataTypes) => {
    return sequelize.define('mining', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        started: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        mineLength: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID'],
        }],
        timestamps: false,
    });
};