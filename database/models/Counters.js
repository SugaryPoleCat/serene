module.exports = (sequelize, DataTypes) => {
    return sequelize.define('counters', {
        ID: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        userID: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        thing: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        amount: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        fromID: {
            type: DataTypes.STRING,
            defaultValue: null,
            allowNull: true,
        },
    }, {
        indexes: [
            {
                unique: false,
                fields: ['userID', 'fromID', 'thing'],
            },
        ],
		timestamps: false,
	});
};