module.exports = (sequelize, DataTypes) => {
    return sequelize.define('shop_pet', {
        ID: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            defaultValue: 0,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID'],
        }],
        timestamps: false,
    });
};