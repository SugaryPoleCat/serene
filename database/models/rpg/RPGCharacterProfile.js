module.exports = (sequelize, DataTypes) => {
    return sequelize.define('rpg_character_profile', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        hp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 100,
        },
    }, {
        indexes: [
            {
                unique: false,
                fields: ['userID'],
            },
        ],
		timestamps: false,
	});
};