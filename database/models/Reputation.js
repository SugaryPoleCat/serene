module.exports = (sequelize, DataTypes) => {
    return sequelize.define('reputation', {
        userID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        amount: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        date: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID'],
        }],
        timestamps: false,
    });
};