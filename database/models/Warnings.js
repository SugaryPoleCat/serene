module.exports = (sequelize, DataTypes) => {
    return sequelize.define('warnings', {
        warningID: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        userID: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        text: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'No reason provided',
        },
        date: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        expires:{
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID', 'warningID'],
        }],
        timestamps: false,
    });
};