module.exports = (sequelize, DataTypes) => {
    return sequelize.define('guild_modules', {
        serverID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        logGuild: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        logMessages: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        logMembers: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        logRoles: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        logEmojis: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        logChannels: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        news: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        welcome: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        booster: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
        announcement: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'false',
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['serverID'],
        }],
        timestamps: false,
    });
};