module.exports = (sequelize, DataTypes) => {
    return sequelize.define('guild_channels', {
        serverID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        logGuild: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        logMessages: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        logMembers: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        logRoles: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        logEmojis: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        logChannels: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        news: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        welcome: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        booster: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        announcement: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['serverID'],
        }],
        timestamps: false,
    });
};