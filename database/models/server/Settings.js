module.exports = (sequelize, DataTypes) => {
    return sequelize.define('guild_settings', {
        serverID: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
        },
        adminRoleID: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: '',
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['serverID'],
        }],
        timestamps: false,
    });
};