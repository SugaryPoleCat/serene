// THIS just keeps track of who has a bank.
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user_bank', {
        userID: {
            type: DataTypes.STRING,
            allowNull: true,
            primaryKey: true,
        },
        bankID: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        strawberrycubes: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrosium: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        }
    }, {
        indexes: [{
            unique: true,
            fields: ['userID', 'bankID'],
        }],
        timestamps: false,
    });
};