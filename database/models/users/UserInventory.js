// This keeps track of user's inventory and what's in it.
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user_inventory', {
        ID: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },
        userID: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        thing: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        amount: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [
            {
                unique: false,
                fields: ['ID', 'userID'],
            },
        ],
        timestamps: false,
    });
};