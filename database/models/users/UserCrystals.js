// This keeps track of user's inventory and what's in it.
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user_crystals', {
        ID: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
        },
        userID: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        colour: {
            // REd, Blue, green, yellow, purple, pink
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        cut: {
            // 2 sides, 3 sidees, 4 sides, 5 sides, 6 sides, 8 sides
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 2,
        },
        shape: {
            // pointy, teardrop, triangle,  sphere, hexagon, rectangular
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        grade: {
            // chipped, flawed, regular, flawless, perfect
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        size: {
            // TIny, Small, medium, large, big, oversized, huge, gigantic
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        amount: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [
            {
                unique: false,
                fields: ['ID', 'userID'],
            },
        ],
        timestamps: false,
    });
};