module.exports = (sequelize, DataTypes) => {
    return sequelize.define('reports', {
        reportID: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        reportedID: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        reporterID:{
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '',
        },
        text:{
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'No reason provided',
        },
        date: {
            type: DataTypes.BIGINT,
            defaultValue: 0,
            allowNull: false,
        },
    }, {
        indexes: [{
            unique: false,
            fields: ['reportID', 'reportedID', 'reporterID'],
        }],
        timestamps: false,
    });
};