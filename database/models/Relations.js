module.exports = (sequelize, DataTypes) => {
    return sequelize.define('relations', {
        parentsID: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
        },
        userID: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['userID', 'parentsID'],
        }],
        timestamps: false,
    });
};