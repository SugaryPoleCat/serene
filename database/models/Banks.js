// THIS keeps track of DIFFERENT banks.
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('banks', {
        bankID: {
            // ID of the bank.
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            defaultValue: 0,
        },
        name: {
            // Name of the bank.
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'No name',
        },
        openFee: {
            // How much people pay to open a bank here.
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        upgradeCost: {
            // How much you have to pay to get a better bank.
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        interestRate: {
            // How much users get per... I havet decided yet.
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        security: {
            // How secure is it. Users attempting to steal will have harder time.
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['bankID'],
        }],
        timestamps: false,
    });
};