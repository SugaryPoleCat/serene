const settings = require('./config/settings.json');
const Discord = require('discord.js');
const botconfig = require('./config/botconfig.json');
const fs = require('fs');
const date = new Date().toUTCString();
const { Sucrons, Levels, Currencies, Reputation } = require('./database/dbObjects');

/**
 * this gets either a random color or a previously encoded color.
 *
 * @param {*} what - you can pick if you want: 'error', 'warning', 'success', 'random' - for a radnom color.
 * @returns color.
 */
function randomHexColor(what){
    let clr = '';
    const hash = '#';
    let string;
    switch(what){
        case 'error':
            clr = 'bf0000';
            break;
        case 'warning':
            clr = 'f1a91d';
            break;
        case 'success':
            clr = '0bc661';
            break;
        case 'random':
            clr = Math.floor(Math.random() * 2 ** 24).toString(16);
            break;
        default:
            clr = Math.floor(Math.random() * 2 ** 24).toString(16);
            break;
        }
    return string = hash + clr;
}

/**
 * This gets a user highlight!
 *  thanks to soru
 * @param {*} member - THIS, is the member to get
 * @param {*} fallback- IF IT FAILS, this is the fallback
 * @returns Returns, either fallback or JUST the id of the member
 */
function getUserHighlight(member, fallback) {
    if(member){
        return [`<@${member.id}>`, member.id];
    }
    return [fallback, null];
}

/**
 * This gets a highlight from a message
 *
 * @param {*} message - WHICH message to get the highlight from
 * @returns getUserHighlight, which means either the ID or a fallback
 */
function getUserHighlightFromMessage(message) {
    return getUserHighlight(message.member, message.author.username);
}

/**
 * THIS gets a mention from a string
 *
 * @param {*} guild - WHICH guild to look through for the member, to highlight
 * @param {*} str - Which string to get it from
 * @returns getUserHighlight, which means either the ID or a fallback
 */
function getUserHighlightFromStr(guild, str){
    const matches = str.match(/^\s*<@!?(\d+)>\s*$/);
    let member = null;
    if(matches){
        member = guild.members.get(matches[1]);
    }
    return getUserHighlight(member, str);
}

/**
 * This gets a phrase, actually, it processes an array based on its length
 *
 * @param {*} what - The array to process based on its length
 * @returns A random value or phrase from the array
 */
function arrayRandomIndex(arr){
    return arr[Math.floor(Math.random() * arr.length)];
}

/**
 * This takes in a string and capitalises FIRST letter of the whole string.
 *
 * @param {*} string - The string to capitalise
 * @returns The capitalised string
 */
function capitalize(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * This should be in the REQUEST and SUGGESTION and REPORT thing
 */


 // HA S OT BE REMADE
async function noClue(message, category, guild){
    let color = message.member.displayHexColor;
    if(color == '#000000') color = randomHexColor('random');
    const content = message.content.slice(9);
    const embedino = new Discord.RichEmbed()
        .setTitle(message.member.displayName)
        .setColor(color)
        .setThumbnail(message.member.displayAvatarURL);
    const logChannel = guild.channels.find(ch => ch.name === settings.channels.logGuild);
    logChannel.send(embedino.setDescription(`__They have a ${category} for us:__\n${content}\n\n**By: **${message.author.tag} with **ID: **${message.author.id}`));
    message.channel.send(embedino.setDescription(`Thank you for your ${category}! The staff will look into it.`));
}

/**
 * This compares 2 numbers between each otehr.
 * @param {*} x - The first number
 * @param {*} y - The second nunmber
 * @param {*} symbol - The symbol used to comapre them
 * @returns - The compared result
 */
function comparator(x, y, symbol){
    switch(symbol){
        case '===':
            return x === y;
        case '==':
            return x == y;
        case '<':
            return x < y;
        case '>':
            return x > y;
        case '<=':
            return x <= y;
        case '>=':
            return x >= y;
        case '!==':
            return x !== y;
        case '!=':
            return x != y;
        default:
            return false;
    }
}

/**
 * This takes in two numbers and a symbol, to do math with the numbers.
 * @param {*} x - The first number
 * @param {*} y - The second number
 * @param {*} symbol - what to do with them
 * @returns The result
 */
function mathNumbers(x, y, symbol){
    if(!isNaN(x)) x = parseFloat(x);
    if(!isNaN(y)) y = parseFloat(y);
    switch(symbol){
        case '+':
            return x + y;
        case '-':
            return x - y;
        case '*':
            return x * y;
        case '/':
            return x / y;
        default:
            return x + y;
    }
}

function mathNumbers2(x, y, symbol){
    if(!isNaN(x)) x = parseFloat(x);
    if(!isNaN(y)) y = parseFloat(y);
    return{
        '+': x + y,
        '-': x - y,
        '*': x * y,
        '/': x / y,
    }[symbol];
}

/**
 * This generates a random number.
 *
 * @param {*} min - The minimum value
 * @param {*} max - The maximum value
 * @returns - the random number
 */
function randomNumber(min, max){
    min = min ? parseInt(min, 10) : 1;
    max = max ? parseInt(max, 10) : min;

    if(max <= min){
        return min;
    }

    return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * This capitalizes a string of multiple words.
 * @param {string} str - Input string
 */
function capitalizeWords(str){
    str = str.split(' ');
    for (let i = 0; i < str.length; i++){
        str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }
    return str.join(' ');
}

/**
 * THIS creates an error embed in the catch block.
 * @param {*} message - just put message here
 * @param {*} err - just put err here
 * @param {*} title - just put help.name here
 */
async function errorEmbed(message, err, title){
    const embedino = new Discord.RichEmbed()
        .setTitle('ERROR')
        .setTimestamp()
        .setColor(randomHexColor('error'));
    const logEmbed = embedino
        .setDescription(`Something done goofed when I tried to process <@${message.author.id}> request, for: **${title}**.\nPlease poke <@${botconfig.botowners.ownerID}> about this issuse.\n\n**Issue:**\`\`\`${err}\`\`\``);
    let logChannel
    await error(err, title);
    if(message.channel.type != 'dm'){
        logChannel = message.guild.channels.find(ch => ch.name == settings.channels.logBot);
        await logChannel.send(logEmbed);
    }
    const channelEmbed = embedino
        .setDescription(`Something done goofed when I tried to process your request, <@${message.author.id}> for **${title}**.\nPlease poke <@${botconfig.botowners.ownerID}> about this issuse.`);
    return message.reply(channelEmbed);
}

/**
 * THIS creates a "were working on it" message in commands we are working on.
 * @param {*} message - just put message herer
 * @param {*} title - just put help.name here
 */
async function workingEmbed(message, title){
    return message.reply(new Discord.RichEmbed()
        .setTitle('CONSTRUCTION AHEAD')
        .setTimestamp()
        .setColor(randomHexColor('warning'))
        .setDescription(`We are working currently on ${title}, <@${message.author.id}>.\nPlease be patient.`));
}

function getNum(A){
    let outputNum = 0;
    for(let i = 0; i < A.length; i++){
        outputNum += A.charCodeAt(i);
    }
    return outputNum;
}

/**
 * THIS littlee thing, takes a name and calculates out a sum of numbers for it.
 * @param {string} name - Preferable string, really.
 */
function nameCalc(name){
    const Name = name.toLowerCase();
    const nameSum = getNum(Name);
    const finalScore = nameSum;
    return finalScore;
}

async function sucronsCalc(target){
    const [dbCurr] = await Currencies.findOrCreate({
        where: { userID: target.id.toString() },
        defaults: { userID: target.id.toString() },
    });
    const [dbLevels] = await Levels.findOrCreate({
        where: { userID: target.id.toString() },
        defaults: { userID: target.id.toString() },
    });
    const [dbRep] = await Reputation.findOrCreate({
        where: { userID: target.id.toString() },
        defaults: { userID: target.id.toString() },
    });
    const [dbSucrons] = await Sucrons.findOrCreate({
        where: { userID: target.id.toString() },
        defaults: { userID: target.id.toString() },
    });
    const results = await nameCalc(target.username) + await dbSucrons.eaten + await Math.floor((await Math.floor(dbLevels.exp / 2.7) + await (dbLevels.level * 500) + await (dbRep.amount * 200) + await (dbCurr.sucrosium * 75)) / 7.7);
    return results;
}

function writeToFile(fileName, variable){
    fs.appendFile(`logs/${fileName}.log`, variable, function(err){
        if(err) throw new Error('FS EXPERIENCED A PROBLEM!\n' + err);
    });
}

/**
 * - This logs the command categories.
 * @param {any} title - The title of category
 */
function cmdCatLoad(title){
    const cmdLog = '\n\n\n' + new Date().toUTCString() + `\n [${title.toUpperCase()} COMMANDS]`;
    console.log(cmdLog);
    writeToFile('cmdlog', cmdLog);
}

/**
 * - This logs the command files.
 * @param {any} title - The title of command
 */
function cmdLoadLog(file, cmd, aliases){
    const cmdLog = `\nLOADED: [${file.toUpperCase()}] || COMMAND: [${cmd}]\nALIASES: [${aliases}]]`;
    console.log(cmdLog);
    writeToFile('cmdlog', cmdLog);
}

/**
 * - This is a standard erroor logger, to more easily create a console.error log.
 * @param {*} Thing - The thing you wish to log, a method, or value or whatever.
 * @param {*} Title - If you want, you can put in a custom title, otherwise it will be generic.
 */
function error(Thing, Title){
    if(!Title) Title = 'AN ERROR HAS OCCURED!';
    const consoleLog = `[ERROR]: ${date}\n${Title} = ` + Thing + '\n';
    writeToFile('error', consoleLog);
    return console.error(consoleLog);
}

/**
 * - This is a standard log logger, to more easily create a console.log log.
 * @param {*} Thing - The thing you wish to log, a method, or value or whatever.
 * @param {*} Title - If you want, you can put in a custom title, otherwise it will be generic.
 */
function log(Thing, Title){
    if(!Title) Title = 'General log';
    const consoleLog = `[LOG]: ${date}\n${Title} = ` + Thing + '\n';
    writeToFile('log', consoleLog);
    return console.log(consoleLog);
}

/**
 * - This is a standard infgo logger, to more easily create a console.info log.
 * @param {*} Thing - The thing you wish to log, a method, or value or whatever.
 * @param {*} Title - If you want, you can put in a custom title, otherwise it will be generic.
 */
function info(Thing, Title){
    if(!Title) Title = 'General info';
    const consoleLog = `[INFO]: ${date}\n${Title} = ` + Thing + '\n';
    writeToFile('info', consoleLog);
    return console.info(consoleLog);
}

/**
 * - This is a standard debug logger, to more easily create a console.debug log.
 * @param {*} Thing - The thing you wish to log, a method, or value or whatever.
 * @param {*} Where - In which files.
 * @param {*} Title - If you want, you can put in a custom title, otherwise it will be generic.
 */
function debug(Thing, Where, Title){
    if(!Title) Title = 'Info or debug';
    const consoleLog = `[DEBUG]: ${date}\n${Title} = ` + Thing + '\n';
    writeToFile('debug', consoleLog);
    return console.debug(consoleLog);
}

/**
 * This creates an embed. I made this because idk, was bored i guess.
 *
 * @param {any} title - The title for the embed. You can input a title, or FALSE to have no title.
 * @param {any} color - Same as for TITLE. input FALSE, to have no title.
 * @param {any} thumbnail - THUMBNAIL! Obvioiusly, input FALSE to have no thumbnail.
 * @param {any} description - And description. Obviously, same as previoius.
 * @returns - RETURNS THE CONSTRUCTED EMBED! YAY!
 */
function embed(title, color, thumbnail, description){
    const embedino = new Discord.RichEmbed()
        .setThumbnail();
    if(title != false){
        embedino.setTitle(title);
    }
    if(color != false){
        embedino.setColor(color);
    }
    if(thumbnail != false){
        embedino.setThumbnail(thumbnail);
    }
    if(description != false){
        embedino.setDescription(description);
    }
    return embedino;
}

/**
 * - Makes random.
 * @param {number} minIn - Minimum value
 * @param {number} maxIn - Maximum value
 */
function randomiser(minIn, maxIn){
    const chanceMin = Math.ceil(minIn);
    const chanceMax = Math.floor(maxIn);
    const chance = Math.floor(Math.random() * (chanceMax - chanceMin + 1)) + chanceMin;
    return chance;
}

function memberColor(message, target){
    let color;
    if(!target) target = message.author;
    if(message.channel.type != 'dm'){
        const member = message.guild.members.find(u => u.id == target.id);
        return color = member.displayHexColor;
    }
    else return color = randomHexColor();
}

/**
 * Its not actually spy stuff. I just call it spy stuff. THis is public chatroom, thats what we're logging and commands.
 * @param {*} message - Just pass message in here.
 * @param {boolean} consoleLog - This detrermines whether to print the output to the console as well.
 * @param {boolean} turnOn - Wether this should be on or off.
 */
function spyStuff(message, consoleLog, turnOn){
    let where, sayCommand, file, spyLog;
    switch(turnOn){
        case true:
            switch(message.author.bot){
                case false:
                    switch(message.channel.type){
                        case 'text':
                            where = message.guild.name.toUpperCase() + 'with ID: ' + message.guild.id + ': #' + message.channel.name;
                            break;
                        case 'dm':
                            where = '@' + message.author.tag + ' #DM';
                            break;
                        default:
                            where = message.guild.name.toUpperCase() + ': #' + message.channel.name;
                            break;
                    }
                    switch(message.content.startsWith(botconfig.prefix)){
                        case true:
                            sayCommand = 'COMMAND EXECUTED';
                            file = 'commandsUsed';
                            break;
                        default:
                            sayCommand = 'SAID';
                            file = 'userSaid';
                            break;
                    }
                    spyLog = `\n${date}\n(${sayCommand}) IN (${where})\nBY: [@${message.author.tag}]\nCONTENT: '${message.content}'`;
                    fs.appendFile(`logs/${file}.log`, spyLog + '\n\n', function(err){
                        if(err) error(err);
                    });
                    switch(consoleLog){
                        case true:
                            console.log(spyLog + '\n');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    return;
            }
            break;
        default:
            return;
    }
}

/**
 * - GET A MENTION
 * @param {*} mention - JUST SEND INSTAD OF message.mentions.users.first() SEND INSTEAD args[0]
 * @param {*} client - Paste in client bcs idk how to solve it
 */
function userMention(mention, client){
    const match = mention.match(/^<@!?(\d+)>$/);
    if(!match) return;
    const id = match[1];
    return client.users.get(id);
}

module.exports = {
    randomHexColor,
    getUserHighlight,
    getUserHighlightFromStr,
    getUserHighlightFromMessage,
    arrayRandomIndex,
    noClue,
    capitalize,
    comparator,
    mathNumbers,
    randomNumber,
    capitalizeWords,
    mathNumbers2,
    errorEmbed,
    workingEmbed,
    cmdCatLoad,
    cmdLoadLog,
    error,
    log,
    info,
    debug,
    getNum,
    nameCalc,
    embed,
    randomiser,
    sucronsCalc,
    memberColor,
    spyStuff,
    userMention,
    writeToFile,
};