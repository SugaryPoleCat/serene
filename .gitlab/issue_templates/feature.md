## THIS IS A NEW ADDITION TO SERENE OR A NEW FEATURE PROPOSITION

**Please mark what type of feature it is between [] these marks in the issue title**.

Please use uppercase. 





(DELETE ABOVE LINES)

**SUMMARY**

> (WRITE HERE WHAT THIS FEATURE IS ABOUT SHORTLY)


**WHAT TYPE OF FEATURE IS IT**

> (TRY TO SUMMARISE WHAT TYPE IT IS. A COMMAND, A GENERAL FEATURE, LIKE A SHOP OR SOMETHING ELSE, LIKE A NEW LOG)

**WHAT SHOULD IT DO**

> (WRITE WHAT THIS FEATURE SHOULD DO)

/label ~Feature
/cc @SugaryPoleCat
/assign @Sorunome @SugaryPoleCat
