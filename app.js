// GET THE REQUIRED STUFF OUT FIRST
const fs = require('fs');
const Discord = require('discord.js');
const botconfig = require('./config/botconfig.json');
const utils = require('./utils');

// The collections
const client = new Discord.Client();

/**
 * Run the discord thing yay.
 */
async function run(){
    const plugins = {};
    try{
        const pluginFile = fs.readdirSync('./plugins').filter(file => file.endsWith('.js'));
        utils.info('Plugin folder located and read', 'Index');
        // Run through the plugins
        for(const file of pluginFile){
            const plugin = require(`./plugins/${file}`);
            if(plugin.load){
                await plugin.load(client);
                utils.info('A plugin has been loaded!', 'Index');
            }
            // Log which ones are loaded.
            utils.info(plugin.name, 'PLUGIN loaded!');
            for(const event in plugin.events){
                if(plugin.events.hasOwnProperty(event)){
                    if (!plugins[event]) {
                        plugins[event] = [];
                    }
                    plugins[event].push(plugin.events[event]);
                }
            }
        }
        for (const event in plugins) {
            if (plugins.hasOwnProperty(event)) {
                utils.info(event, 'Registered hanlder for');
                client.on(event, async function() {
                    // toss all the arguments we received into an array
                    const argsArray = Array.from(arguments);
                    for (const p of plugins[event]) {
                        try {
                            await p(client, argsArray);
                        }
                        catch(err1) {
                            console.error(`ERROR processing event ${event} ` + err1);
                            utils.error(err1, `ERROR Processing event ${event}`);
                        }
                    }
                });
            }
        }
    }
    catch(err2){
        throw new Error(err2);
    }
    client.login(botconfig.token);
}
run();

// Maybe could move process to another place?

process.on('unhandledRejection', error => utils.error(error, 'Uncaught promise rejection'));

process.on('exit', (code) => {
    client.destroy();
    console.log(`Client destroyed.\n\nExited with code: ${code}`);
});