const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const utils = require('../utils');
const settings = require('../config/settings.json');
const { Levels } = require('../database/dbObjects');

const cooldown = new Discord.Collection();

module.exports = {
    name: 'handlerXP',
    events: {
        message: async (client, [message]) => {
            try{
                const now = Date.now();
                let expirationTime, timeLeft, dbLevels, xpAmount, exp, maxExp, level, xpNeeded;
                switch(message.author.bot){
                    case 'false':
                        switch(botconfig.leveling){
                            case 'true':
                                // This is to prevent wear and tear on HDDs doing constant read/write on server.
                                if(cooldown.has(message.author.id)){
                                    expirationTime = cooldown.get(message.author.id) + botconfig.interval;
                                    console.log('timestamsp: ', cooldown.get(message.author.id));
                                    if(now < expirationTime){
                                        timeLeft = (expirationTime - now) / 1000;
                                        return console.log('time to wait: ' + timeLeft + ' for: ' + message.author.tag);
                                    }
                                }
                                cooldown.set(message.author.id, now);
                                setTimeout(() => cooldown.delete(message.author.id), botconfig.interval);
                                [dbLevels] = await Levels.findOrCreate({
                                    where: { userID: message.author.id.toString() },
                                    defaults: { userID: message.author.id.toString() },
                                });
                                xpAmount = utils.randomiser(settings.xp.min, settings.xp.max);
                                dbLevels.exp += parseInt(xpAmount);
                                await dbLevels.save();
                                exp = dbLevels.exp;
                                if(dbLevels.exp >= dbLevels.maxExp){
                                    dbLevels.maxExp = Math.floor(dbLevels.maxExp * 1.25);
                                    dbLevels.level++;
                                    dbLevels.exp = parseInt(dbLevels.exp - dbLevels.maxExp);
                                    await dbLevels.save();
                                    maxExp = dbLevels.maxExp;
                                    level = dbLevels.level;
                                    xpNeeded = maxExp - dbLevels.exp;
                                    return message.author.send(new Discord.RichEmbed()
                                        .setTitle('CONGRATULATIONS!')
                                        .setColor(utils.randomHexColor('success'))
                                        .setTimestamp()
                                        .setThumbnail(message.author.displayAvatarURL)
                                        .setDescription(`You just leveled up!\nYour level is: **${level}**!\n\nYour current experience is: **${exp}** and you will reach next level at: **${maxExp}**\nYou still need **${xpNeeded}**\n\nYou get **${settings.xp.min} - ${settings.xp.max}** experience per message.`));
                                }
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            }
            catch(err){
                return utils.errorEmbed(message, err, 'handlerCommands');
            }
        },
    },
};