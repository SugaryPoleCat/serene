const Discord = require('discord.js');
const { Currencies } = require('../database/dbObjects');
const utils = require('../utils');
const botconfig = require('../config/botconfig.json');
const settings = require('../config/settings.json');

const cooldown = new Discord.Collection();

module.exports = {
    name: 'handlerStrawberryCubes',
    events: {
        message: async (client, [message]) => {
            try{
                const now = Date.now();
                let dbCurr, expirationTime, timeLeft, strawberryChance, sucrosiumChance, sucrosiumAmount, strawberryAmount, sucrosiumPercentWin, strawberryPercentWin;
                switch(message.author.bot){
                    case 'false':
                        switch(botconfig.economy){
                            case 'true':
                                // This is to prevent wear and tear on HDDs doing constant read/write on server.
                                if(cooldown.has(message.author.id)){
                                    expirationTime = cooldown.get(message.author.id) + botconfig.interval;
                                    console.log('timestamsp: ', cooldown.get(message.author.id));
                                    if(now < expirationTime){
                                        timeLeft = (expirationTime - now) / 1000;
                                        return console.log('time to wait: ' + timeLeft + ' for: ' + message.author.tag);
                                    }
                                }
                                cooldown.set(message.author.id, now);
                                setTimeout(() => cooldown.delete(message.author.id), botconfig.interval);
                                [dbCurr] = await Currencies.findOrCreate({
                                    where: { userID: message.author.id.toString() },
                                    defaults: { userID: message.author.id.toString() },
                                });
                                strawberryChance = utils.randomiser(settings.currency.cubes.chanceMin, settings.currency.cubes.chanceMax);
                                if(strawberryChance >= settings.currency.cubes.chanceHit){
                                    strawberryAmount = utils.randomiser(settings.currency.cubes.amountMin, settings.currency.cubes.amountMax);
                                    dbCurr.strawberrycubes += strawberryAmount;
                                    await dbCurr.save();
                                }
                                sucrosiumChance = utils.randomiser(settings.currency.sucrosium.chanceMin, settings.currency.sucrosium.chanceMax);
                                if(sucrosiumChance >= settings.currency.sucrosium.chanceHit){
                                    sucrosiumAmount = utils.randomiser(settings.currency.sucrosium.amountMin, settings.currency.sucrosium.amountMax);
                                    dbCurr.sucrosium += sucrosiumAmount;
                                    await dbCurr.save();
                                    strawberryPercentWin = settings.currency.cubes.chanceMax - settings.currency.cubes.chanceHit;
                                    sucrosiumPercentWin = (settings.currency.sucrosium.chanceMax - settings.currency.sucrosium.chanceHit) / 100;
                                    return message.author.send(new Discord.RichEmbed()
                                        .setTitle('CONGRATULATIONS!')
                                        .setColor(message.member.displayHexColor)
                                        .setTimestamp()
                                        .setThumbnail(message.author.displayAvatarURL)
                                        .setDescription(`Congratulations, ${message.author}!\nYou just got **${sucrosiumAmount}** Sucrosium!\n\nIn total, you have: **${dbCurr.sucrosium}** Sucrosium!\n\nChance to win: **${sucrosiumPercentWin}%**.\nYou can win **${settings.currency.sucrosium.amountMin} - ${settings.currency.sucrosium.amountMax}** Sucrosium.\n\nPer message you gain **${settings.currency.cubes.amountMin} - ${settings.currency.cubes.amountMax}** Strawberrycubes, with a chance of **${strawberryPercentWin}%**.`));
                                }
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            }
            catch(err){
                return utils.errorEmbed(message, err, 'handlerCommands');
            }
        },
    },
};