// const Discord = require('discord.js');
// const utils = require('../utils');
// const botconfig = require('../config/botconfig.json');
// const settings = require('../config/settings.json');

// async function getEmojis(client, colour, emoji, title, description, image){
//     const Guild = client.guilds.find(g => g.id == settings.guild);
//     const Channel = Guild.channels.find(ch => ch.name == settings.channels.logGuild);
//     const embed = new Discord.RichEmbed()
//         .setTitle(`__${emoji}__ - ${title}`)
//         .setColor(utils.randomHexColor(colour))
//         .setTimestamp()
//         .setDescription(`${description}\n\n**ID: **${emoji.id}\n\n**Created At: **${emoji.createdAt}`);
//     if(image){
//         embed.setImage(image);
//     }
//     await Channel.send(embed);
// }

// function createDelete(emoji){
//     const strCreateDelete = `**Name = **${emoji.name}\n**Is it animated = **${emoji.animated}\n**Deletable = **${emoji.deletable}\n**Identifier = **${emoji.identifier}\n**Requires colons = **${emoji.requiresColons}\n**Managed by external service = **${emoji.managed}\n**URL = **${emoji.url}`;
//     return strCreateDelete;
// }

// module.exports = {
//     name: 'logEmojis',
//     events: {
//         emojiCreate: async (client, [emoji]) => {
//             await getEmojis(client, 'success', emoji, 'Has been created!', createDelete(emoji), emoji.url);
//         },
//         emojiDelete: async (client, [emoji]) => {
//             await getEmojis(client, 'error', emoji, 'Has been deleted!', createDelete(emoji), emoji.url);
//         },
//         emojiUpdate: async (client, [oldEmoji, newEmoji]) => {
//             let description = '';
//             if(newEmoji.name != oldEmoji.name){
//                 description += `**New name = **${newEmoji.name}\n**Old name = **${oldEmoji.name}\n\n`;
//             }
//             if(newEmoji.identifier != oldEmoji.identifier){
//                 description += `**New identifier = **${newEmoji.identifier}\n**Old identifier = **${oldEmoji.identifier}\n\n`;
//             }
//             await getEmojis(client, 'warning', newEmoji, 'Has been updated!', description, newEmoji.url);
//         },
//     },
// };