// GET THE REQUIRED STUFF OUT FIRST
const fs = require('fs');
const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const settings = require('../config/settings.json');
const prefix = botconfig.prefix;
const { ServerSettings } = require('../database/dbObjects');

// Custom things
const utils = require('../utils');

// DATABASE STUFF HERE

const commands = new Discord.Collection();
const cooldowns = new Discord.Collection();
const categories = {};

/**
 * - This creates an embed to be sent as an error.
 * @param {string} color - Which color to use
 * @param {*} thumbnail - Which avatar to use
 * @param {string} title - The title
 * @param {string} descritpion - The description field.
 */
function createEmbed(color, thumbnail, title, descritpion){
    const embed = new Discord.RichEmbed()
        .setTitle(title)
        .setDescription(descritpion)
        .setTimestamp()
        .setThumbnail(thumbnail)
        .setColor(utils.randomHexColor(color));
    return embed;
}

module.exports = {
    name: 'handlerCommands',
    getLoadedCommands: () => {
        return commands;
    },
    load: async () => {
        function cmdLoad(title){
            try{
                utils.cmdCatLoad(title);
                const dir = './commands/' + title;
                const cmdFile = fs.readdirSync(dir).filter(file => file.endsWith('.js'));
                for(const file of cmdFile){
                    const command = require(`.${dir}/${file}`);
                    const cmdHelp = command.help;
                    command.category = title;
                    commands.set(cmdHelp.name, command);
                    utils.cmdLoadLog(file, command.help.name, command.help.aliases);
                    // utils.log(JSON.stringify(command));
                }
            }
            catch(err){
                utils.error(err, 'Error while loading command: ');
            }
        }
        cmdLoad('botowner');
        cmdLoad('serverowner');
        cmdLoad('admin');
        cmdLoad('utilities');
        cmdLoad('profile');
        cmdLoad('rpg');
        cmdLoad('fun');
        cmdLoad('converters');
        cmdLoad('nsfw');
        cmdLoad('empire');
        cmdLoad('banks');
        cmdLoad('sucrosia');
        // I want to print out the amount of commands in each category and the total amount of commands.
        commands.forEach((command) => {
            if(!categories[command.category]) categories[command.category] = [];
            categories[command.category].push(command.help.name);
        });
        for(const category in categories){
            if(categories.hasOwnProperty(category)){
                const commandList = categories[category];
                utils.log(commandList.values(), category.toUpperCase());
            }
        }
        utils.log(JSON.stringify(commands));
    },
    events: {
        message: async (client, [message]) => {
            try{
                // utils.log(JSON.stringify(commands));
                let dbSettings;
                if(message.channel.type != 'dm') dbSettings = await ServerSettings.findOne({
                    where: { serverID: message.guild.id.toString() },
                });
                const Author = message.author;
                const AuthorID = Author.id.toString();
                const Member = message.member;
                // OwO Shhh dont look in here.
                utils.spyStuff(message, true, true);

                if(!message.content.startsWith(prefix)) return;
                // ^ THATS A TEMPORARY FIX UNTIL I FIGURE OUT WHAT HAPPENED
                // if(!message.content.startsWith(prefix) || (message.author.bot && message.author.id != settings.soruBOT)) return;

                // const args = message.content.slice(/(! ?)/).split(/ +/);
                // This has to be remade, i want to be able ot have a space between prefix and command.
                const args = message.content.slice(botconfig.prefix.length).split(/ +/);
                console.log(args);
                const commandName = args.shift().toLowerCase();
                const command = commands.get(commandName) || commands.find(cmd => cmd.help.aliases && cmd.help.aliases.includes(commandName));
                // const command = commands.find(cmd => cmd.help.name && cmd.help.name.includes(commandName)) || commands.find(cmd => cmd.help.aliases && cmd.help.aliases.includes(commandName));

                // It will also delete the message, if the message is marked with delete.
                let setAuthor = '';
                let setColor = '';
                let setAvatar = '';
                // FOR TESTING SET IT TO FALSE IN SETTINGS FILE
                if(botconfig.deleteMsg == 'true'){
                    if(message.channel.type === 'text'){
                        // If we cant find message.member
                        if(!message.member){
                            setAuthor = Author.tag;
                            setColor = utils.randomHexColor();
                        }
                        // But if we doooo
                        else{
                            setAuthor = Member.displayName;
                            setColor = Member.displayHexColor;
                        }
                        setAvatar = Author.displayAvatarURL;
                        message.delete();
                    }
                }

                // Checks if command exists
                if(!command){
                    return message.reply(createEmbed('warning', setAvatar, 'Command requested does not exist.', `Command: \`${commandName}\` does not exist, ${message.member.displayName}.\n\nI am happy to assist you in finding the right command. If you so... desire...`));
                }
                const cmdHelp = command.help;
                const cmdConfig = command.config;

                // This checks if the config has GUILDONLY set to true AND if the channel is in the server.
                if(cmdConfig.guildOnly && message.channel.type !== 'text'){
                    return message.reply(createEmbed('error', setAvatar, 'Wrong place!', 'I can not do this here. Please use it in the server.'));
                }

                // Check if its the owner typing
                // MAKE A CHECK IF ITS ALSO THE COOWNER! CAUSE I FAILED LAST TIME
                if(cmdConfig.botowner){
                    if(!Object.values(botconfig.botowners).includes(message.author.id)){
                        return message.reply(createEmbed('error', setAvatar, 'You do not rule me.', `I am terribly sorry ${message.author}, but this command can only be used by <@${botconfig.botowners.ownerID}>, <@${botconfig.botowners.coownerID}> and <@${botconfig.botowners.soruBOT}>.`));
                    }
                }

                if(cmdConfig.owner){
                    if(message.author.id != message.guild.ownerID && !Object.values(botconfig.botowners).includes(message.author.id)) return message.reply(createEmbed('error', setAvatar, 'You are not server owner.', 'I am terribly sorry, but this command can only be used by the server owner.'));
                }

                // Check if user has right permissions (could be made better somehow?)
                if(cmdConfig.admin){
                    const role = message.member.roles.find(r => r.id == dbSettings.adminRoleID);
                    if(role != true && !Object.values(botconfig.botowners).includes(message.author.id)) return message.reply(createEmbed('error', setAvatar, 'You lack the required \'ADMIN\' persmission.', `You lack the required **ADMIN** role to request this command, ${message.author}`));
                }

                // Check if user is a royal scribe
                // Dont think we need scribe? 
                // I mean, if serene becomes spread, this is useless.
                // if(cmdConfig.scribe){
                //     const role = message.member.roles.some(r => r.name == 'STAFF');
                //     if(role != true && !Object.values(botconfig.botowners).includes(message.author.id)) return message.reply(createEmbed('error', setAvatar, 'You lack required \'Royal Scribe\' permission.', `You lack the required **Royal Scribe** role, ${message.author}.`));
                // }

                // Check if the message is in NSFW channel and if the command is NSFW
                if(cmdConfig.nsfw && message.channel.nsfw == false){
                    return message.reply(createEmbed('warning', setAvatar, 'You are being TOO naughty.', `Please use this command again in **NSFW** channel, ${message.author}.`));
                }

                // Check if arguments are provided if a command is marked with ARGS
                if(cmdConfig.args && !args.length){
                    let reply = `You did not provide enough arguments for me, ${message.author}.`;
                    if(cmdHelp.usage){
                        reply += `\n\nThe proper usage would be: \`${prefix}${cmdHelp.name} ${cmdHelp.usage}\`.\nAnd remember that: \`< >\` is required field, \`[ ]\` is optional field and \` | \` marks or.\nMore information can be found in \`!help\`.`;
                    }
                    return message.reply(createEmbed('warning', setAvatar, 'Not enough arguments.', reply));
                }

                // If no cooldown is provided in the command file, make your own.
                // However this is not working i think?
                if(!Object.values(botconfig.botowners).includes(message.author.id)){
                    if(!cooldowns.has(cmdHelp.name)){
                        cooldowns.set(cmdHelp.name, new Discord.Collection());
                    }
                    const now = Date.now();
                    const timestamps = cooldowns.get(cmdHelp.name);
                    const cooldownAmount = (cmdConfig.cooldown || 3) * 1000;
                    if(timestamps.has(AuthorID)){
                        const expirationTime = timestamps.get(AuthorID) + cooldownAmount;
                        if(now < expirationTime){
                            const timeLeft = (expirationTime - now) / 1000;
                            await message.reply(createEmbed('warning', message.author.displayAvatarURL, 'Woah, slow down there!', `You are going too fast **${message.author}**!\n\nPlease wait ${timeLeft.toFixed(1)} more second(s) before asking me to do it again!`));
                            return;
                        }
                    }
                    timestamps.set(AuthorID, now);
                    setTimeout(() => timestamps.delete(AuthorID), cooldownAmount);
                }

                // So this will display an embed in the log channel about who and what sent.
                // Moves this to teh logMessages
                // const msgLogging = false;
                // if(msgLogging == true){
                //     if(message.channel != undefined){
                //         const msgLog = message.guild.channels.find(ch => ch.name == settings.channels.logMessages);
                //         const embedLog = new Discord.RichEmbed()
                //             .setAuthor(setAuthor)
                //             .setColor(setColor)
                //             .setTitle('Used a command')
                //             .setDescription(message.content)
                //             .addField('Author ID', AuthorID, true)
                //             .addField('Message ID', message.id, true)
                //             .setTimestamp();
                //         msgLog.send(embedLog);
                //     }
                // }

                // Finally call the command requested.
                return command.fox(message, args, client);
            }
            catch(err){
                await console.error(err);
                return utils.errorEmbed(message, err, 'handlerCommands');
            }
        },
    },
};
