// const Discord = require('discord.js');
// const utils = require('../utils');
// const botconfig = require('../config/botconfig.json');
// const settings = require('../config/settings.json');

// async function getUsers(client, colour, user, title, description){
//     const Guild = client.guilds.find(g => g.id == settings.guild);
//     const Channel = Guild.channels.find(ch => ch.name === settings.channels.logUsers);
//     const embed = new Discord.RichEmbed()
//         .setThumbnail(user.displayAvatarURL)
//         .setColor(utils.randomHexColor(colour))
//         .setTitle(`__${user.tag}__ - ${title}`)
//         .setTimestamp()
//         .setDescription(`${description}\n\n**ID: **${user.id}\n\n**Created at: **${user.createdAt}`);
//     await Channel.send(embed);
// }

// module.exports = {
//     name: 'logUsers',
//     events: {
//         // Takes CLINET and ARRAY OF THIGS to get from
//         userUpdate: async (client, [oldUser, newUser]) => {
//             let description = '';
//             if(newUser.username != oldUser.username){
//                 description += `**New username = **${newUser.discriminator}\n**Old username = **${oldUser.username}\n\n`;
//             }

//             if(newUser.discriminator != oldUser.discriminator){
//                 description += `**New discriminator = **${newUser.discriminator}\n**Old discriminator = **${oldUser.discriminator}\n\n`;
//             }

//             await getUsers(client, 'warning', newUser, 'Has been updated', description);
//         },
//         guildBanAdd: async (client, [guild, user]) => {
//             await getUsers(client, 'error', user, 'Has been banned!', 'Oh no');
//         },
//         guildBanRemove: async (client, [guild, user]) => {
//             await getUsers(client, 'success', user, 'Has been unbanned!', 'Oh!');
//         },
//     },
// };