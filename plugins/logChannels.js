const Discord = require('discord.js');
const utils = require('../utils');
const botconfig = require('../config/botconfig.json');
const { ServerModules, ServerChannels } = require('../database/dbObjects');

// async function getChannels(client, colour, channel, title, description){
//     const Guild = client.guilds.find(g => g.id == settings.guild);
//     const Channel = Guild.channels.find(ch => ch.id == settings.channels.logGuild);
//     const embed = new Discord.RichEmbed()
//         .setTitle(`__${channel}__ - ${title}`)
//         .setColor(utils.randomHexColor(colour))
//         .setTimestamp()
//         .setDescription(`${description}\n\n**Channel type = **${channel.type}\n\n**Channel ID: **${channel.id}\n\n**Created at: **${channel.createdAt}`);
//     await Channel.send(embed);
// }

module.exports = {
    name: 'logChannels',
    events: {
        channelCreate: async (client, [channel]) => {
            try{
                // Check module first
                // if no module, return without doing anything.
                // check channel then
                // if no channel, use general log
                // if no general log, exit without doing anything
                let dbModule, dbChannels, logChannel, Guild, description, title;
                if(channel.type != 'category' && channel.type != 'dm') {
                    if(channel.parent != null) console.log(`GUILD = '${channel.guild.name}' with ID: ${channel.guild.id} has created a new channel\n\n${channel.name} = name\n${channel.parent.name} = parent\n${channel.id} = ID\n${channel.type} = Type`);
                    else console.log(`GUILD = '${channel.guild.name}' with ID: ${channel.guild.id} has created a new channel\n\n${channel.name} = name\n${channel.id} = ID\n${channel.type} = Type`);
                }
                else if(channel.type != 'dm') console.log(`GUILD = '${channel.guild.name}' with ID: ${channel.guild.id} has created a new category\n\n${channel.name} = name\n${channel.id} = ID\n${channel.type} = Type`);
                switch(channel.type){
                    case 'dm':
                        return;
                    default:
                        dbModule = await ServerModules.findOne({
                            where: { serverID: channel.guild.id.toString() },
                        });
                        switch(dbModule.logChannels){
                            case 'true':
                                dbChannels = await ServerChannels.findOne({
                                    where: { serverID: channel.guild.id.toString() },
                                });
                                Guild = client.guilds.find(g => g.id == dbChannels.serverID);
                                if(dbChannels.logChannels == '' && dbChannels.logGuild != '') logChannel = Guild.channels.find(ch => ch.id == dbChannels.logGuild);
                                else if(dbChannels.logChannels == '' && dbChannels.logGuild == '') return;
                                else if(dbChannels.logChannels != '') logChannel = Guild.channels.find(ch => ch.id == dbChannels.logChannels);
                                description = '';
                                if(channel.type != 'category' && channel.type != 'dm'){
                                    if(channel.parent != null) description += `\n**Parent**: ${channel.parent.name}`;
                                    title = 'CHANNEL';
                                }
                                else if(channel.type == 'category' && channel.type != 'dm') title = 'CATEGORY';
                                return logChannel.send(new Discord.RichEmbed()
                                    .setTitle(`${title} CREATED`)
                                    .setTimestamp()
                                    .setColor(utils.randomHexColor('success'))
                                    .setDescription(`**Name**: ${channel.name}
                                    **ID**: ${channel.id}
                                    **Type**: ${channel.type}${description}`));
                            default:
                                return;
                        }
                }
            }
            catch(err){
                return utils.error(err);
            }
        },
        channelDelete: async (client, [channel]) => {
            try{
                let dbModule, dbChannels, logChannel, Guild, description, title;
                if(channel.type != 'category' && channel.type != 'dm') {
                    if(channel.parent != null) console.log(`GUILD = '${channel.guild.name}' with ID: ${channel.guild.id} has deleted a new channel\n\n${channel.name} = name\n${channel.parent.name} = parent\n${channel.id} = ID\n${channel.type} = Type`);
                    else console.log(`GUILD = '${channel.guild.name}' with ID: ${channel.guild.id} has deleted a new channel\n\n${channel.name} = name\n${channel.id} = ID\n${channel.type} = Type`);
                }
                else if(channel.type != 'dm') console.log(`GUILD = '${channel.guild.name}' with ID: ${channel.guild.id} has deleted a category\n\n${channel.name} = name\n${channel.id} = ID\n${channel.type} = Type`);
                switch(channel.type){
                    case 'dm':
                        return;
                    default:
                        dbModule = await ServerModules.findOne({
                            where: { serverID: channel.guild.id.toString() },
                        });
                        switch(dbModule.logChannels){
                            case 'true':
                                dbChannels = await ServerChannels.findOne({
                                    where: { serverID: channel.guild.id.toString() },
                                });
                                Guild = client.guilds.find(g => g.id == dbChannels.serverID);
                                if(dbChannels.logChannels == '' && dbChannels.logGuild != '') logChannel = Guild.channels.find(ch => ch.id == dbChannels.logGuild);
                                else if(dbChannels.logChannels == '' && dbChannels.logGuild == '') return;
                                else if(dbChannels.logChannels != '') logChannel = Guild.channels.find(ch => ch.id == dbChannels.logChannels);
                                description = '';
                                if(channel.type != 'category' && channel.type != 'dm'){
                                    if(channel.parent != null) description += `\n**Parent**: ${channel.parent.name}`;
                                    title = 'CHANNEL';
                                }
                                else if(channel.type == 'category' && channel.type != 'dm') title = 'CATEGORY';
                                return logChannel.send(new Discord.RichEmbed()
                                    .setTitle(`${title} DELETED`)
                                    .setTimestamp()
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription(`**Name**: ${channel.name}
                                    **ID**: ${channel.id}
                                    **Type**: ${channel.type}${description}`));
                            default:
                                return;
                        }
                }
            }
            catch(err){
                return utils.error(err);
            }
        },
        // channelUpdate: async (client, [oldChannel, newChannel]) => {
        //     // let description = '';
        //     // if(oldChannel != newChannel){
        //     //     description += `**New name = **${newChannel}\n**Old name = **${oldChannel}\n\n`;
        //     // }
        //     // await getChannels(client, 'warning', oldChannel, 'Has been updated!', description);
        //     // try{
                
        //     // }
        //     // catch(err){
        //     //     return utils.error(err);
        //     // }
        // },
    },
};