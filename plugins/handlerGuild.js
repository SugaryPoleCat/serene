const Discord = require('discord.js');
const utils = require('../utils');
const botconfig = require('../config/botconfig.json');
const settings = require('../config/settings.json');
const { ServerSettings, ServerModules, ServerChannels } = require('../database/dbObjects');

module.exports = {
    name: 'handlerGuild',
    events: {
        // This is when we join a server
        guildCreate: async (client, [guild]) => {
            try{
                await ServerSettings.findOrCreate({
                    where: { serverID: guild.id.toString() },
                    defaults: { serverID: guild.id.toString() },
                });
                await ServerChannels.findOrCreate({
                    where: { serverID: guild.id.toString() },
                    defaults: { serverID: guild.id.toString() },
                });
                await ServerModules.findOrCreate({
                    where: { serverID: guild.id.toString() },
                    defaults: { serverID: guild.id.toString() },
                });
                const consoleLog = `Joined the server '${guild.name}' with ID: ${guild.id}`;
                utils.writeToFile('servers', consoleLog);
                const owner = client.users.find(u => u.id == guild.ownerID);
                owner.send(new Discord.RichEmbed()
                    .setTimestamp()
                    .setTitle('Thanks for inviting me!')
                    .setColor(utils.randomHexColor())
                    .setThumbnail(client.user.displayAvatarURL)
                    .setDescription('It is great pleasure to be working on your server.\n\nDo not forget to use `!adminchannels` to set channels for logs and announcements, `!adminmodules` to set which modules I can use and `!adminsettings` to set who can use my admin commands.\n\nDo not forget to check help either! If you need more explanation on how to use the commands I mentioned, use `!help adminmodules`, or any of the commands I mentioned and my help will tell you how to use them. Yes, you can use help in my DMs, just like many of my commands. The help will tell you which ones can be used in DMs.'));
                return console.log(consoleLog);
            }
            catch(err){
                return console.error(err);
            }
        },
        // This is when we leave a server
        guildDelete: async (client, [guild]) => {
            try{
                // This is only if we want the sever's settings gone.
                // await ServerSettings.destroy({
                //     where: { serverID: guild.id.toString() },
                // });
                const consoleLog = `Left the server '${guild.name}' with ID: ${guild.id}`;
                utils.writeToFile('servers', consoleLog);
                return console.log(consoleLog);
            }
            catch(err){
                return console.error(err);
            }
        },
    },
};