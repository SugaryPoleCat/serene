const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const package = require('../package.json');
const utils = require('../utils');
const settings = require('../config/settings.json');
const banks = require('../config/banks.json');

module.exports = {
    name: 'handlerReady',
    events: {
        ready: async (client) => {
            client.user.setPresence({ game:{ name: `people using ${botconfig.prefix} prefix`, type: 'WATCHING' }, status: 'online' });
            // await client.user.setStatus('online');
            // await client.user.setActivity(`people using ${botconfig.prefix} prefix!`, { type: 'WATCHING' });
            await console.log('=========\nREADY\n=========');
        },
        reconnecting: async (client) => {
            client.user.setPresence({ game:{ name: 'Reconnecting...', type: 'LISTENING' }, status: 'dnd' });
        },
    },
};