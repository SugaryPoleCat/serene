// const Discord = require('discord.js');
// const utils = require('../utils');
// const botconfig = require('../config/botconfig.json');
// const settings = require('../config/settings.json');

// async function getRoles(client, colour, role, title, description){
//     const Guild = client.guilds.find(g => g.id == settings.guild);
//     const Channel = Guild.channels.find(ch => ch.name == settings.channels.logGuild);
//     const embed = new Discord.RichEmbed()
//         .setTitle(`__${role.name}__ - ${title}`)
//         .setColor(colour)
//         .setTimestamp()
//         .setDescription(`${description}\n\n**ID: **${role.id}\n\n**Created at: **${role.createdAt}`);
//     await Channel.send(embed);
// }

// function createDelete(role){
//     const strCreateDelete = `**Name = **${role.name}\n**Colour = **${role.hexColor}\n**Mentionable = **${role.mentionable}\n**Permissions = **${role.permissions}\n**Users appear seperately = **${role.hoist}\n**Managed by external service = **${role.managed}\n**Editable = **${role.editable}`;
//     return strCreateDelete;
// }

// module.exports = {
//     name: 'logRoles',
//     events: {
//         roleCreate: async (client, [role]) => {
//             await getRoles(client, utils.randomHexColor('success'), role, 'Has been created!', createDelete(role));
//         },
//         roleDelete: async (client, [role]) => {
//             await getRoles(client, utils.randomHexColor('error'), role, 'Has been deleted!', createDelete(role));
//         },
//         roleUpdate: async (client, [oldRole, newRole]) => {
//             const Guild = client.guilds.find(g => g.id == settings.guild);
//             const Everyone = Guild.roles.get('549955776991461408');
//             let description = null;
//             if(!Everyone){
//                 if(newRole.hoist != oldRole.hoist){
//                     console.log('hoist');
//                     description += `**Do users appear separately now = **${newRole.hoist}\n**Did they used to appear seperately = **${oldRole.hoist}\n\n`;
//                 }
//                 if(newRole.hexColor != oldRole.hexColor){
//                     console.log('color');
//                     description += `**New colour = **${newRole.hexColor}\n**Old colour = **${oldRole.hexColor}\n\n`;
//                 }
//                 if(newRole.name != oldRole.name){
//                     console.log('name');
//                     description += `**New name = **${newRole.name}\n**Old name = **${oldRole.name}\n\n`;
//                 }
//                 // if(newRole.permissions != oldRole.permissions){
//                 //     console.log('permissions');
//                 //     description += `**New permissions = **${newRole.permissions}\n**Old permissions = **${oldRole.permissions}\n\n`;
//                 // }
//                 await getRoles(client, newRole.hexColor, newRole, 'Has been updated!', description);
//             }
//         },
//     },
// };