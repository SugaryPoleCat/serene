// const Discord = require('discord.js');
// const utils = require('../utils');
// const botconfig = require('../config/botconfig.json');
// const settings = require('../config/settings.json');

// async function getGuild(guild, color, title, description, image){
//     const Channel = guild.channels.find(ch => ch.name == settings.channels.logGuild);
//     const embed = new Discord.RichEmbed().setTimestamp()
//         .setColor(color)
//         .setTitle(`__${guild}__ - ${title}`)
//         .setDescription(`${description}\n\n**Created at: **${guild.createdAt}\n\n**ID: **${guild.id}`);
//     if(image){
//         embed.setImage(image);
//         await Channel.send(embed);
//     }
//     else{
//         await Channel.send(embed);
//     }
// }

// module.exports = {
//     name: 'logGuild',
//     events: {
//         // Takes CLINET and ARRAY OF THIGS to get from+
//         guildUpdate: async (client, [oldGuild, newGuild]) => {
//             let description = '';
//             if(newGuild.ownerID != oldGuild.ownerID){
//                 description += `**New owner = **${newGuild.owner.user.tag}\n**Old owner = **${oldGuild.owner.user.tag}\n\n`;
//             }
//             if(newGuild.name != oldGuild.name){
//                 description += `**New name = **${newGuild.name}\n**Old name = **${oldGuild.name}\n\n`;
//             }
//             if(newGuild.nameAcronym != oldGuild.nameAcronym){
//                 description += `**New acronym = **${newGuild.nameAcronym}\n**Old acronym = **${oldGuild.nameAcronym}\n\n`;
//             }
//             if(newGuild.iconURL != oldGuild.iconURL){
//                 description += `**New icon = **${newGuild.iconURL}\n**Old icon = **${oldGuild.iconURL}\n\n`;
//             }
//             if(newGuild.region != oldGuild.region){
//                 description += `**New region = **${newGuild.region}\n**Old region = **${oldGuild.region}\n\n`;
//             }
//             // The fuck is a system channel
//             if(newGuild.systemChannelID != oldGuild.systemChannelID){
//                 description += `**New system channel = **${newGuild.systemChannel}\n**Old sytem channel = **${oldGuild.systemChannel}\n\n`;
//             }
//             if(newGuild.afkChannelID != oldGuild.afkChannelID){
//                 description += `**New AFK channel = **${newGuild.afkChannel}\n**Old AFK channel = **${oldGuild.afkChannel}\n\n`;
//             }
//             if(newGuild.afkTimeout != oldGuild.afkTimeout){
//                 description += `**New AFK timeout = **${newGuild.afkTimeout}\n**Old AFK timeout = **${oldGuild.afkTimeout}\n\n`;
//             }
//             if(newGuild.memberCount != oldGuild.memberCount){
//                 description += `**New member count = **${newGuild.memberCount}\n**Old member count = **${oldGuild.memberCount}\n\n`;
//             }
//             if(newGuild.mfaLevel != oldGuild.mfaLevel){
//                 description += `**New MFA level = **${newGuild.mfaLevel}\n**Old MFA level = **${oldGuild.mfaLevel}\n\n`;
//             }
//             if(newGuild.explicitContentFilter != oldGuild.regexplicitContentFilterion){
//                 description += `**New explicit content level = **${newGuild.explicitContentFilter}\n**Old explicit content level = **${oldGuild.explicitContentFilter}\n\n`;
//             }
//             if(newGuild.large != oldGuild.large){
//                 description += `**New more than 250 members = **${newGuild.large}\n**Old more than 250 members = **${oldGuild.large}\n\n`;
//             }
//             if(newGuild.embedEnabled != oldGuild.embedEnabled){
//                 description += `**New embed enalbed = **${newGuild.embedEnabled}\n**Old embed enabled = **${oldGuild.embedEnabled}\n\n`;
//             }
//             // Roles is a collection, presences is a collection, channels is a colletcion, features is a collection, it would be great to get some sort of thing from them to know WHAT is changing exactly
//             await getGuild(newGuild, 'warning', 'Has changed!', description, newGuild.iconURL);
//         },
//         rateLimit: async (client, [rateLimitInfo]) => {
//             console.log('\nREQUEST LIMIT\nrateLimitInfo.limit: ' + rateLimitInfo.limit + '\nrateLimitInfo.timeDifferece: ' + rateLimitInfo.timeDifference + '\nrateLimitInfo.path: ' + rateLimitInfo.path + '\nrateLimitInfo.method: ' + rateLimitInfo.method);
//         },
//     },
// };