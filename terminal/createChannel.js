const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const utils = require('../utils');
const settings = require('../config/settings.json');

async function run(){
    const [ , , channelName] = await process.argv;
    try{
        const client = await new Discord.Client();
        utils.info('Client established');
        await client.login(botconfig.token).then(utils.info('Client logged in'));
        const Guild = await client.guilds.find(g => g.id == settings.guild);
        const category = await Guild.channels.find(ch => ch.name == 'nigg');
        utils.info(category.name, 'Category found');
        await Guild.createChannel(channelName, 'text').then(utils.info(channelName, 'Channel created'));
        const createdChannel = await Guild.channels.find(ch => ch.name == channelName);
        utils.info(createdChannel.name, 'Created channel found');
        await createdChannel.setParent(category).then(utils.info(category.name, 'Parent category set'));
        await createdChannel.lockPermissions().then(utils.info(`Synchronised permissions: '${createdChannel.name}' with category: '${category.name}'`, 'Synchronised permissions'));
        await client.destroy().then(utils.info('Client destroyed'));
    }
    catch(err){
        utils.error(err);
    }
}
run();