const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const settings = require('../config/settings.json');
const utils = require('../utils');

async function run(){
    try{
        const client = await new Discord.Client();
    }
    catch(err){
        return utils.error(err);
    }
}
run();