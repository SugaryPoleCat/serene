const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const settings = require('../config/settings.json');
const utils = require('../utils');
const Canvas = require('canvas');
const rules = require('../config/rules.json');


async function run(){
    const client = await new Discord.Client();
    await client.login(botconfig.token);
    const Guild = await client.guilds.find(g => g.id == settings.guild);

    const log = Guild.channels.find(ch => ch.name == settings.botLog);
    const staff = Guild.roles.find(r => r.id == settings.staffID);
    const Member = Guild.members.find(u => u.id == client.user.id);
    let culur = Member.displayHexColor;
    if(culur == '#000000'){
        culur = utils.randomHexColor('random');
    }
    const logEmbed = new Discord.RichEmbed()
        .setColor(culur)
        .setThumbnail(client.user.displayAvatarURL)
        .setDescription(`Have been updated.\n${staff}, please check them out.`)
        .setTimestamp();
    let Channel = null;
    /**
     * This gets the channel and if amount is specified, deletes the amount of messages prior.
     *
     * @param {*} title - The title of the channel.
     * @param {*} amount - The amount of messages to delete (OPTIONAL)
     * @returns {*} Channel - Channel
     */
    async function getChannel(title, amount){
        Channel = await Guild.channels.find(ch => ch.name == title);
        // So if you leave the amount empty it just looks for a channel without deleting anything previously.
        if(amount != null || amount != undefined){
            await Channel.bulkDelete(amount);
        }
        return Channel;
    }

    async function muteChannel(title){
        Channel = await Guild.channels.find(ch => ch.name == title);
        await Channel.overwritePermissions(Channel.guild.defaultRole, { VIEW_CHANNEL: false });
    }
    async function unMuteChannel(title){
        Channel = await Guild.channels.find(ch => ch.name == title);
        await Channel.overwritePermissions(Channel.guild.defaultRole, { VIEW_CHANNEL: true });
    }

    const embed = new Discord.RichEmbed();
    async function postChannel(title, color, description){
        const canvas = Canvas.createCanvas(600, 150);
        const ctx = canvas.getContext('2d');
        ctx.fillStyle = color;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.beginPath();
        ctx.lineTo(40, 100);
        ctx.lineTo(5, 70);
        ctx.lineTo(20, 50);
        ctx.lineTo(40, 60);
        ctx.lineTo(60, 50);
        ctx.lineTo(75, 70);
        ctx.closePath();
        ctx.strokeStyle = '#fff';
        ctx.lineWidth = 3;
        ctx.stroke();

        ctx.moveTo(40, 100);
        ctx.lineTo(40, 60);
        ctx.lineTo(75, 70);
        // ctx.lineWidth = 2;
        ctx.stroke();

        ctx.lineTo(40, 60);
        ctx.lineTo(5, 70);
        ctx.stroke();

        ctx.beginPath();
        ctx.lineTo(560, 100);
        ctx.lineTo(595, 70);
        ctx.lineTo(580, 50);
        ctx.lineTo(560, 60);
        ctx.lineTo(540, 50);
        ctx.lineTo(525, 70);
        ctx.closePath();
        ctx.stroke();

        ctx.moveTo(560, 100);
        ctx.lineTo(560, 60);
        ctx.lineTo(525, 70);
        // ctx.lineWidth = 2;
        ctx.stroke();

        ctx.lineTo(560, 60);
        ctx.lineTo(595, 70);
        ctx.stroke();

        ctx.font = '42px sans-serif';
        ctx.fillStyle = '#fff';
        ctx.textAlign = 'center';
        ctx.fillText(title, 300, 85);
        const attachmenet = new Discord.Attachment(canvas.toBuffer(), `${title}.png`);
        await Channel.send(attachmenet);
        embed
            .setColor(color)
            .setDescription(description);
        await Channel.send(embed);
    }
    try{
        const Colors = ['#ff2d62', '#ff3a6c', '#ff4473', '#ff5681', '#ff6088', '#ff7094', '#ff7a9b', '#ff84a3', '#ff8eab', '#ff99b2', '#ffa3ba'];
        const what = process.argv[2];

        const importantChannels = [
            `**${getChannel('empress-music-choice')}** - The Empress' choice of music!`,
            `**${getChannel('announcements')}** - This is where all the announcements are made. Please do not mute this channel and do not mute **everyone** mentions.`,
            `**${getChannel('rules')}** - The rules concerning this server. You SHOULD read them and try to follow them.`,
            `**${getChannel('channel-descriptions')}** - THIS channel! It contains short description what the channels DO.`,
            `**${getChannel('rolelist')}** - A list of roles (might be deleted soon).`,
            `**${getChannel('events')}** - All the important events around in the server and where to find them and what prizes there are!`,
            `**${getChannel('welcome')}** - The channel where people get welcomed!`,
        ];
        const oocChannels = [
            `**${getChannel('introduction-channel')}** - Where you can introduce yourself to the rest of us!`,
            `**${getChannel('general-chat-❤')}** - Where the majority of us hang out!`,
            `**${getChannel('game-chat')}** - Where we talk about how shit the current games are!`,
            `**${getChannel('memez-and-media')}** - Memez and random media!`,
            `**${getChannel('dreams-and-stories')}** - Tell us your dreams and crazy stories you experienced!`,
            `**${getChannel('venting-ranting-problems')}** - Rant, share your problems, troubles, homework... whatever makes you go 'UGH, fuck the world.' and more than that! __PLEASE__ keep it serious in here.`,
        ];
        const serverChannels = [
            `**${getChannel('stream-updates')}** - When an artist or gamer comes online, an update is posted here to notify you!`,
            `**${getChannel('server-suggestions')}** - Post all your suggestions, feedback and requests here, for how we can improve the server!`,
            `**${getChannel('other-servers')}** - Other servers! Have a memey server you think we could be interested in? Post a **PERMAMENT** link here!`,
            `**${getChannel('monthly-feedback')}** - This is where we have lengthy discussions and feedbacks about the previous month, how we enjoyed everything and how we did not!`,
        ];
        const artistChannels = [
            `**${getChannel('commissions')}** - Want a commission, or you are doing commissions? Tell us here!`,
            `**${getChannel('your-art')}** - This is where you SHOULD post your art, like music, videos, drawings, 3D.`,
            `**${getChannel('pony-art')}** - Pony art, that is not yours!`,
            `**${getChannel('furry-art')}** - Furry art, that is not yours!`,
            `**${getChannel('other-art')}** - Other types of art!`,
            `**${getChannel('music-share')}** - Share your music, or music you find, here!`,
            `**${getChannel('screenshots')}** - Screenshots from games, or applications!`,
            `**${getChannel('photographs')}** - IRL Photographs!`,
            `**${getChannel('hourly-twi-art')}** - A broken thing.`,
        ];
        const randomRPChannels = [
            `**${getChannel('oc-intros')}** - Introduce your OCs in here, so we know who we are RPing with!`,
            `**${getChannel('sfw-rp-requests')}** - Request specific roleplays here, to be roleplayed in DMs or one of the RP channels here. Please, WHERE in the request, or when you start a discussion with someone.`,
            `**${getChannel('random-rp-1')}** - One of our random RP channels!`,
            `**${getChannel('random-rp-2')}** - One of our random RP channels!`,
        ];
        const RPbuildingChannels = [
            `**${getChannel('oc-building')}** - Need help building your new cool OC? Or do you need help making current one better? You have come to the right place!`,
            `**${getChannel('world-building')}** - Have a world that needs to be fleshed out? Or want to build a new one? Welcome home!`,
        ];
        const sucrosiaChannels = [
            `**${getChannel('sucrosia-rules')}** - Rules concerning the serious RP.`,
            `**${getChannel('sucrosia-lore')}** - Some lore we established concerning the serious RP.`,
            `**${getChannel('sucrosia-world-building')}** - Worl building for our serious RP.`,
        ];
        const botChannels = [
            `**${getChannel('sugar-wash-log')}** - See who washed Sugar from her stink recently! Make sure to wash her daily, to prevent stink!`,
            `**${getChannel('serene-updates')}** - This is where I post what kind of new things I got and what I got fixed!`,
            `**${getChannel('general-botchat')}** - Use this to communicate with the bots! Like me!`,
            `**${getChannel('spam-bots')}** - This is where you SHOULD spam -slots and alexa stuff, that would otherwise, spam the botchat.`,
        ];
        const voiceChannels = [
            `**${getChannel('voice-text')}** - Where you talk, if you are muted, or do not wish to talk, but listen instead.`,
            `**${getChannel('stream-chat')}** - This is where a streamer WOULD chat! If anyone would actually use the stream voice chat here while streaming.`,
            `**${getChannel('550358803992936449')}** - This is the GENERAL voice chat! Wish to talk to others using a microphone? Here you go!`,
            `**${getChannel('560805435326857256')}** - If you want nothing else, but to listen to music all day long, here you go!`,
            `**${getChannel('564141809484431362')}** - Where streamers wishing to talk, should be!`,
        ];
        const staffChannels = [
            `**${getChannel('timeout')}** - THIS special channel is where you will be sent if you misbehave and you do not want to listen to staff! Please, start listening. We do not wish to kick or ban anyone, we would rather talk it out with you instead.`,
        ];

        if(what == 'channel'){
            await getChannel('channel-descriptions', 22);
            await muteChannel('channel-descriptions');
            await postChannel('IMPORTANT STUFF', Colors[0], importantChannels.join('\n\n'));
            await postChannel('OOC', Colors[1], oocChannels.join('\n\n'));
            await postChannel('SERVER STUFF', Colors[2], serverChannels.join('\n\n'));
            await postChannel('ARTISAN LOUNGE', Colors[3], artistChannels.join('\n\n'));
            await postChannel('RANDOM ROLEPLAYS', Colors[4], randomRPChannels.join('\n\n'));
            await postChannel('RP BUILDING', Colors[5], RPbuildingChannels.join('\n\n'));
            await postChannel('SUCROSIA', Colors[6], sucrosiaChannels.join('\n\n'));
            await postChannel('BOT THINGS', Colors[7], botChannels.join('\n\n'));
            await postChannel('NSFW', Colors[8], '**THESE ARE THE SAME AS OTHERS, IF THEY SHARE THE SAME NAME, JUST WITH NSFW TOPICS ALLOWED.**');
            await postChannel('VOICE CHAT', Colors[9], voiceChannels.join('\n\n'));
            await postChannel('STAFF', Colors[10], staffChannels.join('\n\n'));
            await unMuteChannel('channel-descriptions');
            await logEmbed.setTitle('Channels');
        }
        else if(what == 'rules'){
            await getChannel('rules', 8);
            await muteChannel('rules');
            await postChannel('GENERAL RULES', Colors[0], `${rules.general.rule1}${rules.general.rule2}${rules.general.rule3}${rules.general.rule4}${rules.general.rule5}${rules.general.rule6}${rules.general.rule7}${rules.general.rule8}${rules.general.rule9}`);
            await postChannel('GENERAL RULES', Colors[1], `${rules.general2.rule10}${rules.general2.rule11}${rules.general2.rule12}`);
            await postChannel('NSFW RULES', Colors[2], `${rules.nsfw.rule1}${rules.nsfw.rule2}${rules.nsfw.rule3}${rules.nsfw.rule4}${rules.nsfw.rule5}`);
            await postChannel('RP RULES', Colors[3], `${rules.rp.rule1}${rules.rp.rule2}${rules.rp.rule3}${rules.rp.rule4}${rules.rp.rule5}${rules.rp.rule6}`);
            await unMuteChannel('rules');
            await logEmbed.setTitle('Rules');
        }
        await log.send(logEmbed);
        return client.destroy().then(utils.log(`I have sent everything from ${what}.`));
    }
    catch(err){
        return utils.error(err);
    }
}
run();