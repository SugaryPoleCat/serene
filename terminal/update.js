const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const settings = require('../../config/settings.json');
const json = require('../package.json');

const utils = require('../utils');

async function run(){
    try{
        const client = await new Discord.Client();
        utils.info('Connected to Discord', 'cmd/update');
        await client.login(botconfig.token);
        utils.info('Client logged in', 'cmd/update');
        const Guild = await client.guilds.find(g => g.id == settings.guild);
        utils.info(Guild, 'Guild found');
        const Channel = await Guild.channels.find(ch => ch.name == settings.channels.logBot);
        utils.info(Channel.name, 'Channel found');
        const Member = await Guild.members.find(m => m.id == client.user.id);
        utils.info(Member.user.tag, 'Member found');
        let color = await Member.displayHexColor;
        if(color == '#000000'){
            color = await utils.randomHexColor('random');
        }
        utils.info(color, 'Colour found');

        const date = await new Date().toUTCString();
        utils.info(date, 'Date set');

        const description = await 'yay';
        utils.info(description, 'Description set');

        const embed = await new Discord.RichEmbed()
            .setTitle('I got a new update!')
            .setColor(color)
            .setTimestamp()
            .setDescription(`**__${date}__**\n**${client.user.username}** - __${json.version}__\n\n${description}`);
        utils.info('Embed set', 'cmd/update');
        await Channel.send(embed).then(utils.info('Embed sent', 'cmd/update'));
        return client.destroy().then(utils.info('Client destroyed', 'cmd/update'));
    }
    catch(err){
        return utils.error(err);
    }
}
run();