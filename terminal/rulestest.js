const Discord = require('discord.js');
const botconfig = require('../config/botconfig.json');
const settings = require('../config/settings.json');
const utils = require('../utils');
const rulesJSON = require('../config/rules.json');

function displayRule(array, counter){
    let reply = '';
    for(let x = 0; x < array.length; x++){
        reply += `**${x + counter}:** ${array[x]}\n`;
    }
    return reply;
}

async function fox(){
    try{
        /*
        Should get rules from an external thing, like a JSON file or something similar.

        1. GETS THE RULES CHANNEL.
        2. MAKES CHANNEL INVISBLE FOR PEOPLE.
        3. DELETES THE OLD RULES.
        4. RESENDS THE NEW RULES.
        5. MAKES CHANNEL VISIBLE AGAIN.
        */
        // utils.log(JSON.stringify(rulesJSON));
        // utils.log(JSON.stringify(rules2JSON));
        // const general1 = Array.from(rules2JSON.general);
        // utils.log(general1.join('\n\n'));
        const generalarray = Array.from(rulesJSON.general);
        const general2array = Array.from(rulesJSON.general2);
        const nsfwarray = Array.from(rulesJSON.nsfw);
        const rparray = Array.from(rulesJSON.rp);
        const rp2array = Array.from(rulesJSON.rp2);
        let totalrules;
        let counter = 1;
        await utils.log(displayRule(generalarray, counter), 'General 1 rules');
        counter += generalarray.length;
        await utils.log(displayRule(general2array, counter), 'General 2 rules');
        counter = 1;
        await utils.log(displayRule(nsfwarray, counter), 'NSFW rules');
        await utils.log(displayRule(rparray, counter), 'RP 1 rules');
        counter += rparray.length;
        await utils.log(displayRule(rp2array, counter), 'RP 2 rules');
        totalrules = generalarray.length + general2array.length;
        await utils.log(totalrules, 'Total general rules');
        totalrules = nsfwarray.length;
        await utils.log(totalrules, 'Total nsfw rules');
        totalrules = rparray.length + rp2array.length;
        await utils.log(totalrules, 'Total rp rules');
        totalrules = generalarray.length + general2array.length + nsfwarray.length + rparray.length + rp2array.length;
        await utils.log(totalrules, 'Total rules');
        return utils.log(':)');
    }
    catch(err){
        return utils.error(err);
    }
}

fox();