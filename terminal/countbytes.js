// const bit = process.argv[2];
// console.log('bits: ' + bit);
// const countBytes = bit / 8;
// console.log('Bytes: ' + countBytes.toFixed(2));

// const countKiloBits = bit / 1024;
// console.log('Kilobits: ' + countKiloBits.toFixed(2));
// const countKiloBytes = countBytes / 1024;
// console.log('KiloBytes: ' + countKiloBytes.toFixed(2));

// const countMegaBits = countKiloBits / 1024;
// console.log('Megabits: ' + countMegaBits.toFixed(2));
// const countMegaBytes = countKiloBytes / 1024;
// console.log('MegaBytes: ' + countMegaBytes.toFixed(2));

// const countGigaBits = countMegaBits / 1024;
// console.log('Gigabits: ' + countGigaBits.toFixed(2));
// const countGigaBytes = countMegaBytes / 1024;
// console.log('GigaBytes: ' + countGigaBytes.toFixed(2));

// const countTerraBits = countGigaBits / 1024;
// console.log('Terrabits: ' + countTerraBits.toFixed(2));
// const countTerraBytes = countGigaBytes / 1024;
// console.log('TerraBytes: ' + countTerraBytes.toFixed(2));

// const countPetaBits = countTerraBits / 1024;
// console.log('Petabits: ' + countPetaBits.toFixed(2));
// const countPetaBytes = countTerraBytes / 1024;
// console.log('TerraBytes: ' + countPetaBytes.toFixed(2) + '\n\n');



// const revPetaBytes = bit;
// console.log('PetaBytes: ' + revPetaBytes);
// const revPetaBits = revPetaBytes * 8;
// console.log('Petabits: ' + revPetaBits.toFixed(2));

// const revTerraBytes = revPetaBytes * 1024;
// console.log('TerraBytes: ' + revTerraBytes.toFixed(2));
// const revTerraBits = revTerraBytes * 8;
// console.log('Terrabits: ' + revTerraBits.toFixed(2));

// const revGigaBytes = revTerraBytes * 1024;
// console.log('GigaBytes: ' + revGigaBytes.toFixed(2));
// const revGigaBits = revGigaBytes * 8;
// console.log('Gigabits: ' + revGigaBits.toFixed(2));

// const revMegaBytes = revGigaBytes * 1024;
// console.log('MegaBytes: ' + revMegaBytes.toFixed(2));
// const revMegaBits = revMegaBytes * 8;
// console.log('Megabits: ' + revMegaBits.toFixed(2));

// const revKiloBytes = revMegaBytes * 1024;
// console.log('KiloBytes: ' + revKiloBytes.toFixed(2));
// const revKiloBits = revKiloBytes * 8;
// console.log('Kilobits: ' + revKiloBits.toFixed(2));

// const revBytes = revKiloBytes * 1024;
// console.log('Bytes: ' + revBytes.toFixed(2));
// const revBits = revBytes * 8;
// console.log('Bits: ' + revBits.toFixed(2));
const numeral = require('numeral');
function bytesToBits(bytes){
    const result = bytes * 8;
    return result;
}

function bitsToBytes(bits){
    const result = bits / 8;
    return result;
}

function bytesToKilo(bytes){
    const result = bytes / 1024;
    return result;
}

function kiloToBytes(bytes){
    const result = bytes * 1024;
    return result;
}

function counter(one, que, two){
    let result;
    if(one >= two){
        if(que == '+') result = one + two;
        else if(que == '-') result = one - two;
        else if(que == '*') result = one * two;
        else if(que == '/') result = one / two;
    }
    else{
        if(que == '+') result = twot + one;
        else if(que == '-') result = two - one;
        else if(que == '*') result = two * one;
        else if(que == '/') result = two / one;
    }
    return result;
}

const what = process.argv[2];
const amount = parseInt(process.argv[3]);
const symbol = process.argv[4];
const amount2 = parseInt(process.argv[5]);
const what2 = process.argv[6];

let petaBytes, petaBits, terraBytes, terraBits, gigaBytes, gigaBits, megaBytes, megaBits, kiloBytes, kiloBits, bytes, bits;
switch(symbol){
    case '+':
        break;
    default:
        switch(what){
            case 'b':
                bits = amount;
                bytes = bitsToBytes(bits);
                kiloBits = bytesToKilo(bits);
                kiloBytes = bytesToKilo(bytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'B':
                bytes = amount;
                bits = bytesToBits(bytes);
                kiloBits = bytesToKilo(bits);
                kiloBytes = bytesToKilo(bytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'kb':
                kiloBits = amount;
                kiloBytes = bitsToBytes(kiloBits);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'KB':
                kiloBytes = amount;
                kiloBits = bytesToBits(kiloBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'mb':
                megaBits = amount;
                megaBytes = bitsToBytes(megaBits);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'MB':
                megaBytes = amount;
                megaBits = bytesToBits(megaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'gb':
                gigaBits = amount;
                gigaBytes = bitsToBytes(gigaBits);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'GB':
                gigaBytes = amount;
                gigaBits = bytesToBits(gigaBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'tb':
                terraBits = amount;
                terraBytes = bitsToBytes(terraBits);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'TB':
                terraBytes = amount;
                terraBits = bytesToBits(terraBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'pb':
                petaBits = amount;
                petaBytes = bitsToBytes(petaBits);
                terraBits = kiloToBytes(petaBits);
                terraBytes = kiloToBytes(petaBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                break;
            case 'PB':
                petaBytes = amount;
                petaBits = bytesToBits(petaBytes);
                terraBits = kiloToBytes(petaBits);
                terraBytes = kiloToBytes(petaBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                break;
        }
}

console.log('\nbits: ' + numeral(bits).format('0,0.00') + ' b');
console.log('Kilobits: ' + numeral(kiloBits).format('0,0.00') + ' Kb');
console.log('Megabits: ' + numeral(kiloBits).format('0,0.00') + ' Mb');
console.log('Gigabits: ' + numeral(gigaBits).format('0,0.00') + ' Gb');
console.log('Terrabits: ' + numeral(terraBits).format('0,0.00') + ' Tb');
console.log('Petabits: ' + numeral(petaBits).format('0,0.00') + ' Pb\n\n');
console.log('Bytes: ' + numeral(bytes).format('0,0.00') + ' B');
console.log('KiloBytes: ' + numeral(kiloBytes).format('0,0.00') + ' KB');
console.log('MegaBytes: ' + numeral(megaBytes).format('0,0.00') + ' MB');
console.log('GigaBytes: ' + numeral(gigaBytes).format('0,0.00') + ' GB');
console.log('TerraBytes: ' + numeral(terraBytes).format('0,0.00') + ' TB');
console.log('PetaBytes: ' + numeral(petaBytes).format('0,0.00') + ' PB\n');