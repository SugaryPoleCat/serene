const myExp = 18990;
const expIncrease = 1.25;
let currentExp = 100;
let level = 1;
while(currentExp < myExp){
    currentExp *= expIncrease;
    level += 1;
    console.log(`${currentExp} = n\n${level} = level`);
}