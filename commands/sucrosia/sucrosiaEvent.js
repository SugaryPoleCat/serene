const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');

const help = {
    name: 'sevent',
    description: 'Create a new sucrosian event.',
    aliases: [''],
    usage: '<event room name> <description>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('SUCROSIAN EVENT')
            .setColor(utils.memberColor(message));
        if(message.content.slice(8) == '' || message.content.slice(8) == null) return message.reply(embed
            .setColor(utils.randomHexColor('error'))
            .setDescription('Please put in something, to paste into the event description. Max: **2048** letters, including spaces.'));
        const channel = message.guild.channels.find(ch => ch.name == 'sucrosian-event-list');
        const now = new Date().toUTCString();
        return channel.send(embed
            .setDescription(message.content.slice(8))
            .addBlankField()
            .addField('DATE', now, false));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};