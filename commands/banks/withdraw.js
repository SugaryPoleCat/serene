const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { UserBank, Currencies } = require('../../database/dbObjects');
const banks = require('../../config/banks.json');

const help = {
    name: 'withdraw',
    description: 'Withdraw your hard earned goods back from the bank!',
    usage: '<cubes | sucrosium> <amount (1-100000)>',
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: true,
    delete: true,
    nsfw: false,
};
/*
This will handle the depositing commands in the thing.
*/
async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('WITHDRAW')
            .setColor(utils.randomHexColor('error'))
            .setTimestamp();
        const what = args[0];
        let amount, dbCurr, dbBank, dbUserBank;
        switch(what){
            case 'cubes':
                dbUserBank = await UserBank.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbUserBank) return message.reply(embed
                    .setDescription('You are not registered in any bank.\nPlease use `!bank list` and then use `!bank register <id>` to register to a bank.'));
                if(dbUserBank.strawberrycubes == 0) return message.reply(embed
                    .setDescription('You have **0** __Strawberrycubes__.\n\nYou literally can not withdraw **0**. Perhaps you should deposit first.'));
                amount = args[1];
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(amount == 'all') amount = dbUserBank.strawberrycubes;
                else{
                    if(amount > 100000) return message.reply(embed
                        .setDescription('you can not withdraw more than **100000** __Strawberrycubes__.'));
                    if(amount < 1) return message.reply(embed
                        .setDescription('You can not withdraw less than **1** __Strawberrycubes__.'));
                    if(amount > dbUserBank.strawberrycubes) return message.reply(embed
                        .setDescription(`You can not withdraw more than what you have in your **${dbBank.name}** account.\n\nYou currently have **${dbCurr.strawberrycubes}** __Strawberrycubes__ and you tried to withdraw **${amount}** __Strawberrycubes__.\n\nYou could also type \`all\` instead of a number to withdraw **ALL** your __Strawberycubes__.`));
                }
                dbCurr.strawberrycubes += parseInt(amount);
                dbUserBank.strawberrycubes -= parseInt(amount);
                await dbCurr.save();
                await dbUserBank.save();
                dbBank = await banks.find(obj => {
                    return obj.bankID == dbUserBank.bankID;
                });
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`I have successfully withdrawn **${amount}** of your __Strawberrycubes__, from **${dbBank.name}** account and handed them over to your backpack.\n\nYou have **${dbUserBank.sucrosium}** __Sucrosium__ remaining in your bank account.\nYou now have **${dbCurr.strawberrycubes}** __Strawberrycubes__ in your inventory.`));
            case 'sucrosium':
                dbUserBank = await UserBank.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbUserBank) return message.reply(embed
                    .setDescription('You are not registered in any bank.\nPlease use `!bank list` and then use `!bank register <id>` to register to a bank.'));
                if(dbUserBank.sucrosium == 0) return message.reply(embed
                    .setDescription('You have **0** __Sucrosium__.\n\nYou literally can not withdraw **0**. Perhaps you should deposit first.'));
                amount = args[1];
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(amount == 'all') amount = dbUserBank.sucrosium;
                else{
                    if(amount > 100000) return message.reply(embed
                        .setDescription('you can not withdraw more than **100000** __Sucrosium__.'));
                    if(amount < 1) return message.reply(embed
                        .setDescription('You can not withdraw less than **1** __Sucrosium__.'));
                    if(amount > dbUserBank.sucrosium) return message.reply(embed
                        .setDescription(`You can not withdraw more than what you have in your **${dbBank.name}** account.\n\nYou currently have **${dbCurr.sucrosium}** __Sucrosium__ and you tried to withdraw **${amount}** __Sucrosium__.\n\nYou could also type \`all\` instead of a number to withdraw **ALL** your __Sucrosium__.`));
                }
                dbCurr.sucrosium += parseInt(amount);
                dbUserBank.sucrosium -= parseInt(amount);
                await dbCurr.save();
                await dbUserBank.save();
                dbBank = await banks.find(obj => {
                    return obj.bankID == dbUserBank.bankID;
                });
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`I have successfully withdrawn **${amount}** of your __Sucrosium__, from **${dbBank.name}** account and handed them over to your backpack.\n\nYou have **${dbUserBank.sucrosium}** __Sucrosium__ remaining in your bank account.\nYou now have **${dbCurr.sucrosium}** __Sucrosium__ in your inventory.`));
            default:
                return message.reply(embed
                    .setDescription(`No such argument found.\nPlease use: \`${help.usage}\``));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};