const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { UserBank, Currencies } = require('../../database/dbObjects');
const banks = require('../../config/banks.json');

const help = {
    name: 'deposit',
    description: 'Deposit your hard earned goods into the bank, so you can not lose them!',
    usage: '<cubes | sucrosium> <amount (1-100000)>',
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: true,
    delete: true,
    nsfw: false,
};
/*
This will handle the depositing commands in the thing.
*/
async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('DEPOSIT')
            .setColor(utils.randomHexColor('error'))
            .setTimestamp();
        const what = args[0];
        let amount, dbCurr, dbBank, dbUserBank;
        switch(what){
            case 'cubes':
                dbUserBank = await UserBank.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbUserBank) return message.reply(embed
                    .setDescription('You are not registered in any bank.\nPlease use `!bank list` and then use `!bank register <id>` to register to a bank.'));
                amount = args[1];
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(dbCurr.strawberrycubes == 0) return message.reply(embed
                    .setDescription('You have **0** __Strawberrycubes__.\n\nYou literally can not donate **0**.'));
                if(amount == 'all') amount = dbCurr.strawberrycubes;
                else{
                    if(amount > 100000) return message.reply(embed
                        .setDescription('you can not deposit more than **100000** __Strawberrycubes__.'));
                    if(amount < 1) return message.reply(embed
                        .setDescription('You can not donate less than **1** __Strawberrycubes__.'));
                    if(amount > dbCurr.strawberrycubes) return message.reply(embed
                        .setDescription(`You can not deposit more than what you have on you currently.\n\nYou currently have **${dbCurr.strawberrycubes}** __Strawberrycubes__ and you tried to deposit **${amount}** __Strawberrycubes__.\n\nYou could also type \`all\` instead of a number to deposit **ALL** your __Strawberycubes__.`));
                }
                dbCurr.strawberrycubes -= parseInt(amount);
                dbUserBank.strawberrycubes += parseInt(amount);
                await dbCurr.save();
                await dbUserBank.save();
                dbBank = await banks.find(obj => {
                    return obj.bankID == dbUserBank.bankID;
                });
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`I have successfully deposited **${amount}** of your __Strawberrycubes__ into your **${dbBank.name}** account.\n\nYou now have **${dbUserBank.strawberrycubes}** __Strawberrycubes__ in your bank account.\nYou now have **${dbCurr.strawberrycubes}** __Strawberrycubes__ in your inventory.`));
            case 'sucrosium':
                dbUserBank = await UserBank.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbUserBank) return message.reply(embed
                    .setDescription('You are not registered in any bank.\nPlease use `!bank list` and then use `!bank register <id>` to register to a bank.'));
                amount = args[1];
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(dbCurr.sucrosium == 0) return message.reply(embed
                    .setDescription('You have **0** __Sucrosium__.\n\nYou literally can not donate **0**.'));
                if(amount == 'all') amount = dbCurr.sucrosium;
                else{
                    if(amount > 100000) return message.reply(embed
                        .setDescription('you can not deposit more than **100000** __Sucrosium__.'));
                    if(amount < 1) return message.reply(embed
                        .setDescription('You can not donate less than **1** __**Sucrosium__.'));
                    if(amount > dbCurr.sucrosium) return message.reply(embed
                    .setDescription(`You can not deposit more than what you have on you currently.\n\nYou currently have **${dbCurr.sucrosium}** __Sucrosium__ and you tried to deposit **${amount}** __Sucrosium__.\n\nYou could also type \`all\` instead of a number to deposit **ALL** your __Sucrosium__.`));
                }
                dbCurr.sucrosium -= parseInt(amount);
                dbUserBank.sucrosium += parseInt(amount);
                await dbCurr.save();
                await dbUserBank.save();
                dbBank = await banks.find(obj => {
                    return obj.bankID == dbUserBank.bankID;
                });
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`I have successfully deposited **${amount}** of your __Sucrosium__ into your **${dbBank.name}**, bank account.\n\nYou now have **${dbUserBank.sucrosium}** __Sucrosium__ in your bank account.\nYou now have **${dbCurr.sucrosium}** __Sucrosium__ in your inventory.`));
            default:
                return message.reply(embed
                    .setDescription(`No such argument found.\nPlease use: \`${help.usage}\``));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};