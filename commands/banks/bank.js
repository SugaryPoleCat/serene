const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { UserBank, Currencies } = require('../../database/dbObjects');
const banks = require('../../config/banks.json');

const help = {
    name: 'bank',
    description: 'Here you can register yourself to a bank, upgrade from current to a new one, unregister, check details of banks, or even check what bank you\'re in.',
    usage: '<list | check | upgrade | register | unregister>\nREGISTER: <bankID>\n UPGRADe IS NOT YET IMPLEMENTED',
};
const config = {
    cooldown: 7,
    guildOnly: false,
    args: true,
    delete: true,
    nsfw: false,
};

const typeMatch = ['yes', 'no'];
const type = response => {
    return typeMatch.some(answer => answer.toLowerCase() === response.content.toLowerCase());
};
/* This command has to check for things like:
LIST =  list all banks available.
REGISTER = register yourself to a bank, provided with a bank ID OR name.
UPGARDE = automatically upgrade to a higher bank tier. This takes the model of the current user, checks which bank he has, then pulls an ID 1 higher than he has and BLAM. Upgrade.
Oviouosly it checks if it's possible an dhow much it would cost. MAYBE the model has ot provide how much the cost has to be.
UNREGISTER OR CANCEL = This rmeoves the user frm curretn bank and cleans their registry.
DETAILS = view more details about a bank.
*/
async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('BANKING')
            .setTimestamp();
        const what = args[0];
        // MOVE THIS COLLECTION OUT OF HERE SO WE DONT HAVE TO REDO IT EVERYTIME THIS COMMAND IS CALLED.
        const banksList = new Discord.Collection();
        let bankListSorted;
        let dbBank, dbBankUser, dbCurr, id, target, targetText, collected, rType, oldBank;
        /* so we need tog et upgrade done.
        It should look for bank you have now, then go up an ID and see how much the cost is and se if you can do it.*/
        switch(what){
            // FOR UNREGISTER
            // Check with !join, to add awaitmessage and await a reply.
            // It should ask "are you sure"
            case 'list':
                await banks.forEach(b => banksList.set(b.bankID, b));
                bankListSorted = await banksList.sort((a, b) => a.bankID - b.bankID)
                    .map((bank, position) => `__ID__: **${position}**\n__NAME__: **${bank.name}**\nCost to open: **${bank.openFee}**`)
                    .join('\n\n');
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setDescription(`**__Here is a list of our banks__**\n\n${bankListSorted}`));
            case 'register':
                dbBankUser = await UserBank.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(dbBankUser) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You are already registered in a bank.\nPlease unregister first, or upgrade.'));
                id = parseInt(args[1]);
                dbBank = banks.find(obj => {
                    return obj.bankID == id;
                });
                if(dbBank == undefined || !dbBank) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('This bank does not exist.\nPlease check the ID again.'));
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(dbCurr.strawberrycubes == 0) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('Your balance is **0** __Strawberycubes__.\n\nUnfortunately, I can not open an account here, if  you have no money on your person.'));
                if(dbBank.openFee > dbCurr.strawberrycubes) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription(`You do not have enough money to open the account.\nThe cost is: ${dbBank.openFee} __Strawberrycubes__ and you only have: ${dbCurr.strawberrycubes} __Strawberrycubes__.\n\nPlease get the required sum, before opening the account. You can use \`!work\` to boost your income and \`!daily\`.`));
                dbBankUser = await UserBank.findOrCreate({
                    where: { userID: message.author.id.toString(), bankID: id },
                    defaults: { userID: message.author.id.toString(), bankID: id },
                });
                dbCurr.strawberrycubes -= dbBank.openFee;
                await dbCurr.save();
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`Congratulations!\nYou have successfully opened the account in: **${dbBank.name}**, bank!\n\nYou now have: **${dbCurr.strawberrycubes}** __Strawberrycubes__ after opening the account.\nPlease, use \`!deposit\` to deposit some goods!`));
            case 'unregister':
                dbBankUser = await UserBank.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbBankUser) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You are not registered in any bank.'));
                await message.reply(embed
                    .setDescription('Are you sure you want to unregister?\n\n**__WARNING__**\nAnything you kept in your bank, **WILL BE LOST** if you have not withdrawn, if you answer yes.\nPlease answer `yes`, `no`. I\'m waiting **10** seconds.')
                    .setColor(utils.randomHexColor('warning')));
                collected = await message.channel.awaitMessages(type, { maxMatches: 1, time: 10000, errors: ['time'] });
                rType = collected.first().content.toLowerCase();
                oldBank = await banks.find(obj => {
                    return obj.bankID == dbBankUser.bankID;
                });
                if(rType == 'yes'){
                    await UserBank.destroy({
                        where: { userID: message.author.id.toString() },
                    });
                    return message.reply(embed
                        .setColor(utils.memberColor(message, message.author))
                        .setThumbnail(message.author.displayAvatarURL)
                        .setTimestamp()
                        .setDescription(`Congratulations!\n\nYou have successfully unregistered from the: **${oldBank.name}**, bank account.`));
                }
                else if(rType == 'no') return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setDescription('Okay, I\'m cancelling your request to unregister.')
                    .setThumbnail(message.author.displayAvatarURL)
                    .setTimestamp());
                break;
            case 'upgrade':
                dbBankUser = await UserBank.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbBankUser) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You are not registered in any bank.\n\nOpen an account in a bank first, with `!bank list` and then picking one you like with `!bank register <id>`.'));
                id = parseInt(dbBankUser.bankID + 1);
                // First find the bank we have
                oldBank = await banks.find(obj => {
                    return obj.bankID == dbBankUser.bankID;
                });
                // THEN see if we go ONE ID UP, if it's possible.
                dbBank = await banks.find(obj => {
                    return obj.bankID == id;
                });
                if(!dbBank) return message.reply(embed
                    .setColor(utils.randomHexColor('warning'))
                    .setDescription('You have the currently, best bank of Sucrosia.\n\nI can not upgrade you any further.'));
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaulst: { userID: message.author.id.toString() },
                });
                if(dbCurr.strawberrycubes == 0) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('Your balance is **0** __Strawberycubes__.\n\nUnfortunately, I can not upgrade you if you have no money to upgrade with.'));
                if(dbBank.upgradeCost > dbCurr.strawberrycubes) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription(`You do not have enough money to upgrade.\nThe cost is: ${dbBank.upgradeCost} __Strawberrycubes__ and you only have: ${dbCurr.strawberrycubes} __Strawberrycubes__.\n\nPlease get the required sum, before upgrading. You can use \`!work\` to boost your income and \`!daily\`.`));
                dbCurr.strawberrycubes -= dbBank.upgradeCost;
                dbBankUser.bankID = dbBank.bankID;
                await dbCurr.save();
                await dbBankUser.save();
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`Congratulations!\nYou have upgraded your bank from: **${oldBank.name}**, bank, to: **${dbBank.name}**, bank!\n\nYou now have: **${dbCurr.strawberrycubes}** __Strawberrycubes__ after the upgrade.`));
            case 'check':
                target = message.mentions.users.first() || message.author;
                dbBankUser = await UserBank.findOne({
                    where: { userID: target.id.toString() },
                });
                if(!dbBankUser) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You have not registered to any bank.\nIf you wish to however, check the list of banks with `!bank list` and see which one you can join.'));
                dbBank = await banks.find(obj => {
                    return obj.bankID == dbBankUser.bankID;
                });
                if(!dbBank) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription(`Bank not found...\nPerhaps it does not exist anymore? Check with ${botconfig.botowners.ownerID} about resetting your bank.`));
                await message.reply(embed
                    .setColor(utils.memberColor(message))
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription('To keep it private, I have sent you a DM info about your bank and what you have deposited there.'));
                return message.author.send(embed
                    .setColor(utils.memberColor(message))
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription(`Your bank is: **${dbBank.name}**.\n
                    You currently have deposited:
                    __Strawberrycubes__: **${dbBankUser.strawberrycubes}**
                    __Sucrosium__: **${dbBankUser.sucrosium}**`));
            default:
                return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('No such argument found.'));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};