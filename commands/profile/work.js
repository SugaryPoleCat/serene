const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Work, Sucrons, Currencies } = require('../../database/dbObjects');

const help = {
    name: 'work',
    description: 'This will let you work for an amount of time provided! The more you work, the better the bonus! You can also cancel your work, if you have any, and provide new amount of time!',
    usage: '<work | check | cancel>\n\nIf you provided <work> choice, please also include: <Nd | hNh | m>. N is time. d/h/m is either days/hours/minutes. Working more generates more resources, but you will have to wait for longer.',
};
const config = {
    cooldown: 10,
    args: true,
    delete: true,
    nsfw: false,
    guildOnly: false,
};

// TODO:
// Add some sort of chance to get more or le   ss resources based on time.

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('WORK')
            .setColor(utils.randomHexColor('error'))
            .setTimestamp();
        let dbCurr, dbSuc, dbWork, matches, time, typeOfTime, time2, typeOfTime2, reply = '', cubesWon, sucronsWon, sucrosiumWon, timeWon, extraCubes, description;
        const what = args[0];
        const currentTime = Math.floor(new Date().getTime() / 1000);
        let timeToCount;
        let timeToWait;
        switch(what){
            case 'check':
                dbWork = await Work.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbWork){
                    description = 'You do not have any work currently.\nPlease start with `!work work <amount of time>`, if you wish.';
                    break;
                }
                if(currentTime > dbWork.workLength){
                    [dbWork] = await Work.findOrCreate({
                        where: { userID: message.author.id.toString() },
                        defaults: { userID: message.author.id.toString() },
                    });
                    timeWon = dbWork.workLength - dbWork.started;
                    // So these are the base values.
                    cubesWon = 100 + Math.floor(timeWon / 36);
                    sucronsWon = 50 + Math.floor(timeWon / 90);
                    sucrosiumWon = 10 + Math.floor(timeWon / 126);
                    [dbCurr] = await Currencies.findOrCreate({
                        where: { userID: message.author.id.toString() },
                        defaults: { userID: message.author.id.toString() },
                    });
                    dbCurr.strawberrycubes += parseInt(cubesWon);
                    dbCurr.sucrosium += parseInt(sucrosiumWon);
                    await dbCurr.save();
                    [dbSuc] = await Sucrons.findOrCreate({
                        where: { userID: message.author.id.toString() },
                        defaults: { userID: message.author.id.toString() },
                    });
                    dbSuc.eaten += parseInt(sucronsWon);
                    dbSuc.sucrons = await utils.sucronsCalc(message.author);
                    await dbSuc.save();
                    embed.setColor(utils.memberColor(message))
                        .setThumbnail(message.author.displayAvatarURL);
                    description = `Congratulations, you have completed your work!\n\nIn return, you win **${cubesWon}** __Strawberrycubes__, **${sucrosiumWon}** __Sucrosium__ and **${sucronsWon}** __Sucrons__.\nYou have a total of **${dbCurr.strawberrycubes}** __Strawberrycbes__, **${dbCurr.sucrosium}** __Sucrosium__ and **${dbSuc.sucrons}** __Sucrons__.`;
                    break;
                }
                else{
                    timeToWait = parseInt(dbWork.workLength - currentTime);
                    const timeToWaitS = timeToWait % 60;
                    timeToWait /= 60;
                    const timeToWaitM = Math.floor(timeToWait % 60);
                    timeToWait /= 60;
                    const timeToWaitH = Math.floor(timeToWait % 24);
                    embed.setColor(utils.randomHexColor('warning'))
                        .setThumbnail(message.author.displayAvatarURL);
                    description = `You have not completed your work yet. You must wait: **${timeToWaitH}** __hours__ and **${timeToWaitM}** __minutes__.`;
                    break;
                }
            case 'work':
                [dbWork] = await Work.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                matches = args[1].match(/(\d*)(\w)?((\d*)(\w))/);
                utils.log(matches);
                // 1 hour 3600;
                // 1 minute 60;
                // 1 day 86400;
                // Checks waht the user provided for how long they wish to wrok.
                switch(matches[2]){
                    case undefined:
                        dbWork.started = currentTime;
                        typeOfTime = matches[3];
                        time = matches[1];
                        if(typeOfTime == 'h'){
                            if(time > 72){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for more than **72** __hours__.'));
                            }
                            else if(time < 1){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for less than **1** __hours__.'));
                            }
                            timeToCount = currentTime + (time * 3600);
                            typeOfTime = 'hours';
                        }
                        else if(typeOfTime == 'd'){
                            if(time > 3){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for more than **3** __days__.'));
                            }
                            else if(time < 1){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for less than **1** __day__.'));
                            }
                            timeToCount = currentTime + (time * 86400);
                            typeOfTime = 'days';
                        }
                        else{
                            return message.reply(embed
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('Please use either `d`, or `h` for DAYS or HOURS respectively.'));
                    }
                        utils.log(timeToCount);
                        dbWork.workLength = timeToCount;
                        await dbWork.save();
                        reply = `${time} ${typeOfTime}`;
                        break;
                    default:
                        dbWork.started = currentTime;
                        typeOfTime = matches[2];
                        time = matches[1];
                        time2 = matches[4];
                        typeOfTime2 = matches[5];
                        if(typeOfTime == 'h'){
                            if(time > 72){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for more than **72** __hours__.'));
                            }
                            else if(time < 1){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for less than **1** __hour__.'));
                            }
                            timeToCount = currentTime + (time * 3600);
                            typeOfTime = 'hours';
                            reply = `${time} hours`;
                        }
                        else if(typeOfTime == 'd'){
                            if(time > 3){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for more than **3** __days__.'));
                            }
                            else if(time < 1){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for less than **1** __day__.'));
                            }
                            timeToCount = currentTime + (time * 86400);
                            typeOfTime = 'days';
                            reply = `${time} days`;
                        }
                        if(typeOfTime2 == 'h'){
                            if(time2 > 23){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for more than **23** __hours__.'));
                            }
                            else if(time < 1){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for less than **1** __hour__.'));
                            }
                            timeToCount = timeToCount + (time2 * 3600);
                            typeOfTime2 = 'hours';
                        }
                        else if(typeOfTime2 == 'm'){
                            if(time2 > 59){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for more than **59** __minutes__.'));
                            }
                            else if(time < 1){
                                return message.reply(embed
                                    .setColor(utils.randomHexColor('error'))
                                    .setDescription('You can not work for less than **1** __minute__.'));
                            }
                            timeToCount = timeToCount + (time2 * 60);
                            typeOfTime2 = 'minutes';
                        }
                        else{
                            return message.reply(embed
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('Dude, just... It is not rocket science! Just-- `<XdYh>` OR `<XhYm>`! Where X and Y are numbers and `d, h, m` are days, hours and minutes! GOD.'));
                        }
                        utils.log(timeToCount);
                        dbWork.workLength = timeToCount;
                        await dbWork.save();
                        reply += ` and ${time2} ${typeOfTime2}.`;
                        break;
                }
                return message.reply(embed
                    .setThumbnail(message.author.displayAvatarURL)
                    .setColor(utils.memberColor(message, message.author))
                    .setDescription(`Congratulations! You have decided to work for **${matches[0]}**!\n\nDo not forget to check your work after it is done with \`!work check\`!`));
            case 'cancel':
                dbWork = await Work.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbWork){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('error'))
                        .setDescription('You have not started any work, that I can cancel.'));
                }
                await Work.destroy({
                    where: { userID: message.author.id.toString() },
                });
                return message.reply(embed
                    .setThumbnail(message.author.displayAvatarURL)
                    .setColor(utils.memberColor(message, message.author))
                    .setDescription('I have cancelled your work. I hope you are happy not earning extra.'));
            default:
                return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('I-- am unsure what you want to do at this point... PLease, read help to know what the fuck you\'re doing, or try: `check`, `work`, `cancel`.'));
        }
        return message.reply(embed
            .setDescription(description));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};