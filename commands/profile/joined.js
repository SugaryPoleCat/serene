const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'joined',
    description: 'Check which roles you are currently joined in.',
};
const config = {
    cooldown: 10,
    guildOnly: true,
    args: false,
    delete: true,
    nsfw: false,
};

let roleArraySpecies = [];
let roleArrayGender = [];
let roleArrayAccess = [];
let roleArrayJob = [];
let roleArrayStaff = [];
let roleArrayOwner = [];
let roleArrayNormal = [];
// const roleCollection = [roleArrayAccess, roleArrayNormal, roleArrayGender, roleArrayJob, roleArraySpecies, roleArrayStaff, roleArrayOwner];
function findRole(message, arrayName, roleArrayName){
    for(let x = 0; x <= arrayName.length; x++){
        const role = message.member.roles.find(r => r.name == arrayName[x]);
        if(role){
            roleArrayName.push(role.name);
        }
    }
}
// let description = '';
// function addDescription(arrayCheck, arrayName, roleArrayName){
//     for(let i = 0; i < arrayName.length; i++){
//         const role = message.member.roles.find(r => r.name == arrayName[i]);
//         if(role) roleArrayName.push(role.name);
//     }
//     if(!roleArrayName) description += `**${arrayCheck}**\n\`\`\`${roleArrayName.join(', ')}\`\`\``;
//     else description += `**${arrayCheck}**\n\`\`\`None\`\`\``;
// }
const embed = new Discord.RichEmbed()
    .setTimestamp();

async function fox(message, args, client){
    // lets get it on!
    try{
        const arrayDescriptors = ['=== ACCESS ROLES ===', '=== NORMAL ROLES ===', '=== GENDER ===', '=== PROFESSION ===', '=== SPECIES ===', '=== STAFF ROLES ===', '=== OWNER ROLES ==='];
        const arrayAccess = ['NSFW', 'RP', 'NSFW memes', 'STAFF', 'Newcomers'];
        const arrayNormal = ['Citizens', 'Hybrid'];
        const arrayGender = ['Male', 'Mare', 'Stalion', 'Mare', 'Other gender'];
        const arrayJob = ['Artisans', 'Cooks', 'Maids', 'Hospital staff'];
        const arraySpecies = ['Unicorn', 'Earth pony', 'Pegasus', 'Alicorn', 'Changeling', 'Batpony', 'Wolf', 'Fox', 'Other', 'Horse'];
        const arrayStaff = ['Royal Family', 'Sucrosian Council', 'Sucrosian Guards', 'Royal Scientist', 'Royal Scribe', 'Royal Artisan'];
        const arrayOwner = ['The Empress', 'Sorunome Chat Puppet'];

        findRole(message, arrayAccess, roleArrayAccess);
        findRole(message, arrayNormal, roleArrayNormal);
        findRole(message, arrayGender, roleArrayGender);
        findRole(message, arrayJob, roleArrayJob);
        findRole(message, arraySpecies, roleArraySpecies);
        findRole(message, arrayStaff, roleArrayStaff);
        findRole(message, arrayOwner, roleArrayOwner);

        if(roleArrayAccess.length == 0) roleArrayAccess.push('None');
        if(roleArrayNormal.length == 0) roleArrayNormal.push('None');
        if(roleArrayGender.length == 0) roleArrayGender.push('None');
        if(roleArrayJob.length == 0) roleArrayJob.push('None');
        if(roleArraySpecies.length == 0) roleArraySpecies.push('None');
        if(roleArrayStaff.length == 0) roleArrayStaff.push('None');
        if(roleArrayOwner.length == 0) roleArrayOwner.push('None');

        embed
            .setTitle('JOINED')
            .setColor(message.member.displayHexColor)
            .setThumbnail(message.author.displayAvatarURL)
            .setDescription(`**${arrayDescriptors[0]}**\n
            \`\`\`${roleArrayAccess.join(', ')}\`\`\`\n\n**${arrayDescriptors[1]}**\n\`\`\`${roleArrayNormal.join(', ')}\`\`\`\n\n**${arrayDescriptors[2]}**\n\`\`\`${roleArrayGender.join(', ')}\`\`\`\n\n**${arrayDescriptors[3]}**\n\`\`\`${roleArrayJob.join(', ')}\`\`\`\n\n**${arrayDescriptors[4]}**\n\`\`\`${roleArraySpecies.join(', ')}\`\`\`\n\n**${arrayDescriptors[5]}**\n\`\`\`${roleArrayStaff.join(', ')}\`\`\`\n\n**${arrayDescriptors[6]}**\n\`\`\`${roleArrayOwner.join(', ')}\`\`\``);

        // addDescription(arrayDescriptors[0], arrayAccess, roleArrayAccess);
        // addDescription(arrayDescriptors[1], arrayNormal, roleArrayNormal);
        // addDescription(arrayDescriptors[2], arrayGender, roleArrayGender);
        // addDescription(arrayDescriptors[3], arrayJob, roleArrayJob);
        // addDescription(arrayDescriptors[4], arraySpecies, roleArraySpecies);
        // addDescription(arrayDescriptors[5], arrayStaff, roleArrayStaff);
        // addDescription(arrayDescriptors[6], arrayOwner, roleArrayOwner);
        // embed
        //     .setTitle('JOINED')
        //     .setColor(message.member.displayHexColor)
        //     .setThumbnail(message.author.displayAvatarURL)
        //     .setDescription(description);
        // for(let i = 0; i < arrayDescriptors.length; i++){
        //     description += arrayDescriptors
        // }
        // embed
        //     .setTitle('JOINED')
        //     .setColor(message.member.displayHexColor)
        //     .setThumbnail(message.author.displayAvatarURL)
        //     .setDescription(description);
        roleArraySpecies = [];
        roleArrayGender = [];
        roleArrayAccess = [];
        roleArrayJob = [];
        roleArrayStaff = [];
        roleArrayOwner = [];
        roleArrayNormal = [];
        return message.reply(embed);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};