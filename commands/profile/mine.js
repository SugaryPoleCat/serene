const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Mining, UserResources, UserCrystals } = require('../../database/dbObjects');

const help = {
    name: 'mine',
    description: 'This will let you mine for rare minerals.',
    usage: '<mine | crystals | check | cancel>\nCancel will ask if you are sure before canceling.',
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
};

let chances, dbCrystals, colour, cut, shape, grade, size, amount;

const crystalColour = ['Red', 'Blue', 'Green', 'Yellow', 'Purple', 'Pink'];
// For dispaling info, just add 'sided' or 'sides' manually.
const crystalCut = [2, 3, 4, 5, 6, 8];
const crystalShape = ['Pointy', 'Teardrop', 'Rounded', 'Triangular', 'Spherical', 'Hexagonal', 'Rectangular'];
const crystalGrade = ['Chipped', 'Flawed', 'Regular', 'Flawless', 'Perfect'];
const crystalSize = ['Tiny', 'Small', 'Medium', 'Large', 'Big', 'Oversized', 'Huge', 'Gigantic'];

function getCrystals(){
    
}

async function fox(message, args, client){
    try{
        // This should let you mine for stuff.
        // MAYBE you should be able to select what to mine for, the time is set and cooldown between minings is a few hours.
        // Mining time is set.
        // Crystals are harder to mine for, so they take longer.
        const embed = new Discord.RichEmbed()
            .setTitle('MINE')
            .setColor(utils.randomHexColor('error'))
            .setTimestamp();
        const what = args[0];
        let description, dbMine, dbResources, timeToWait, stone, wood, goldOre, silverOre;
        const currentTime = Math.floor(new Date().getTime() / 1000);
        let crystalsFound = [];
        switch(what){
            case 'check':
                dbMine = await Mining.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbMine){
                    description = 'You do not have any mining work currently.\nPlease start with `!mine mine`, or `!mine crystals`.';
                    break;
                }
                // IF you got the right time after the required time.
                if(currentTime > dbMine.mineLength){
                    description = 'Congratulations!\nYou have successfully completed work and gained following:\n\n';
                    [dbMine] = await Mining.findOrCreate({
                        where : { userID: message.author.id.toString() },
                        defaults: { userID: message.author.id.toString() },
                    });
                    switch(dbMine.type){
                        case 'mine':
                            // get stone, wood, ores.
                            chances = Math.random();
                            if(chances >= 0.5 && chances < 0.7){
                                wood = Math.floor(Math.random() * (50 - 10));
                                stone = Math.floor(Math.random() * (25 - 5));
                                silverOre = Math.floor(Math.random() * 10);
                                description += `**${wood}** __Wood__, **${stone}** __Stone__ and **${silverOre}** __Silver Ore__!`;
                            }
                            else if(chances >= 0.7){
                                wood = Math.floor(Math.random() * (100 - 50));
                                stone = Math.floor(Math.random() * (50 - 25));
                                silverOre = Math.floor(Math.random() * (50 - 10));
                                goldOre = Math.floor(Math.random() * 10);
                                description += `**${wood}** __Wood__, **${stone}** __Stone__, **${silverOre}** __Silver Ore__ and **${goldOre}** __Gold Ore__!`;
                            }
                            else{
                                wood = Math.floor(Math.random() * 10);
                                stone = Math.floor(Math.random() * 5);
                                goldOre = 0;
                                silverOre = 0;
                                description += `**${wood}** __Wood__ and **${stone}** __Stone__!`;
                            }
                            dbResources = await UserResources.findOrCreate({
                                where: { userID: message.author.id.toString() },
                                defaults: { userID: message.author.id.toString() },
                            });
                            dbResources.stone += parseInt(stone);
                            dbResources.wood += parseInt(wood);
                            dbResources.goldOre += parseInt(goldOre);
                            dbResources.silverOre += parseInt(silverOre);
                            await dbResources.save();
                            description += `\n\nYou now have: **${dbResources.wood}** __Wood__, **${dbResources.stone}** __Stone__, **${dbResources.silverOre}** __Silver Ore__ and **${dbResources.goldOre}** __Gold Ore__.`;
                            break;
                        case 'crystal':
                            // Chances for,: DIFFERENT CRYSTALS IN TOTAL, COLOR, CUT, SHAPE, GRADE, SIZE, AMOUNT.
                            // so frst we check chance on if a user will get more than one type of a crystal. Type is tied to the colour.
                            // So in total you can have 6 different crystals at a time.
                            // chances = Math.random();
                            // if(chances > 0.2){
                            //     // 2
                            // }
                            // else if(chances > 0.4){
                            //     // 3
                            // }
                            // else if(chances > 0.6){
                            //     // 4
                            // }
                            // else if(chances > 0.8){
                            //     // 5
                            // }
                            // else if(chances > 0.9){
                            //     // 6
                            // }
                            // else{
                            //     // 1
                            // }

                            // We will do the different crystals antoher time;

                            // to make it more like... more special attributes have loweer chance of being got.

                            colour = await utils.arrayRandomIndex(crystalColour);
                            cut = await utils.arrayRandomIndex(crystalCut);
                            shape = await utils.arrayRandomIndex(crystalShape);
                            grade = await utils.arrayRandomIndex(crystalGrade);
                            size = await utils.arrayRandomIndex(crystalSize);
                            amount = await Math.floor(Math.random() * 5);
                            dbCrystals = await UserCrystals.findOrCreate({
                                where: { userID: message.author.id.toString(), colour: colour, cut: parseInt(cut), shape: shape, grade: grade, size: size },
                                defaults: { userID: message.author.id.toString(), colour: colour, cut: parseInt(cut), shape: shape, grade: grade, size: size },
                            });
                            dbCrystals.amount += amount;
                            await dbCrystals.save();
                            description = `**${amount}**, **${dbCrystals.colour}** coloured, **${dbCrystals.shape}** shaped, **${dbCrystals.cut}** sided, **${dbCrystals.grade}**, **${dbCrystals.size}**`;
                            if(amount > 1) description += ' crystals!';
                            else description += ' crystal!';
                            break;
                        default:
                            break;
                    }
                    embed.setColor(utils.memberColor(message)
                        .setThumbnail(message.author.displayAvatarURL));
                    break;
                }
                else{
                    timeToWait = parseInt(dbMine.mineLength - currentTime);
                    const timeToWaitS = timeToWait % 60;
                    timeToWait /= 60;
                    const timeToWaitM = Math.floor(timeToWait % 60);
                    timeToWait /= 60;
                    const timeToWaitH = Math.floor(timeToWait % 24);
                    embed.setColor(utils.randomHexColor('warning'));
                    description = `You have not completed your mining work yet.\nYou must wait: **${timeToWaitH}** __hours__ and **${timeToWaitM}** __minutes__.`;
                }
                break;
            case 'mine':
                // wokr for 1 hour.
                dbMine = await Mining.find({
                    where: { userID: message.author.id.toString() },
                });
                if(dbMine){
                    description = 'You are already doing a mining work.\nEither cancel it with `!mine cancel`, or check it with `!mine check`.';
                    break;
                }
                dbMine = await Mining.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                dbMine.started = parseInt(currentTime);
                dbMine.mineLength = parseInt(currentTime + 3600);
                dbMine.type = 'mine';
                await dbMine.save();
                description = 'Congratulations!\nCheck back again in an hour with `!mine check` and get your rewards!';
                break;
            case 'crystals':
                // work for 2 hours.
                dbMine = await Mining.find({
                    where: { userID: message.author.id.toString() },
                });
                if(dbMine){
                    description = 'You are already doing a mining work.\nEither cancel it with `!mine cancel`, or check it with `!mine check`.';
                    break;
                }
                dbMine = await Mining.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                dbMine.started = parseInt(currentTime);
                dbMine.mineLength = parseInt(currentTime + 7200);
                dbMine.type = 'crystal';
                await dbMine.save();
                description = 'Congratulations!\nCheck back again in an hour with `!mine check` and get your rewards!';
                break;
            case 'cancel':
                dbMine = await Mining.findOne({
                    where: { userID: message.author.id.toString() },
                });
                if(!dbMine){
                    description = 'You have not started any mining work, that I could cancel.';
                    break;
                }
                await Mining.destroy({
                    where: { userID: message.author.id.toString() },
                });
                break;
            default:
                return;
        }
        return message.reply(embed
            .setDescription(description));
        // return utils.workingEmbed(message, help.name);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};