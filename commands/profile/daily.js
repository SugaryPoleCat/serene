const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Dailies, Currencies } = require('../../database/dbObjects');

const help = {
    name: 'daily',
    description: 'Get daily! Gettng 3 days in a row will give you a **STREAK BONUS!** Geting 5 in a row will give you **BIG STREAK BONUS!**',
};
const config = {
    cooldown: 5,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed();
        const authorID = message.author.id.toString();
        const [dbDaily] = await Dailies.findOrCreate({
            where: { userID: authorID },
            defaults: { userID: authorID },
        });
        const [dbCurr] = await Currencies.findOrCreate({
            where: { userID: authorID },
            defaults: { userID: authorID },
        });
        // Get raw LINUX time in seconds and miliseconds since 1970.
        const pastTime = dbDaily.date;
        // we devide by 1000 as JS times are in milliseconds, the databases ones in seconds, though.
        const currentTime = Math.floor(new Date().getTime() / 1000);

        // Get the days since 1970
        const daysPast = Math.floor(pastTime / (60 * 60 * 24));
        const daysCurrent = Math.floor(currentTime / (60 * 60 * 24));


        if(daysPast < daysCurrent){
            const dailyBonus = 200;
            dbCurr.strawberrycubes += parseInt(dailyBonus);
            await dbCurr.save();
            dbDaily.date = currentTime;
            dbDaily.streak += parseInt(1);
            await dbDaily.save();
            let description;
            description = `You got a daily bonus of: **${dailyBonus}** Strawberrycubes!\nYou now have **${dbCurr.strawberrycubes}** Strawberrycubes!`;

            // this checks if we have the right amount of streak days.
            // If its anything else, you lose streak.
            // To be generous keep at 2.
            const streakDays = daysCurrent - daysPast;
            if(streakDays <= 2){
                dbDaily.streak += parseInt(1);
                description += `\n\nYour current __STREAK__ is: **${dbDaily.streak}**.`;
                await dbDaily.save();
            }
            else{
                dbDaily.streak = parseInt(0);
                await dbDaily.save();
                description += '\n\nSorry to inform you, but you failed to use this command yesterday, your streak has been reset to **0**.';
            }

            // This checks how much streak you have.
            if(dbDaily.streak > 0 && dbDaily.streak % 5 == 0){
                const bigStreakBonus = 2500;
                const sucrosiumBonus = 50;
                dbCurr.strawberrycubes += parseInt(bigStreakBonus);
                dbCurr.sucrosium += parseInt(sucrosiumBonus);
                await dbCurr.save();
                description += `\n\n**BIG Streak!**\n\nYou are on your **${dbDaily.streak}** streak! Here's a big bonus of **${bigStreakBonus}** Strawberrycubes and **${sucrosiumBonus}** Sucrosium!`;
            }
            else if(dbDaily.streak > 0 && dbDaily.streak % 3 == 0){
                const streakBonus = 750;
                dbCurr.strawberrycubes += parseInt(streakBonus);
                await dbCurr.save();
                description += `\n\n**Streak!**\n\nYou are on your **${dbDaily.streak}** streak! Here's a little bonus of **${streakBonus}** Strawberrycubes!`;
            }

            // PRINT
            embed
                .setTitle('DAILY')
                .setTimestamp()
                .setThumbnail(message.author.displayAvatarURL)
                .setColor(utils.memberColor(message, message.member))
                .setDescription(description);
            return message.channel.send(embed);
        }

        // If you use command too early.
        else{
            const nextDay = daysCurrent + 1;
            const nextTime = nextDay * 60 * 60 * 24;
            let waitTime = nextTime - currentTime;
            const waitTimeS = waitTime % 60;
            waitTime /= 60;
            const waitTimeM = Math.floor(waitTime) % 60;
            waitTime /= 60;
            const waitTimeH = Math.floor(waitTime);

            embed.setTitle('DAILY')
                .setTimestamp()
                .setThumbnail(message.author.displayAvatarURL)
                .setColor(utils.randomHexColor('error'))
                .setDescription(`You still have to wait = ${waitTimeH} hours and ${waitTimeM} minutes!`);
            return message.channel.send(embed);
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};