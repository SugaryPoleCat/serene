const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'join',
    description: 'just use the command and follow the further instructions! It says what you need to type to join a role.',
    usage: 'Write what the command says to write and follow it\'s steps.',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    delete: true,
    nsfw: false,
};

 // lets get it on!
const arr_species = ['Unicorn', 'Earth pony', 'Pegasus', 'Alicorn', 'Changeling', 'Batpony', 'Wolf', 'Fox', 'Other', 'Horse'];
const arr_gender = ['Male', 'Female', 'Stalion', 'Mare', 'Other gender'];
const arr_access = ['NSFW', 'RP', 'NSFW memes'];
const arr_profession = ['Artisans', 'Cooks', 'Maids', 'Hospital staff'];

const type_match = ['Species', 'Gender', 'Access', 'Job'];

const arr_things = ['Unicorn', 'Earth pony', 'Pegasus', 'Alicorn', 'Changeling', 'Batpony', 'Wolf', 'Fox', 'Other', 'Horse', 'Male', 'Female', 'Stalion', 'Mare', 'Other gender', 'Artisans', 'Cooks', 'Maids', 'Hospital staff', 'RP', 'NSFW'];
let embed_desc = '';
// Response
let r_type = '';
let r_what = '';
const arrayOfRoles = [];
function findRoles(arrayName, message){
    for(let x = 0; x <= arrayName.length; x++){
        const foundRole = message.member.roles.find(r => r.name == arrayName[x]);
        if(foundRole) arrayOfRoles.push(foundRole.name);
    }
}
function capitalize(cap_string){
    return cap_string = cap_string.charAt(0).toUpperCase() + cap_string.slice(1);
}
// function removeRoles(r_array){
//     for (let i = 0; i < r_array.length; i++){
//         if(Member.roles.some(role => role.name == r_array[i])){
//             Member.removeRole(searchRoles(r_array[i]));}
//     }
// }
function searchRoles(roleName, message){
    const r_roleName = message.guild.roles.find(role => role.name == roleName);
    return r_roleName;
}
// function someRoles(roleName){
//     const r_role = message.member.roles.some(role =)
// }
const type = response => {
    return type_match.some(answer => answer.toLowerCase() === response.content.toLowerCase());
};
const what = response => {
    return arr_things.some(answer => answer.toLowerCase() === response.content.toLowerCase());
};

async function fox(message, args, client){
    const intro_embed = new Discord.RichEmbed()
            .setTitle('JOIN')
            .setThumbnail(message.author.avatarURL)
            .setColor(message.member.displayHexColor)
            .setDescription(`What do you wish to join? You can join:\n\`\`\`${type_match.join(', ')}\`\`\`\nI'm waiting 15 seconds for you to pick, before canceling.`);
    await message.channel.send(intro_embed);
    try{
        const collected = await message.channel.awaitMessages(type, { maxMatches: 1, time: 15000, errors: ['time'] });
        r_type = collected.first().content.toLowerCase();
        console.log('r_type : ' + r_type);
        if(r_type == 'species'){
            findRoles(arr_species, message);
            if(arrayOfRoles.length == 0){
                arrayOfRoles.push('None');
            }
            embed_desc = `\nYou can join: \`\`\`${arr_species.join(', ')}\`\`\`\n\nYou can leave: \`\`\`${arrayOfRoles.join(', ')}\`\`\`\nI'm waiting 10 seconds for you to pick, before canceling.`;
            message.member.addRole(searchRoles('=== SPECIES ===', message));
        }
        else if(r_type == 'gender'){
            findRoles(arr_gender, message);
            if(arrayOfRoles.length == 0){
                arrayOfRoles.push('None');
            }
            embed_desc = `\nYou can join: \`\`\`${arr_gender.join(', ')}\`\`\`\n\nYou can leave: \`\`\`${arrayOfRoles.join(', ')}\`\`\`\nI'm waiting 10 seconds for you to pick, before canceling.`;
            message.member.addRole(searchRoles('=== GENDER ===', message));
        }
        else if(r_type == 'access'){
            findRoles(arr_access, message);
            if(arrayOfRoles.length == 0){
                arrayOfRoles.push('None');
            }
            embed_desc = `\nYou can join: \`\`\`${arr_access.join(', ')}\`\`\`\n\nYou can leave: \`\`\`${arrayOfRoles.join(', ')}\`\`\`\nI'm waiting 10 seconds for you to pick, before canceling.`;
            message.member.addRole(searchRoles('=== ACCESS ROLES ===', message));
        }
        else if(r_type == 'job'){
            findRoles(arr_profession, message);
            if(arrayOfRoles.length == 0){
                arrayOfRoles.push('None');
            }
            embed_desc = `\nYou can join: \`\`\`${arr_profession.join(', ')}\`\`\`\n\nYou can leave: \`\`\`${arrayOfRoles.join(', ')}\`\`\`\nI'm waiting 15 seconds for you to pick, before canceling.`;
            message.member.addRole(searchRoles('=== PROFESSION ===', message));
        }
        const type_embed = new Discord.RichEmbed()
            .setTitle('JOIN')
            .setColor(message.member.displayHexColor)
            .setDescription(embed_desc);
        await message.channel.send(type_embed);
        const collected2 = await message.channel.awaitMessages(what, { maxMatches: 1, time: 15000, errors: ['time'] });
        r_what = collected2.first().content.toLowerCase();
        let description;
        if(message.member.roles.find(r => r.name == capitalize(r_what))){
            await message.member.removeRole(searchRoles(capitalize(r_what), message));
            description = `You __**left**__: **${r_what}**, from **${r_type}**`;
        }
        else{
            await message.member.addRole(searchRoles(capitalize(r_what), message));
            description = `You __**joined**__: **${r_what}**, from **${r_type}**`;
        }
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setThumbnail(message.author.displayAvatarURL)
            .setColor(message.member.displayHexColor)
            .setTitle('JOIN')
            .setDescription(description);
        await message.channel.send(embed);
    }
    catch(err){
        const cancel_embed = new Discord.RichEmbed()
            .setThumbnail(message.author.avatarURL)
            .setTitle('JOIN')
            .setColor(message.member.displayHexColor)
            .setDescription(`${message.author}, looks like you did not want to join anyhting.`);
        return message.reply(cancel_embed);
    }
}

module.exports = {
    help, config, fox,
};