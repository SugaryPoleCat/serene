const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Levels } = require('../../database/dbObjects');

const help = {
    name: 'level',
    description: 'Check your current level and experience.',
    aliases: ['lvl'],
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const target = message.mentions.users.first() || message.author;

        const [dbLevel] = await Levels.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        return message.reply(new Discord.RichEmbed()
            .setTitle('LEVEL')
            .setColor(utils.memberColor(message, target))
            .setTimestamp()
            .setThumbnail(target.displayAvatarURL)
            .setDescription(`Your level is: **${dbLevel.level}**.\n\nYou have **${dbLevel.exp}** Experience and you will reach next level at **${dbLevel.maxExp}** Experience.\nYou still need **${dbLevel.maxExp - dbLevel.exp}** Experience.`));
        // I want also to include how much experince you get per message from settings file. Wold be cool if we could make them global.
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};