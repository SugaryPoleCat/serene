const Discord = require('discord.js');
const utils = require('../../utils');
const { Currencies, Reputation, Levels, Reports, Warnings, Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'profile',
    description: '',
    aliases: [''],
    usage: '<@target> <what> <amount>',
};
const config = {
    cooldown: 7,
    guildOnly: false,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let targetText;
        const target = message.mentions.users.first() || message.author;
        const embed = new Discord.RichEmbed()
            .setTitle('PROFILE')
            .setColor(utils.memberColor(message, target));
        const msg = await message.reply(embed
            .setDescription('Fetching profile...'));
        const [dbCurr] = await Currencies.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        const strawberrycubes = await dbCurr.strawberrycubes;
        const sucrosium = await dbCurr.sucrosium;

        const [reputation] = await Reputation.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        const rep = reputation.amount;

        const [dbExp] = await Levels.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        const level = await dbExp.level;
        const exp = await dbExp.exp;
        const maxExp = await dbExp.maxExp;
        const expToLevel = await maxExp - exp;

        const reports = await Reports.count({
            where: { reportedID: target.id.toString() },
        });

        const warns = await Warnings.count({
            where: { userID: target.id.toString() },
        });

        const [dbSucrons] = await Sucrons.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });

        const sucrons = await utils.sucronsCalc(target);
        dbSucrons.sucrons = await parseInt(sucrons);
        await dbSucrons.save();

        if(target == message.author.id) targetText = 'YOU';
        else if(target != message.author.id) targetText = 'THEY';

        await message.reply(embed
            .setTimestamp()
            .setThumbnail(target.displayAvatarURL)
            .setDescription(`**__${targetText} HAVE__**\n
            **__CURRENCIES__**
            Strawberrycubes: **${strawberrycubes}**
            Sucrosium: **${sucrosium}**\n
            **__REPUTATION__**
            Reputation: **${rep}**
            Reports: **${reports}**
            Warns: **${warns}**\n
            **__LEVEL__**
            Level: **${level}**
            Experience: **${exp}**
            Next level at: **${maxExp}**
            Experience needed to levelup: **${expToLevel}**\n
            **__POWER__**
            Sucrons: **${sucrons}**`));
        return msg.delete();
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};