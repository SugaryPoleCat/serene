const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const crafting = require('../../config/crafting.json')['stuff'];

const help = {
    name: 'craft',
    description: '',
    aliases: [''],
    usage: '<list | do>\nDO: also requires <id>, like so: `!craft do 1`.',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('CRAFT')
            .setColor(utils.randomHexColor('error'));
        const what = args[0];
        switch(what){
            case 'list':
                message.reply(`ugh`);
                break;
            case 'do':
                break;
            default:
                return message.reply(embed
                    .setDescription(`The following **${what}** case has not been found.\n\nPlease try \`list\`, or \`do\`.`));
        }
        return utils.workingEmbed(message, help.name);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};