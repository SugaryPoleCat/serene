const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Currencies } = require('../../database/dbObjects');

const help = {
    name: 'balance',
    description: 'This will display the amount of currencies you have.',
    aliases: ['bal', 'curr'],
    usage: '[@someone]',
};
const config = {
    cooldown: 7,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const target = message.mentions.users.first() || message.author;
        const [dbCurr] = await Currencies.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        let person;
        if(target.id != message.author.id) person = 'Their';
        else if(target.id == message.author.id) person = 'Your';
        return message.reply(new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('BALANCE')
            .setColor(utils.memberColor(message, target))
            .setThumbnail(target.displayAvatarURL)
            .setDescription(`${person} current balance is:\n
            __Strawberrycubes:__ **${dbCurr.strawberrycubes}**
            __Sucrosium:__ **${dbCurr.sucrosium}**`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};