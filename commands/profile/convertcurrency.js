const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Currencies } = require('../../database/dbObjects');

const help = {
    name: 'convertcurrency',
    description: 'Convert your hard earned Cubes or Sucrosium into each other.\nThe conversion rate is: 500 cubes = 1 sucrosium.',
    aliases: ['convertcurr', 'convcurr', 'currconv'],
    usage: '<cubes (to cubes) | sucrosium (to sucrosium)> <amount>',
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('CURRENCY CONVERSION');
        const what = args[0];
        let amount, dbCurr, convertedAmount, dbAmountlol;
        if(args[1] != undefined || isNaN(args[1]) || !args[1]) amount = parseInt(args[1]);
        else return message.reply(embed
            .setColor(utils.randomHexColor('warning'))
            .setDescription('Amount you provided is not correct format, or you did not provide it at all.'));
        switch(what){
            case 'sucrosium':
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(dbCurr.strawberrycubes == 0) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You have **0** Strawberrycubes to convert.'));
                if(amount > dbCurr.strawberrycubes) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You can not convert more Strawberrycubes, than what you currently have.'));
                convertedAmount = Math.floor(amount / 500);
                dbCurr.sucrosium += convertedAmount;
                dbCurr.strawberrycubes -= (convertedAmount * 500);
                await dbCurr.save();
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`You have succesfully converted **${convertedAmount * 500}** __Strawberrycubes__ into **${convertedAmount}** __Sucrosium__.\n\nYou now have: **${dbCurr.strawberrycubes}**__ Strawberycubes__ and **${dbCurr.sucrosium}**__ Sucrosium__!`));
            case 'cubes':
                [dbCurr] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(dbCurr.sucrosium == 0) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You have **0** Sucrosium to convert.'));
                if(amount > dbCurr.sucrosium) return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You can not convert more Sucrosium, than what you currently have.'));
                dbCurr.sucrosium -= amount;
                dbCurr.strawberrycubes += amount * 500;
                await dbCurr.save();
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription(`You have succesfully converted **${amount}** __Sucrosium__ into **${amount * 500}** __Strawberrycubes__.\n\nYou now have: **${dbCurr.strawberrycubes}**__ Strawberycubes__ and **${dbCurr.sucrosium}**__ Sucrosium__!`));
            default:
                return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('I did not find the currency you wish to convert.'));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};