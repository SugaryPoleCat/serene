const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Reputation } = require('../../database/dbObjects');

const help = {
    name: 'reputation',
    description: 'If you like someone a lot, give them some rep! It will increase their power and will also maybe make them notice you!',
    aliases: ['rep'],
    usage: '<@someone>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

const embed = new Discord.RichEmbed();

async function fox(message, args, client){
    try{
        const target = message.mentions.users.first();
        const Member = message.guild.members.find(u => u.id == target.id);
        const [dbRep] = await Reputation.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        const [dbUser] = await Reputation.findOrCreate({
            where: { userID: message.author.id.toString() },
            defaults: { userID: message.author.id.toString() },
        });
        // Get raw LINUX time in seconds and miliseconds since 1970.
        const pastTime = dbUser.date;
        // we devide by 1000 as JS times are in milliseconds, the databases ones in seconds, though.
        const currentTime = Math.floor(new Date().getTime() / 1000);

        // Get the days since 1970
        const daysPast = Math.floor(pastTime / (60 * 60 * 24));
        const daysCurrent = Math.floor(currentTime / (60 * 60 * 24));

        if(daysPast < daysCurrent){
            const rep = 1;
            dbRep.amount += parseInt(rep);
            dbUser.date = currentTime;
            await dbUser.save();
            await dbRep.save();
            const description = `You gave ${target} **${rep}** __reputation points__!\n\nThey now have **${dbRep.amount}** __Reputation__!\nThey have grown in strength.`;

            embed
                .setTitle('REPUTATION')
                .setTimestamp()
                .setThumbnail(target.displayAvatarURL)
                .setColor(Member.displayHexColor)
                .setDescription(description);
            return message.channel.send(embed);
        }
        else{
            const nextDay = daysCurrent + 1;
            const nextTime = nextDay * 60 * 60 * 24;
            let waitTime = nextTime - currentTime;
            const waitTimeS = waitTime % 60;
            waitTime /= 60;
            const waitTimeM = Math.floor(waitTime) % 60;
            waitTime /= 60;
            const waitTimeH = Math.floor(waitTime);

            embed.setTitle('REPUTATION')
                .setTimestamp()
                .setThumbnail(message.author.displayAvatarURL)
                .setColor(utils.randomHexColor('error'))
                .setDescription(`You still have to wait: **${waitTimeH}** __hours__ and **${waitTimeM}** __minutes__!`);
            return message.reply(embed);
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};