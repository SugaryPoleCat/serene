const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'r34',
    description: 'Test!',
    aliases: [''],
    usage: '',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: true,
};

async function fox(message, args, client){
    try{
        return utils.workingEmbed(message, help.name);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};