const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const numeral = require('numeral');

const help = {
    name: 'convertdata',
    description: 'This will convert the provided amount of bits, bytes or whatever into every other byte and bit, up to Peta, in per second format, per minute and per hour.\nIf you get a **NaN** anywhere, the value is SOOOOO SMAAAALL, the number formatter can\'t process it! Working on a fix for that.',
    aliases: ['convdata', 'convdtt'],
    usage: '<b (small b means bit) | B (big b means bytes) | kb (kilobit) | KB (kilobyte) | mb (megabit) | MB (megabyte) | gb (gigabit) | GB (gigabyte) | tb (terrabit) | TB (terrabyte) | pb (petabit) | PB (petabyte)',
};
const config = {
    cooldown: 7,
    args: true,
    delete: true,
    guildOnly: false,
    nsfw: false,
};

function bytesToBits(bytes){
    return bytes * 8;
}

function bitsToBytes(bits){
    return bits / 8;
}

function bytesToKilo(bytes){
    return bytes / 1024;
}

function kiloToBytes(bytes){
    return bytes * 1024;
}

function toMinutes(bitin){
    return bitin * 60;
}

function toHours(bitin){
    return bitin * 60 * 60;
}

async function fox(message, args, client){
    try{
        const what = args[0];
        const amount = parseFloat(args[1]);

        let petaBytes, petaBits, terraBytes, terraBits, gigaBytes, gigaBits, megaBytes, megaBits, kiloBytes, kiloBits, bytes, bits;
        switch(what){
            case 'b':
                bits = amount;
                bytes = bitsToBytes(bits);
                kiloBits = bytesToKilo(bits);
                kiloBytes = bytesToKilo(bytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'B':
                bytes = amount;
                bits = bytesToBits(bytes);
                kiloBits = bytesToKilo(bits);
                kiloBytes = bytesToKilo(bytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'kb':
                kiloBits = amount;
                kiloBytes = bitsToBytes(kiloBits);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'KB':
                kiloBytes = amount;
                kiloBits = bytesToBits(kiloBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'mb':
                megaBits = amount;
                megaBytes = bitsToBytes(megaBits);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'MB':
                megaBytes = amount;
                megaBits = bytesToBits(megaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'gb':
                gigaBits = amount;
                gigaBytes = bitsToBytes(gigaBits);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'GB':
                gigaBytes = amount;
                gigaBits = bytesToBits(gigaBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'tb':
                terraBits = amount;
                terraBytes = bitsToBytes(terraBits);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'TB':
                terraBytes = amount;
                terraBits = bytesToBits(terraBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'pb':
                petaBits = amount;
                petaBytes = bitsToBytes(petaBits);
                terraBits = kiloToBytes(petaBits);
                terraBytes = kiloToBytes(petaBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                break;
            case 'PB':
                petaBytes = amount;
                petaBits = bytesToBits(petaBytes);
                terraBits = kiloToBytes(petaBits);
                terraBytes = kiloToBytes(petaBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                break;
        }
        return message.reply(new Discord.RichEmbed()
            .setColor(utils.memberColor(message, message.member))
            .setTitle('BITS AND BYTES')
            .setTimestamp()
            .setDescription(`**${amount}** ${what} is:\n\n
            Bits/s: **${numeral(bits).format('0,0.00')}** bps
            Bits/m: **${numeral(toMinutes(bits)).format('0,0.00')}** bpm
            Bits/h: **${numeral(toHours(bits)).format('0,0.00')}** bph\n
            Kilobits/s: **${numeral(kiloBits).format('0,0.00')}** Kbps
            Kilobits/m: **${numeral(toMinutes(kiloBits)).format('0,0.00')}** Kbpm
            Kilobits/h: **${numeral(toHours(kiloBits)).format('0,0.00')}** Kbph\n
            Megabits/s: **${numeral(megaBits).format('0,0.00')}** Mbps
            Megabits/m: **${numeral(toMinutes(megaBits)).format('0,0.00')}** Mbpm
            Megabits/h: **${numeral(toHours(megaBits)).format('0,0.00')}** Mbph\n
            Gigabits/s: **${numeral(gigaBits).format('0,0.00')}** Gbps
            Gigabits/m: **${numeral(toMinutes(gigaBits)).format('0,0.00')}** Gbpm
            Gigabits/h: **${numeral(toHours(gigaBits)).format('0,0.00')}** Gbph\n
            Terrabits/s: **${numeral(terraBits).format('0,0.00')}** Tbps
            Terrabits/m: **${numeral(toMinutes(terraBits)).format('0,0.00')}** Tbpm
            Terrabits/h: **${numeral(toHours(terraBits)).format('0,0.00')}** Tpbh\n
            Petabits/s: **${numeral(petaBits).format('0,0.00')}** Pbps
            Petabits/m: **${numeral(toMinutes(petaBits)).format('0,0.00')}** Pbpm
            Petabits/h: **${numeral(toHours(petaBits)).format('0,0.00')}** Pbph\n\n\n
            Bytes/s: **${numeral(bytes).format('0,0.00')}** B/s
            Bytes/m: **${numeral(toMinutes(bytes)).format('0,0.00')}** B/m
            Bytes/h: **${numeral(toHours(bytes)).format('0,0.00')}** B/h\n
            KiloBytes/s: **${numeral(kiloBytes).format('0,0.00')}** KB/s
            KiloBytes/m: **${numeral(toMinutes(kiloBytes)).format('0,0.00')}** KB/m
            KiloBytes/h: **${numeral(toHours(kiloBytes)).format('0,0.00')}** KB/h\n
            MegaBytes/s: **${numeral(megaBytes).format('0,0.00')}** MB/s
            MegaBytes/m: **${numeral(toMinutes(megaBytes)).format('0,0.00')}** MB/m
            MegaBytes/h: **${numeral(toHours(megaBytes)).format('0,0.00')}** MB/h\n
            GigaBytes/s: **${numeral(gigaBytes).format('0,0.00')}** GB/s
            GigaBytes/m: **${numeral(toMinutes(gigaBytes)).format('0,0.00')}** GB/m
            GigaBytes/h: **${numeral(toHours(gigaBytes)).format('0,0.00')}** GB/h\n
            TerraBytes/s: **${numeral(terraBytes).format('0,0.00')}** TB/s
            TerraBytes/m: **${numeral(toMinutes(terraBytes)).format('0,0.00')}** TB/m
            TerraBytes/h: **${numeral(toHours(terraBytes)).format('0,0.00')}** TB/h\n
            PetaBytes/s: **${numeral(petaBytes).format('0,0.00')}** PB/s
            PetaBytes/m: **${numeral(toMinutes(petaBytes)).format('0,0.00')}** PB/m
            PetaBytes/h: **${numeral(toHours(petaBytes)).format('0,0.00')}** PB/h`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};