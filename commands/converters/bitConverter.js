const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const numeral = require('numeral');

const help = {
    name: 'convertbit',
    description: 'This will convert the provided amount of bits, bytes or whatever into every other byte and bit, up to Peta.\nIf you get a **NaN** anywhere, the value is SOOOOO SMAAAALL, the number formatter can\'t process it! Working on a fix for that.',
    aliases: ['convbit'],
    usage: '<b (small b means bit) | B (big b means bytes) | kb (kilobit) | KB (kilobyte) | mb (megabit) | MB (megabyte) | gb (gigabit) | GB (gigabyte) | tb (terrabit) | TB (terrabyte) | pb (petabit) | PB (petabyte)',
};
const config = {
    cooldown: 7,
    args: true,
    delete: true,
    guildOnly: false,
    nsfw: false,
};

/*
Bit converter, is a mess.

at some point I want to remake it better.
*/

function bytesToBits(bytes){
    const result = bytes * 8;
    return result;
}

function bitsToBytes(bits){
    const result = bits / 8;
    return result;
}

function bytesToKilo(bytes){
    const result = bytes / 1024;
    return result;
}

function kiloToBytes(bytes){
    const result = bytes * 1024;
    return result;
}

async function fox(message, args, client){
    try{
        const what = args[0];
        const amount = parseInt(args[1]);

        const embed = new Discord.RichEmbed()
            .setColor(utils.memberColor(message, message.member))
            .setTitle('BITS AND BYTES')
            .setTimestamp();

        let petaBytes, petaBits, terraBytes, terraBits, gigaBytes, gigaBits, megaBytes, megaBits, kiloBytes, kiloBits, bytes, bits;
        switch(what){
            case 'b':
                bits = amount;
                bytes = bitsToBytes(bits);
                kiloBits = bytesToKilo(bits);
                kiloBytes = bytesToKilo(bytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'B':
                bytes = amount;
                bits = bytesToBits(bytes);
                kiloBits = bytesToKilo(bits);
                kiloBytes = bytesToKilo(bytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'kb':
                kiloBits = amount;
                kiloBytes = bitsToBytes(kiloBits);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'KB':
                kiloBytes = amount;
                kiloBits = bytesToBits(kiloBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                megaBits = bytesToKilo(kiloBits);
                megaBytes = bytesToKilo(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'mb':
                megaBits = amount;
                megaBytes = bitsToBytes(megaBits);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'MB':
                megaBytes = amount;
                megaBits = bytesToBits(megaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                gigaBits = bytesToKilo(megaBits);
                gigaBytes = bytesToKilo(megaBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'gb':
                gigaBits = amount;
                gigaBytes = bitsToBytes(gigaBits);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'GB':
                gigaBytes = amount;
                gigaBits = bytesToBits(gigaBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                terraBits = bytesToKilo(gigaBits);
                terraBytes = bytesToKilo(gigaBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'tb':
                terraBits = amount;
                terraBytes = bitsToBytes(terraBits);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'TB':
                terraBytes = amount;
                terraBits = bytesToBits(terraBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                petaBits = bytesToKilo(terraBits);
                petaBytes = bytesToKilo(terraBytes);
                break;
            case 'pb':
                petaBits = amount;
                petaBytes = bitsToBytes(petaBits);
                terraBits = kiloToBytes(petaBits);
                terraBytes = kiloToBytes(petaBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                break;
            case 'PB':
                petaBytes = amount;
                petaBits = bytesToBits(petaBytes);
                terraBits = kiloToBytes(petaBits);
                terraBytes = kiloToBytes(petaBytes);
                gigaBits = kiloToBytes(terraBits);
                gigaBytes = kiloToBytes(terraBytes);
                megaBits = kiloToBytes(gigaBits);
                megaBytes = kiloToBytes(gigaBytes);
                kiloBits = kiloToBytes(megaBits);
                kiloBytes = kiloToBytes(megaBytes);
                bits = kiloToBytes(kiloBits);
                bytes = kiloToBytes(kiloBytes);
                break;
            default:
                return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('No such bit or byte exists.'));
        }
        return message.reply(embed
            .setDescription(`**${amount}** ${what} is:\n\n
            Bits: **${numeral(bits).format('0,0.00')}** b
            Kilobits: **${numeral(kiloBits).format('0,0.00')}** Kb
            Megabits: **${numeral(megaBits).format('0,0.00')}** Mb
            Gigabits: **${numeral(gigaBits).format('0,0.00')}** Gb
            Terrabits: **${numeral(terraBits).format('0,0.00')}** Tb
            Petabits: **${numeral(petaBits).format('0,0.00')}** Pb\n\n
            Bytes: **${numeral(bytes).format('0,0.00')}** B
            KiloBytes: **${numeral(kiloBytes).format('0,0.00')}** KB
            MegaBytes: **${numeral(megaBytes).format('0,0.00')}** MB
            GigaBytes: **${numeral(gigaBytes).format('0,0.00')}** GB
            TerraBytes: **${numeral(terraBytes).format('0,0.00')}** TB
            PetaBytes: **${numeral(petaBytes).format('0,0.00')}** PB\n\n
            If you want to know your data transfer, this can work as a data counter.
            2 Mbps is 0.25 MBps.\nOr let me count it for you with \`!dtt\`. It's used exactly the same way this is used.`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};