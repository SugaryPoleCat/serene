const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const numeral = require('numeral');

const help = {
    name: 'convertmass',
    description: 'This will convert a mass, into other masses!',
    aliases: ['convmass'],
    usage: '<kg | pound> <mass in numbers, can be decimal>',
};
const config = {
    cooldown: 5,
    args: true,
    delete: true,
    guildOnly: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let kg, pound;
        const what = args[0];
        const amount = parseFloat(args[1]);
        const embed = new Discord.RichEmbed()
            .setColor(utils.memberColor(message, message.member))
            .setTimestamp()
            .setTitle('MASS CONVERSION');

        switch(what){
            case 'kg':
                kg = amount;
                pound = kg * 2.205;
                break;
            case 'pound':
                pound = amount;
                kg = pound / 2.205;
                break;
            default:
                return message.reply(embed
                    .setDescription('No such mass exists!')
                    .setColor(utils.randomHexColor('error')));
        }
        return message.reply(embed
            .setDescription(`A **${amount}** of ${what} is:\n\n**KG: **${numeral(kg).format('0,0.000')}
            **Pounds: **${numeral(pound).format('0,0.000')}`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};