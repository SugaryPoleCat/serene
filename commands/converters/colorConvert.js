const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const Canvas = require('canvas');
const snekfetch = require('snekfetch');

const help = {
    name: 'colorconvert',
    description: 'This will convert a **RGB** color to HEX and **HEX** to RGB and will display the colour as well!',
    aliases: ['convcolor', 'convclr'],
    usage: '<rgb | hex>\nFor RGB: <r> <g> <b> seperate each number by space.\nFor HEX: <hexcode | #hexcode>, it can either be ffffff, or #ffffff.',
};
const config = {
    cooldown: 5,
    args: true,
    delete: true,
    guildOnly: false,
    nsfw: false,
};
function RGBtoHex(rgb){
    let hex = Number(rgb).toString(16);
    if (hex.length < 2) {
        hex = '0' + hex;
    }
    return hex;
}
function fullColorHex(r, g, b){
    const red = RGBtoHex(r);
    const green = RGBtoHex(g);
    const blue = RGBtoHex(b);
    return red + green + blue;
}

function hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
    } : null;
}

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('COLOUR CONVERTER');
        const what = args[0];
        let red, green, blue, rgb, displayclr;
        switch(what){
            case 'rgb':
                red = parseInt(args[1]);
                green = parseInt(args[2]);
                blue = parseInt(args[3]);
                if(red || green || blue > 255){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('error'))
                        .setDescription('Please provide no more than **255** for each colour.'));
                }
                if(red || green || blue < 0){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('error'))
                        .setDescription('Please provide no less than **0** for each colour.'));
                }
                return message.reply(embed
                    .setColor(fullColorHex(red, green, blue))
                    .setDescription(`Your color, **RGB(${red}, ${green}, ${blue})** in HEX is: **#${fullColorHex(red, green, blue)}**.`));
            case 'hex':
                displayclr = args[1];
                if(displayclr[0] == '#'){
                    displayclr.slice(1);
                }
                if(args[1].length > 7){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('error'))
                        .setDescription(`**${args[1]}** is not a correct HEX format.\nThe correct hex format would be: **${args[1].slice(0, 6)}**`));
                }
                rgb = hexToRgb(args[1]);
                return message.reply(embed
                    .setColor(displayclr)
                    .setDescription(`Your color, **HEX ${args[1]}** in RGB is: **${rgb.r}, ${rgb.g}, ${rgb.b}**.`));
            default:
                return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('You must either use **rgb** or **hex** please.'));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};