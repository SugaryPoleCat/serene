const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const numeral = require('numeral');

const help = {
    name: 'convertlength',
    description: 'This will convert a length, into other lengths!',
    aliases: ['convlength', 'convlen'],
    usage: '<km | miles> <distance in numbers, can be decimal>',
};
const config = {
    cooldown: 5,
    args: true,
    delete: true,
    guildOnly: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let km, mil;
        const what = args[0];
        const amount = parseFloat(args[1]);
        const embed = new Discord.RichEmbed()
            .setColor(utils.memberColor(message, message.member))
            .setTimestamp()
            .setTitle('LENGTH CONVERSION');

        switch(what){
            case 'km':
                km = amount;
                mil = km / 1.609;
                break;
            case 'miles':
                mil = amount;
                km = mil * 1.609;
                break;
            default:
                return message.reply(embed
                    .setDescription('No such length exists!')
                    .setColor(utils.randomHexColor('error')));
        }
        return message.reply(embed
            .setDescription(`A **${amount}** of ${what} is:\n\n**KM: **${numeral(km).format('0,0.000')}
            **Miles: **${numeral(mil).format('0,0.000')}`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};