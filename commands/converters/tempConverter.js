const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const numeral = require('numeral');

const help = {
    name: 'converttemperature',
    description: 'This will convert a temperature provided, into other temperatures!',
    aliases: ['convtemp'],
    usage: '<celsius | f (fahrenheit) | kelvin> <temperature in numbers, can be decimal>',
};
const config = {
    cooldown: 5,
    args: true,
    delete: true,
    guildOnly: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let fah, cel, kel;
        let reply;
        let what = args[0];
        const amount = parseFloat(args[1]);
        const embed = new Discord.RichEmbed()
            .setColor(utils.memberColor(message, message.member))
            .setTimestamp()
            .setTitle('TEMPERATURE CONVERSION');

        switch(what){
            case 'celsius':
                cel = amount;
                fah = (cel * 9/5) + 32;
                kel = cel + 273.15;
                break;
            case 'f':
                fah = amount;
                cel = (fah - 32) * 5 / 9;
                kel = (fah - 32) * 5 / 9 + 273.15;
                what = 'fahrenheit';
                break;
            case 'kelvin':
                kel = amount;
                cel = kel - 273.15;
                fah = (kel - 273.15) * 9 / 5 + 32;
                break;
            default:
                return message.reply(embed
                    .setDescription('No such temperature exists!')
                    .setColor(utils.randomHexColor('error')));
        }
        return message.reply(embed
            .setDescription(`A **${amount}** of ${what} is:\n\n**Celsius: **${numeral(cel).format('0,0.000')}
            **Fahrenheit: **${numeral(fah).format('0,0.000')}
            **Kelvin: **${numeral(kel).format('0,0.000')}`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};