const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const settings = require('../../config/settings.json');
const { Reports } = require('../../database/dbObjects');

const help = {
    name: 'report',
    description: 'If someone is being naughty and you dislike it, use this quick command to report them! Who reported, when and why, will be logged just for the staff.',
    usage: '<my cool report cause>\nYOU MUST provide this text.',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('REPORT')
            .setTimestamp();
        const target = message.mentions.users.first();
        if(target == undefined){
            return message.channel.send(embed
                .setColor(utils.randomHexColor('error'))
                .setDescription('I need a mention of a user to report, baka.'));
        }
        if(target == message.author.id){
            return message.channel.send(embed
                .setColor(utils.randomHexColor('error'))
                .setDescription('You--- you neanderthal. You can NOT report yourself.'));
        }
        const cutLength = 8 + target.id.length + 3;
        const reason = message.content.slice(cutLength);
        if(reason == null){
            return message.channel.send(embed
                .setColor(utils.randomHexColor('error'))
                .setDescription('You NEED to provide a reason.'));
        }
        const logChannel = message.guild.channels.find(ch => ch.name == settings.channels.logBot);
        const currentTime = Math.floor(new Date().getTime() / 1000);
        const [dbRep] = await Reports.findOrCreate({
            where: { reportedID: target.id.toString(), reporterID: message.author.id.toString() },
            default: { reportedID: target.id.toString(), reporterID: message.author.id.toString() },
        });
        dbRep.text = reason;
        dbRep.date = currentTime;
        await dbRep.save();
        await logChannel.send(embed
            .setColor(message.member.displayHexColor)
            .setDescription(`${message.author} reported ${target} for:\n\`\`\`${reason}\`\`\`.\n\n**AUTHOR**\n[TAG]: ${message.author.tag} [ID]: ${message.author.id}\n\n**TARGET**\n[TAG]: ${target.tag} [ID]: ${target.id}`));
        return message.channel.send(embed
            .setColor(message.member.displayHexColor)
            .setDescription(`You have reported **${target}**.\nReport has been logged and staff will look into it.`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};