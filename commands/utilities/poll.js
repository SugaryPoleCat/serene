const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'poll',
    description: 'Create a poll quickly, other\'s can vote on!',
    usage: '<text>\nOR\n<{title} (please put title between { } brackets, else it won\'t work)> <[option1] (please put text for choice 1, between [ ] brackets, else it won\'t work)> [[option2]] etc...\n You can have up to 10 options. You **MUST** put title between { } brackets or else it won\'t work. Each option has to be between [ ] brackets.',
};
const config = {
    cooldown: 10,
    guildOnly: true,
    args: false,
    delete: true,
    nsfw: false,
};

const emojiNumbers = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣', '🔟'];
const emojiLetters = ['🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮', '🇯'];

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('POLL')
            .setTimestamp();
        // Cut lengt = THE PREFIX + NAME or ALIAS actually.... + 1 for space.
        const cutLength = botconfig.prefix.length + help.name.length + 1;
        const curatedContent = message.content.substring(cutLength);
        if(curatedContent == null || !curatedContent || curatedContent == '!poll'){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription(`${message.author}, please input some text after \`!poll\`.`));
        }
        // So this will check if the VERY first thing for the curated content string, is {
        if(curatedContent[0] == '{'){
            let matches = curatedContent.match(/^{([^}]+)}(?:\s*\[([^\]]+)\])+/);
            if(!matches){
                return message.reply(embed
                    .setColor(utils.randomHexColor('warning'))
                    .setDescription(`${message.author}, please use the CORRECT formatting.`));
            }
            const title = matches[1];
            const options = [];
            const optionsRegex = /\s*\[([^\]]+)\]/g;
            while(matches = optionsRegex.exec(curatedContent)){
                options.push(matches[1]);
            }
            if(options.length > 10){
                return message.reply(embed
                    .setColor(utils.randomHexColor('warning'))
                    .setDescription(`${message.author}, please do not provide more than **10** options.`));
            }
            // TODO: "title" holds the poll title, "options" the array of all the options
            // console.log('POLL WITH OPTIONS:', title, options);
            let sendString = '';
            for(let y = 0; y < options.length; y++){
                sendString += `${emojiNumbers[y]} ${options[y]}\n`;
            }

            const sentMessage = await message.channel.send(embed
                .setTitle(title)
                .setColor(message.member.displayHexColor)
                .setDescription(sendString));
            for(let x = 0; x < options.length; x++){
                await sentMessage.react(emojiNumbers[x]);
            }
            return;
        }
        embed
            .setColor(message.member.displayHexColor)
            .setDescription(`A new poll has been created by ${message.member.displayName}\n\n${curatedContent}`);
        // return message.channel.send(embed).then(sentMessage => {
        //     sentMessage.react('👍').then(() => sentMessage.react('👎')).then(() => sentMessage.react('🤷')).catch(() => utils.errorEmbed(message, 'AAAA', help.name));
        // });
        const sentMessage = await message.channel.send(embed);
        await sentMessage.react('👍');
        await sentMessage.react('👎');
        return sentMessage.react('🤷');
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};
