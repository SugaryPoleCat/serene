const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Currencies, Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'give',
    description: 'Give someone some of your things!',
    usage: '<@someone> <cubes | sucrons | sucrosium> <amount (1-100000)>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('GIVE')
            .setTimestamp();
        const target = message.mentions.users.first();
        if(target.id == message.author.id){
            return message.reply(embed
                .setDescription('You can not give anything to yourself...')
                .setColor(utils.randomHexColor('warning'))
                .setThumbnail(message.author.displayAvatarURL));
        }
        const member = message.mentions.members.first();

        let dbTarget;
        let dbUser;

        const what = args[1];
        if(what == null){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription('You need to provide me with **WHAT** you want to give.\nYou can provide me with: `cubes`, or `sucrosium`.'));
        }
        const amount = parseInt(args[2]);
        if(isNaN(amount)){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription('The amount you want to give, has to be a **NUMBER**.'));
        }
        if(amount > 100000){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription('You can not give more than **100000**.'));
        }
        if(amount < 1){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription('You can not give less than **1**. Like... why are you even trying to give less?'));
        }
        switch(what){
            case 'cubes':
                [dbTarget] = await Currencies.findOrCreate({
                    where: { userID: target.id.toString() },
                    defaults: { userID: target.id.toString() },
                });
                [dbUser] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(amount > dbUser.strawberrycubes){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('warning'))
                        .setDescription('You can not give more than what you currently have.'));
                }
                dbTarget.strawberrycubes += parseInt(amount);
                dbUser.strawberrycubes -= parseInt(amount);
                await dbTarget.save();
                await dbUser.save();
                return message.reply(embed
                    .setColor(member.displayHexColor)
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription(`I gave ${target} **${amount}** of your Strawberrycubes.\nThey now have **${dbTarget.strawberrycubes}** Strawberrycubes, while you remain with **${dbUser.strawberrycubes}** Strawberrycubes.`));
            case 'sucrosium':
                [dbTarget] = await Currencies.findOrCreate({
                    where: { userID: target.id.toString() },
                    defaults: { userID: target.id.toString() },
                });
                [dbUser] = await Currencies.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(amount > dbUser.sucrosium){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('warning'))
                        .setDescription('You can not give more than what you currently have.'));
                }
                dbTarget.sucrosium += parseInt(amount);
                dbUser.sucrosium -= parseInt(amount);
                await dbTarget.save();
                await dbUser.save();
                return message.reply(embed
                    .setColor(member.displayHexColor)
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription(`I gave ${target} **${amount}** of your Sucrosium.\nThey now have **${dbTarget.sucrosium}** Sucrosium, while you remain with **${dbUser.sucrosium}** Sucrosium.`));
            case 'sucrons':
                [dbTarget] = await Sucrons.findOrCreate({
                    where: { userID: target.id.toString() },
                    defaults: { userID: target.id.toString() },
                });
                [dbUser] = await Sucrons.findOrCreate({
                    where: { userID: message.author.id.toString() },
                    defaults: { userID: message.author.id.toString() },
                });
                if(amount > dbUser.eaten){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('warning'))
                        .setDescription('You can not share more Sucrons than what you currently have.'));
                }
                dbTarget.eaten += await parseInt(amount);
                dbTarget.sucrons = await utils.sucronsCalc(target);
                dbUser.eaten -= await parseInt(amount);
                dbUser.sucrons = await utils.sucronsCalc(message.author);
                await dbTarget.save();
                await dbUser.save();
                return message.reply(embed
                    .setColor(member.displayHexColor)
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription(`I shared ${target} **${amount}** of your Sucrons.\nThey now have **${dbTarget.sucrons}** Sucrosium, while you remain with **${dbUser.sucrons}** Sucrosium.`));
            default:
                return message.reply(embed
                    .setColor(utils.randomHexColor('warning'))
                    .setDescription('I found no matches, to what you are trying to give.\n**REMAINDER:** You can give `cubes`, `sucrosium` or `sucrons`.'));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};