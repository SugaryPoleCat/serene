const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const package = require('../../package.json');

const help = {
    name: 'serene',
    description: 'This will display current information about Serene.',
    aliases: ['bot'],
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let currentServer;
        if(message.channel.type == 'dm') currentServer = 'DMs';
        else if(message.channel.type != 'dm') currentServer = message.guild.name;
        const serverAmount = Array.from(client.guilds.values());
        const channelAmount = Array.from(client.channels.values());
        const userAmount = Array.from(client.users.values());
        const readyAt = new Date().toUTCString(client.readyTimestamp);

        const uptime = Math.floor(client.uptime / 1000);
        const uptimeD = Math.floor(uptime / 86400);
        const uptimeH = Math.floor(uptime % 86400 / 3600);
        const uptimeM = Math.floor(uptime % 86400 % 3600 / 60);
        const uptimeS = Math.floor(uptime % 86400 % 3600 % 60);

        const dDisplay = uptimeD > 0 ? uptimeD + (uptimeD == 1 ? ' day, ' : ' days, ') : '';
        const hDisplay = uptimeH > 0 ? uptimeH + (uptimeH == 1 ? ' hour, ' : ' hours, ') : '';
        const mDisplay = uptimeM > 0 ? uptimeM + (uptimeM == 1 ? ' minute, ' : ' minutes, ') : '';
        const sDisplay = uptimeS > 0 ? uptimeS + (uptimeS == 1 ? ' second' : ' seconds') : '';

        let status;
        if(client.status == 0) status = '**READY**';
        // TODO: create an amount of dependencies based on dependencies, devdependenceis based on devdependencies and total amount of depenedencies.
        // The first problem can be done with .forEach() map thingy. The second is just adding those 2 consts up.

        return message.reply(new Discord.RichEmbed()
            .setThumbnail(client.user.displayAvatarURL)
            .setColor(utils.memberColor(message, client.user))
            .setTitle('SERENE')
            .setDescription(`**INFORMATION ABOUT ME**\n
            Current version: **${package.version}**.
            Current server: **${currentServer}**.
            In: **${serverAmount.length}** servers, handling **${channelAmount.length}** channels and **${userAmount.length}** users.\n\n
            I have been activated on: **${readyAt}** and I have been active for: **${dDisplay}${hDisplay}${mDisplay}${sDisplay}**.\n\n
            Connection to Discord: **${status}**.
            My current ping to the websocket: **${Math.floor(client.ping)}**ms.`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};