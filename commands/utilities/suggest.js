const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const settings = require('../../config/settings.json');

const help = {
    name: 'suggest',
    description: 'A public suggestion! The same as Request, but this is made public instead!',
    usage: '<my cool awesome suggestion text>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('SUGGESTION')
            .setTimestamp()
            .setColor(message.member.displayHexColor);
        const cutlength = 9;
        const request = message.content.slice(cutlength);
        if(request == null || request == undefined || !request){
            return message.reply(embed
                .setColor(utils.randomHexColor('error'))
                .setDescription('Please type out your suggestion. Do I really have to tell you this...'));
        }
        const logChannel = message.guild.channels.find(ch => ch.name == settings.channels.logGuild);
        await logChannel.send(embed
            .setAuthor(message.author.tag)
            .setThumbnail(message.author.displayAvatarURL)
            .setTitle('NEW SUGGESTION\n' + new Date().toUTCString())
            .setDescription(`${message.author} has suggested following:\n\`\`\`${request}\`\`\``));
        embed
            .setTitle('SUGGESTION')
            .setDescription(`${message.author} has suggested following:\n\`\`\`${request}\`\`\``);
        const sentMessage = await message.reply(embed);
        await sentMessage.react('👍');
        await sentMessage.react('👎');
        return sentMessage.react('🤷');
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};