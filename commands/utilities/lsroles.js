const Discord = require('discord.js');
const utils = require('../../utils');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');

const help = {
    name: 'lsroles',
    description: 'List how many roles are in each notaion. Like: how many are in **=== ACCESS ROLES ===**.',
};
const config = {
    cooldown: 7,
    guildOnly: true,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const totalRoles = Array.from(message.guild.roles.values());

        // comparing these to, staff to bot, yeilds negative. Do it the otehr way around to get list of roles.
        const ownerRoles = message.guild.roles.find(r => r.name == '=== OWNER ROLES ===');
        const botRoles = message.guild.roles.find(r => r.name == '=== BOT ROLES ===');
        const staffRoles = message.guild.roles.find(r => r.name == '=== STAFF ROLES ===');
        const specialRoles = message.guild.roles.find(r => r.name == '=== SPECIAL COLORS ===');
        const speciesRoles = message.guild.roles.find(r => r.name == '=== SPECIES ===');
        const jobRoles = message.guild.roles.find(r => r.name == '=== PROFESSIONS ===');
        const genderRoles = message.guild.roles.find(r => r.name == '=== GENDERS ===');
        const normalRoles = message.guild.roles.find(r => r.name == '=== NORMAL ROLES ===');
        const accessRoles = message.guild.roles.find(r => r.name == '=== ACCESS ROLES ===');

        const countOwner = totalRoles.length - ownerRoles.position - 1;
        const countBot = ownerRoles.position - botRoles.position - 1;
        const countStaff = botRoles.position - staffRoles.position - 1;
        const countSpecial = staffRoles.position - specialRoles.position - 1;
        const countSpecies = specialRoles.position - speciesRoles.position - 1;
        const countJobs = speciesRoles.position - jobRoles.position - 1;
        const countGenders = jobRoles.position - genderRoles.position - 1;
        const countNormal = genderRoles.position - normalRoles.position - 1;
        const countAccess = normalRoles.position - accessRoles.position - 1;

        return message.reply(new Discord.RichEmbed()
            .setTitle(message.member.displayName)
            .setColor(message.member.displayHexColor)
            .setThumbnail(message.author.displayAvatarURL)
            .setTimestamp()
            .setDescription(`**Total roles = **${totalRoles.length}\n**Owner roles = **${countOwner}\n**Bot roles = **${countBot}\n**Staff roles = **${countStaff}\n\n**Species = **${countSpecies}\n**Jobs = **${countJobs}\n**Genders = **${countGenders}\n**Normal roles = **${countNormal}\n**Access roles = **${countAccess}`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};