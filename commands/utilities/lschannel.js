const Discord = require('discord.js');
const utils = require('../../utils');

const help = {
    name: 'lschannels',
    description: 'For curiosity\'s sake! List how many channels are in each category and how many users can see.',
    aliases: ['lsch'],
};
const config = {
    cooldown: 7,
    guildOnly: true,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const Guild = message.guild;
        utils.log(Guild, 'Guild found');

        const totalChannels = Array.from(Guild.channels.values());
        // for(let x = 0; x < categoryIDs.length; x++){
        // }
        const importantInformation = Guild.channels.find(ch => ch.name == 'IMPORTANT INFORMATION');
        const ooc = Guild.channels.find(ch => ch.name == 'OOC');
        const artisansLounge = Guild.channels.find(ch => ch.name == 'ARTISANS LOUNGE');
        const roleplaying = Guild.channels.find(ch => ch.name == 'ROLEPLAYING');
        const sucrosia = Guild.channels.find(ch => ch.name == 'SUCROSIA');
        const nsfw = Guild.channels.find(ch => ch.name == 'NSFW');

        // Voice is giving trouble, because it has voice channels in it.
        const voiceChats = Guild.channels.find(ch => ch.name == 'VOICE CHATS');
        const countImportant = Array.from(importantInformation.children.values());
        const countOOC = Array.from(ooc.children.values());
        const countArtisans = Array.from(artisansLounge.children.values());
        const countRoleplaying = Array.from(roleplaying.children.values());
        const countSucrosia = Array.from(sucrosia.children.values());
        const countNSFW = Array.from(nsfw.children.values());
        const countVoice = Array.from(voiceChats.children.values());

        const channelsUsersCanSee = countImportant.length + countOOC.length + countArtisans.length + countRoleplaying.length + countSucrosia.length + countNSFW.length + countVoice.length;
        const hiddenChannels = totalChannels.length - channelsUsersCanSee;

        // SO without using .length, it just throws out the channel list. USEFUL.
        const embed = new Discord.RichEmbed()
            .setColor(message.member.displayHexColor)
            .setTimestamp()
            .setThumbnail(message.author.displayAvatarURL)
            .setTitle(message.member.displayName)
            .setDescription(`**Total channels = **${totalChannels.length}\n**Channels users can see = **${channelsUsersCanSee}\n**Private channels = **${hiddenChannels}\n\n**Important information = **${countImportant.length}\n**OOC = **${countOOC.length}\n**Artisans lounge = **${countArtisans.length}\n**Roleplayinng = **${countRoleplaying.length}\n**Sucrosia = **${countSucrosia.length}\n**NSFW = **${countNSFW.length}\n**Voice channels = **${countVoice.length}`);
        return message.channel.send(embed);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};