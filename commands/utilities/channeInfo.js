const Discord = require('discord.js');
const utils = require('../../utils');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');

const help = {
    name: 'channelinfo',
    description: 'Get some fancy info about a mentioned channel!',
    aliases: ['channeli', 'chinfo'],
    usage: '<#channel>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const Guild = client.guilds.find(g => g.id === settings.guild);
        utils.info(Guild, 'Guild found');
        const Guild2 = message.guild;
        utils.log(Guild2, 'Guild2 found');

        const target = message.mentions.channels.first();

        const channel = Guild.channels.find(ch => ch.id === target.id);
        const channelItems = Array.from(channel.parent.children.values());
        utils.log(`Calculated position = ${channel.calculatedPosition}\nCreated at = ${channel.createdAt}\nParent = ${channel.parent.name}\nParent has this many children = ${channelItems.length}`, 'channelInfo');
        const embed = new Discord.RichEmbed()
            .setTitle('Channel list thing')
            .setAuthor(message.member.displayName)
            .setTimestamp()
            .setThumbnail(message.author.displayAvatarURL)
            .setColor(message.member.displayHexColor)
            .setDescription(`Calculated position = ${channel.calculatedPosition}\nCreated at = ${channel.createdAt}\nParent = ${channel.parent.name}\nParent has this many children = ${channelItems.length}`);
        return message.channel.send(embed);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};