const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');

const help = {
    name: 'ping',
    description: 'Test your ping!',
};
const config = {
    cooldown: 5,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('PING')
            .setColor(utils.memberColor(message, message.author));
        const msg = await message.channel.send(embed
            .setDescription('Pong!'));
        return msg.edit(embed
            .setTitle('PONG!')
            .setDescription(`Your latency is: **${msg.createdTimestamp - message.createdTimestamp}**ms.\n
            My latency to the websocket is: **${client.ping}**ms.`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};