const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const settings = require('../../config/settings.json');

const help = {
    name: 'request',
    description: 'Stealthly request something that should happen in the server or with the bot. You can request for example for a new role, change in rules, new bot, new feature for the bot ETC.',
    usage: '<my cool request text>',
};
const config = {
    cooldown: 10,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('REQUEST')
            .setTimestamp()
            .setColor(message.member.displayHexColor);
        const cutlength = 9;
        const request = message.content.slice(cutlength);
        if(request == null || request == undefined || !request){
            return message.reply(embed
                .setColor(utils.randomHexColor('error'))
                .setDescription('Please type out your request. I mean honestly...'));
        }
        const logChannel = message.guild.channels.find(ch => ch.name == settings.channels.logGuild);
        await message.reply(embed
            .setThumbnail(message.author.displayAvatarURL)
            .setDescription('Thank you for your Request!\n\nIt has been sent to the log channel and is being processed by the staff!'));
        return logChannel.send(embed
            .setAuthor(message.author.tag)
            .setTitle('NEW REQUEST\n' + new Date().toUTCString())
            .setDescription(request));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};