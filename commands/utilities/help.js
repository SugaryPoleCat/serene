const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const getCommands = require('../../plugins/handlerCommands.js').getLoadedCommands;

const help = {
    name: 'help',
    description: 'This will help you on how to use the commands included in this bot!',
    usage: '[Commandname]',
};
const config = {
    cooldown: 5,
    guildOnly: false,
    nsfw: false,
    args: false,
    delete: false,
};
// MYWAY
// const categories = ['fun', 'owner', 'admin', 'utilities', 'empire', 'admin', 'converters', 'nsfw'];
// const commandlist = [];
const commands = getCommands();
let data = [];
async function fox(message, args, client){
    try {
        const categories = {};
        const embed = new Discord.RichEmbed()
            .setTitle('HELP');
        // console.log('this is help');
        // utils.log('This is help');

        if(!args.length){
            // message.author.send('Here\'s a list of all my commands: ');
            // My way
            // for(let x = 0; x < categories.length; x++){
            //     commands.forEach(function(command){
            //         if(command.category == categories[x]){
            //             commandlist.push(command);
            //         }
            //     });
            //     utils.info(JSON.stringify(commandlist));
            //     data.push(`**Category: ${categories[x]}**\n\`${commandlist.map(command2 => command2.help.name).join('`, `')}\`\n`);
            //     commandlist.length = 0;
            // }

            // SORU WAY
            commands.forEach((command) => {
                // We assume apparently form the beginning that categories is an array, that will hold the names of the commands.
                if (!categories[command.category]) {
                    categories[command.category] = [];
                }
                // Then we push those names into the array, into the object. We pass only the name as not the whole command is necessary and its just egh.
                categories[command.category].push(command.help.name);
            });

            // THEN WE CHECK, for each category, in the category list we made before, IF categories, has a property -- Category.
            for (const category in categories) {
                if (categories.hasOwnProperty(category)) {
                    // Then we make a command list based on that and poof.
                    const commandlist = categories[category];
                    // console.log(`${category}:`, commandlist);
                    data.push(`**${category.toUpperCase()}**\n\`${commandlist.join('`, `')}\`\n`);
                }
            }
            await message.author.send(embed
                .setTimestamp()
                .setThumbnail(client.user.displayAvatarURL)
                .setColor(utils.memberColor(message, client.user))
                .setDescription(`**HOW DO I USE THE COMMANDS**\n
                It is really simple. Look at the \`Usage\` in the command. This will tell you **HOW** to use something.\n
                If something says \`<normal | fancy>\`, it means you **HAVE** to provide, **EITHER** \`normal\`, or \`fancy\`, right after typing \`!COMMANDNAME\`, for example: \`!say fancy\`.\n
                If for example, a command says \`[@mention]\`, it means the \`@mention\` is optional and the mention can by anyone.\n
                If it says \`@someone\`, it has to be directed at a member or user.\n
                IN SHORT: \`< >\` = REQUIRED, \`[ ]\` = OPTIONAL, \`things seperated by |\` = Means either option will work, \`( thing between these )\` = are explanations or descriptions on the particular option.\n
                \`@mention\` = mention something, \`@someone\` = mention a user, \`@role\` = mention a role, \`#channel\` = mention a channel.`));
            await message.author.send(embed
                .setColor(utils.memberColor(message, client.user))
                .setTimestamp()
                .setThumbnail(client.user.displayAvatarURL)
                .setDescription(data));
            // utils.info(data);
            if(message.channel.type != 'dm') await message.reply(embed
                .setColor(utils.memberColor(message, client.user))
                .setThumbnail(client.user.displayAvatarURL)
                .setTimestamp()
                .setDescription('I\'ve sent you a list of my commands to your DMs!'));
            data = [];
            return message.author.send(embed
                .setColor(utils.memberColor(message, client.user))
                .setThumbnail(client.user.displayAvatarURL)
                .setTimestamp()
                .setDescription('If you wish to know more, please use `!help COMMANDNAME` to display information about each command!'));
        }
        const name = args[0];
        const command = commands.get(name.toLowerCase()) || commands.find(c => c.aliases && c.aliases.includes(name.toLowerCase()));

        if (!command){
            return message.reply(embed
                .setColor(utils.randomHexColor('error'))
                .setThumbnail(client.user.displayAvatarURL)
                .setTimestamp()
                .setDescription('This command does not exist!'));
        }

        // THE COMMAND HELP DETAILS
        // This data is when a user specifies a command they need help for.
        data.push(`**__Name__:** ${command.help.name}`);
        data.push(`**__Category__: ** ${command.category}`);
        if (command.help.aliases) data.push(`**__Aliases__:** ${command.help.aliases.join(', ')}\n`);
        data.push(`**__Server only__:** ${command.config.guildOnly}`);
        if (command.help.description) data.push(`**__Description__:** ${command.help.description}\n`);
        if (command.help.usage) data.push(`**__Usage__:** \`${botconfig.prefix}${command.help.name} ${command.help.usage}\``);
        if (command.config.delete) data.push(`**__Deletes after use__:** ${command.config.delete}`);
        if (command.config.admin) data.push(`**__Admin only__:** ${command.config.admin}`);
        if (command.config.owner) data.push(`**__Owner only__: **${command.config.owner}`);
        if (command.config.botowner) data.push(`**__BOT Owner only__: **${command.config.botowner}`);
        data.push(`**__Cooldown__:** ${command.config.cooldown || 3} second(s)`);

        await message.channel.send(embed
            .setColor(utils.memberColor(message, client.user))
            .setThumbnail(client.user.displayAvatarURL)
            .setTimestamp()
            // .addField('Name', command.help.name)
            .setDescription(data));
        return data = [];
    }
    catch (err) {
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};