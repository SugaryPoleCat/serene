const Discord = require('discord.js');
const utils = require('../../utils');
const { ServerChannels, ServerSettings, ServerModules } = require('../../database/dbObjects');

const help = {
    name: 'refresh',
    description: 'Destroy the connection.',
};
const config = {
    cooldown: 1,
    guildOnly: false,
    args: false,
    admin: false,
    botowner: true,
    delete: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        client.guilds.forEach(async (g) => {
            const dbChannels = await ServerChannels.findOne({
                where: { serverID: g.id.toString() },
            });
            if(dbChannels){
                console.log(`Found server in dbChannels with ID: ${g.id}`);
                await ServerChannels.destroy({
                    where: { serverID: g.id.toString() },
                });
                await ServerChannels.findOrCreate({
                    where: { serverID: g.id.toString() },
                    default: { serverID: g.id.toString() },
                });
                console.log(`Recreated dbChannels with ID: ${g.id}`);
            }
            else{
                console.log(`Did not find server in dbChannels with ID: ${g.id}`);
                await ServerChannels.findOrCreate({
                    where: { serverID: g.id.toString() },
                    default: { serverID: g.id.toString() },
                });
                console.log(`Created dbChannels with ID: ${g.id}`);
            }

            const dbModules = await ServerModules.findOne({
                where: { serverID: g.id.toString() },
            });
            if(dbModules){
                console.log(`Found server in dbModules with ID: ${g.id}`);
                await ServerModules.destroy({
                    where: { serverID: g.id.toString() },
                });
                await ServerModules.findOrCreate({
                    where: { serverID: g.id.toString() },
                    default: { serverID: g.id.toString() },
                });
                console.log(`Recreated dbModules with ID: ${g.id}`);
            }
            else{
                console.log(`Did not find server in dbModules with ID: ${g.id}`);
                await ServerModules.findOrCreate({
                    where: { serverID: g.id.toString() },
                    default: { serverID: g.id.toString() },
                });
                console.log(`Created dbModules with ID: ${g.id}`);
            }

            const dbSettings = await ServerSettings.findOne({
                where: { serverID: g.id.toString() },
            });
            if(dbSettings){
                console.log(`Found server in dbSettings with ID: ${g.id}`);
                await ServerSettings.destroy({
                    where: { serverID: g.id.toString() },
                });
                await ServerSettings.findOrCreate({
                    where: { serverID: g.id.toString() },
                    default: { serverID: g.id.toString() },
                });
                console.log(`Recreated dbSettings with ID: ${g.id}`);
            }
            else{
                console.log(`Did not find server in dbSettings with ID: ${g.id}`);
                await ServerSettings.findOrCreate({
                    where: { serverID: g.id.toString() },
                    default: { serverID: g.id.toString() },
                });
                console.log(`Created dbSettings with ID: ${g.id}`);
            }
            const ownerDM = client.users.find(u => u.id == g.ownerID);
            await ownerDM.send(new Discord.RichEmbed()
                .setTitle('DATABASE CHANGED')
                .setColor(utils.randomHexColor())
                .setThumbnail(client.user.displayAvatarURL)
                .setTimestamp()
                .setDescription('The databse for servers has changed, thus every server underwent necessary changes. This results in a reset of your settings. Please set them again.\n\nIf you recieved this message more than once, please ignore. This is sent only to the **OWNERS** of **EACH** server I am in. This means you own more than one server.'));
        });
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owener destroy.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};