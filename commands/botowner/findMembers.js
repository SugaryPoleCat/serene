const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'tmembers',
    description: 'A test command apparently.',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: false,
    admin: false,
    botowner: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let arr_members = [];
        const Guild = message.guild();
        const Members = Guild.members();
        for(let x = 0; x < Members.length; x++){
            const memberino = Members[x];
            if(memberino.hasRole('STAFF')){
                console.log(`Member logged: ${memberino.username}`);
                arr_members.push(memberino[x]);
            }
        }
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owner find members.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};