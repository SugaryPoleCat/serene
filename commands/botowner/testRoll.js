const Discord = require('discord.js');
const utils = require('../../utils');

const help = {
    name: 'troll',
    description: '',
    aliases: [''],
    usage: 'd20 or 2d20',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: true,
    admin: false,
    delete: true,
    nsfw: false,
    botowner: true,
};

let total = 0;
let arr_totals = [];

function getTotal(times, rolling){
    if(!times){
        times = 1;
    }
    for(let i = 0; i < times; i++){
        const rolled = getRoll(rolling);
        arr_totals.push(rolled);
        total += rolled;
    }
}

function getRoll(rolling){
    const ayyroll = Math.floor((Math.random() * rolling) + 1);
    return ayyroll;
}

async function fox(message, args, client){
    try{
        // WHAT DO I WANT
        /*
        1. User types in d100
        2. It prints out a random number as if user actually threw a d100 dice.
        3. if user types in 2d100, it will throw the dice 2 times, each time, adding the randomised number to an array and displaying the total
        4. Maximum loops is 9
        */
        let color = message.member.displayHexColor;
        if(color == '#000000') color = utils.randomHexColor();
        else color = message.member.displayHexColor;
        const curated = message.content.substring(message.content.indexOf(' ') + 1);

        utils.info(curated, 'Curated');
        const matches = curated.match(/^(\d*)d(\d+)$/);
        utils.info(matches, 'matches');
        const amount = matches[2];
        const multiplier = matches[1];
        getTotal(multiplier, amount);
        const embed = new Discord.RichEmbed()
            .setTitle(message.member.displayName)
            .setColor(color)
            .setDescription(`Total: ${total}, rolls: ${arr_totals}`);
        return message.channel.send(embed);
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owner testroll.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};