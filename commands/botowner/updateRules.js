const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const rulesJSON = require('../../config/rules.json');
const Canvas = require('canvas');

const help = {
    name: 'rules',
    description: 'Delete old rules and send new ones.',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
    botowner: true,
};

function displayRule(array, counter){
    let reply = '';
    for(let x = 0; x < array.length; x++){
        reply += `**${x + counter}:** ${array[x]}\n\n`;
    }
    return reply;
}

async function postGraphic(channel, title, color){
    const canvas = Canvas.createCanvas(600, 150);
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
    ctx.lineTo(40, 100);
    ctx.lineTo(5, 70);
    ctx.lineTo(20, 50);
    ctx.lineTo(40, 60);
    ctx.lineTo(60, 50);
    ctx.lineTo(75, 70);
    ctx.closePath();
    ctx.strokeStyle = '#fff';
    ctx.lineWidth = 3;
    ctx.stroke();

    ctx.moveTo(40, 100);
    ctx.lineTo(40, 60);
    ctx.lineTo(75, 70);
    // ctx.lineWidth = 2;
    ctx.stroke();

    ctx.lineTo(40, 60);
    ctx.lineTo(5, 70);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(560, 100);
    ctx.lineTo(595, 70);
    ctx.lineTo(580, 50);
    ctx.lineTo(560, 60);
    ctx.lineTo(540, 50);
    ctx.lineTo(525, 70);
    ctx.closePath();
    ctx.stroke();

    ctx.moveTo(560, 100);
    ctx.lineTo(560, 60);
    ctx.lineTo(525, 70);
    // ctx.lineWidth = 2;
    ctx.stroke();

    ctx.lineTo(560, 60);
    ctx.lineTo(595, 70);
    ctx.stroke();

    ctx.font = '42px sans-serif';
    ctx.fillStyle = '#fff';
    ctx.textAlign = 'center';
    ctx.fillText(title, 300, 85);
    const attachmenet = await new Discord.Attachment(canvas.toBuffer(), `${title}.png`);
    return channel.send(attachmenet);
}

async function fox(message, args, client){
    try{
        const colors = ['#ff2d62', '#ff3a6c', '#ff4473', '#ff5681', '#ff6088', '#ff7094', '#ff7a9b', '#ff84a3', '#ff8eab', '#ff99b2', '#ffa3ba'];
        const embed = new Discord.RichEmbed();
        /*
        Should get rules from an external thing, like a JSON file or something similar.

        1. GETS THE RULES CHANNEL.
        2. MAKES CHANNEL INVISBLE FOR PEOPLE.
        3. DELETES THE OLD RULES.
        4. RESENDS THE NEW RULES.
        5. MAKES CHANNEL VISIBLE AGAIN.
        */
        const channel = message.mentions.channels.first();
        const generalarray = Array.from(rulesJSON.general);
        const general2array = Array.from(rulesJSON.general2);
        const nsfwarray = Array.from(rulesJSON.nsfw);
        const rparray = Array.from(rulesJSON.rp);
        const rp2array = Array.from(rulesJSON.rp2);
        let totalrules;
        let counter = 1;
        let reply;
        await channel.overwritePermissions(channel.guild.defaultRole, { VIEW_CHANNELL: false });
        await channel.bulkDelete(14);
        await postGraphic(channel, 'GENERAL RULES', colors[0]);
        await channel.send(embed
            .setColor(colors[0])
            .setDescription(displayRule(generalarray, counter)));
        counter += generalarray.length;
        await postGraphic(channel, 'GENERAL RULES', colors[1]);
        await channel.send(embed
            .setColor(colors[1])
            .setDescription(displayRule(general2array, counter)));
        counter = 1;
        await postGraphic(channel, 'NSFW RULES', colors[2]);
        await channel.send(embed
            .setColor(colors[2])
            .setDescription(displayRule(nsfwarray, counter)));
        await postGraphic(channel, 'RP RULES', colors[3]);
        await channel.send(embed
            .setColor(colors[3])
            .setDescription(displayRule(rparray, counter)));
        counter += rparray.length;
        await postGraphic(channel, 'RP RULES', colors[4]);
        await channel.send(embed
            .setColor(colors[4])
            .setDescription(displayRule(rp2array, counter)));
        await postGraphic(channel, 'RULES STATS', colors[5]);
        totalrules = await generalarray.length + general2array.length;
        reply = `Total general rules: **${totalrules}**`;
        totalrules = await nsfwarray.length;
        reply += `\nTotal NSFW rules: **${totalrules}**`;
        totalrules = await rparray.length + rp2array.length;
        reply += `\nTotal RP rules: **${totalrules}**`;
        totalrules = await generalarray.length + general2array.length + nsfwarray.length + rparray.length + rp2array.length;
        reply += `\nTotal amount of rules: **${totalrules}**`;
        await channel.send(embed
            .setColor(colors[5])
            .setDescription(reply));
        await postGraphic(channel, 'INVITE LINK', colors[6]);
        await channel.send(embed
            .setColor(colors[6])
            .setThumbnail(message.guild.iconURL)
            .setDescription('https://discord.gg/TRcdUD5'));
        await channel.overwritePermissions(channel.guild.defaultRole, { VIEW_CHANNEL: true });
        return message.reply(':)');
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owner update rules.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};