const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'resetnames',
    description: '',
    aliases: [''],
    usage: '',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: false,
    admin: false,
    botowner: true,
    delete: true,
    nsfw: false,
};
const typeMatch = ['yes', 'no'];
const type = response => {
    return typeMatch.some(answer => answer.toLowerCase() === response.content.toLowerCase());
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setColor(message.member.displayHexColor)
            .setThumbnail(message.author.displayAvatarURL)
            .setTitle('RESET NAMES');
        await message.reply(embed
            .setDescription(`Are you sure?\nPlease answer with \`${typeMatch.join('`, `')}\`.\nI'm waiting **10** seconds.`));
        const collected = await message.channel.awaitMessages(type, { maxMatches: 1, time: 10000, errors: ['time'] });
        const rType = collected.first().content.toLowerCase();
        if(rType == 'yes'){
            await message.guild.members.forEach(b => {
                utils.log(`MEMBER: ${b.displayNickname} nickname changed to: ${b.user.username}`);
                b.setNickname(b.user.username);
            });
            // Guild.members.tap(member => utils.log(`Member`));
            return message.reply(embed
                .setTimestamp()
                .setDescription('All nicknames have been reset.'));
            // Guild.members.tap(member => member.setNickname(member.user.username));
            // console.log(JSON.stringify(members));
        }
        else return message.reply(embed
            .setTimestamp()
            .setThumbnail(client.user.displayAvatarURL)
            .setDescription('THEN WHY ARE YOU WAISTING MY TIME'));
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to admin reset names.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};