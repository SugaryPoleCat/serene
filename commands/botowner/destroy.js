const Discord = require('discord.js');
const utils = require('../../utils');

const help = {
    name: 'destroy',
    description: 'Destroy the connection.',
};
const config = {
    cooldown: 1,
    guildOnly: false,
    args: false,
    admin: false,
    botowner: true,
    delete: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let color = message.member.displayHexColor;
        if(color == '#000000') color = utils.randomHexColor('random');
        const embed = new Discord.RichEmbed()
            .setAuthor(message.author.tag)
            .setThumbnail(message.author.displayAvatarURL)
            .setTitle('Client quitting')
            .setColor(color)
            .setDescription('I am being disconnected and shutdown.\nHave a nice day and see you later!');
        await message.channel.send(embed);
        return client.destroy().then(utils.info('Client destroyed, please restart.'));
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owener destroy.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};