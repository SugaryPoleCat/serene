const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { UserInventory } = require('../../database/dbObjects');

const help = {
    name: 'inv',
    description: '',
    aliases: [''],
    usage: '',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const [dbStone] = await UserInventory.findOrCreate({
            where: { userID: message.author.id.toString(), thing: 'Stone' },
            defaults: { userID: message.author.id.toString(), thing: 'Stone' },
        });
        const [dbGoldBar] = await UserInventory.findOrCreate({
            where: { userID: message.author.id.toString(), thing: 'Gold Bar' },
            defaults: { userID: message.author.id.toString(), thing: 'Gold Bar' },
        });
        dbStone.amount++;
        dbGoldBar.amount++;
        await dbStone.save();
        await dbGoldBar.save();
        return message.reply(`You just got: **${dbStone.amount}** __${dbStone.thing}__ and **${dbGoldBar.amount}** __${dbGoldBar.thing}__.`);
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owner inventory.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};