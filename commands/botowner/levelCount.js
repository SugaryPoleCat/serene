const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Levels } = require('../../database/dbObjects');

const help = {
    name: 'countlevels',
    description: '',
    aliases: [''],
    usage: '',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    botowner: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        client.users.forEach(async function(user){
            const dbLevel = await Levels.findOne({
                where: { userID: user.id },
            });
            if(!dbLevel) return console.log('nothing ofound');
            const currentXP = dbLevel.exp;
            const expIncrease = 1.25;
            let maxEXP = 100;
            let level = 1;
            while(maxEXP < currentXP){
                maxEXP *= expIncrease;
                level += 1;
            }
            dbLevel.level = parseInt(level);
            dbLevel.maxExp = parseInt(maxEXP *= expIncrease);
            await dbLevel.save();
            return console.log(user.tag + 'Done');
        });
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owner recount levels.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};