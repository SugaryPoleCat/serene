const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');

const help = {
    name: 'ospank',
    description: 'Why did I make this command.',
    aliases: [''],
    usage: '',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
};

// why did i make htis command
async function fox(message, args, client){
    try{
        const target = message.mentions.users.first();
        const embed = new Discord.RichEmbed()
            .setTitle('SPANK')
            .setColor(utils.memberColor(message))
            .setDescription(`${message.author} you naughty pon, you have spanked ${target} ~`);
        return message.channel.send(embed);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};