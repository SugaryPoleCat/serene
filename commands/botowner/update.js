const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const json = require('../../package.json');
const utils = require('../../utils');

const help = {
    name: 'update',
    description: 'Pass a fancy update to the guild.',
    usage: '<awesome update text>',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
    botowner: true,
};

async function fox(message, args, client){
    try{
        const Channel = message.guild.channels.find(ch => ch.name == 'serene-updates');
        const date = new Date().toUTCString();
        const curatedContent = message.content.slice(8);
        const embed = new Discord.RichEmbed()
            .setTitle('I got a new update!')
            .setColor(message.member.displayHexColor)
            .setDescription(`**__${date}__**\n**${client.user.username} - ${json.version}**\n\`\`\`${curatedContent}\`\`\``)
            .setTimestamp();
        return Channel.send(embed);
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owner update my channel.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};