const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Currencies, Reputation, Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'ownergive',
    description: 'This handles what to give someone or take.',
    aliases: ['ogive'],
    usage: '<@target> <cubes | sucrosium | rep | sucrons> <amount>',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: true,
    admin: true,
    botowner: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    const embed = new Discord.RichEmbed()
        .setTimestamp()
        .setTitle('GIVE');
    try{
        const target = message.mentions.users.first();
        const member = message.guild.members.find(u => u.id == target.id);
        let what = args[1];
        const amount = parseInt(args[2]);
        let newAmount;
        let tookOrGave;
        if(!target || target == undefined){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription(`${message.author}, I need a target.`));
        }
        if(!what || what == undefined){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription(`${message.author}, I need **what** argument, as in __WHAT__ do you want me to give.`));
        }
        if(!amount || amount == undefined || isNaN(amount)){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription(`${message.author}, I need an amount.`));
        }
        else if(amount > 100000){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription(`${message.author}, I can not give more than **100000** of **${what}**.`));
        }
        else if(amount < -100000){
            return message.reply(embed
                .setColor(utils.randomHexColor('warning'))
                .setDescription(`${message.author}, I can not give less than **-100000** of **${what}**.`));
        }
        else{
            if(amount < 0){
                tookOrGave = 'took';
            }
            else if(amount >= 0){
                tookOrGave = 'gave';
            }
            if(what == 'cubes'){
                const [dbTarget] = await Currencies.findOrCreate({
                    where: { userID: target.id.toString() },
                    defaults: { userID: target.id.toString() },
                });
                dbTarget.strawberrycubes += parseInt(amount);
                if(dbTarget.strawberrycubes < 0){
                    dbTarget.strawberrycubes = parseInt(0);
                }
                await dbTarget.save();
                newAmount = parseInt(dbTarget.strawberrycubes);
                what = 'Strawberrycubes';
            }
            else if(what == 'sucrosium'){
                const [dbTarget] = await Currencies.findOrCreate({
                    where: { userID: target.id.toString() },
                    defaults: { userID: target.id.toString() },
                });
                dbTarget.sucrosium += parseInt(amount);
                if(dbTarget.sucrosium < 0){
                    dbTarget.sucrosium = parseInt(0);
                }
                await dbTarget.save();
                newAmount = parseInt(dbTarget.sucrosium);
                what = 'Sucrosium';
            }
            else if(what == 'rep'){
                const [dbTarget] = await Reputation.findOrCreate({
                    where: { userID: target.id.toString() },
                    defaults: { userID: target.id.toString() },
                });
                dbTarget.amount += parseInt(amount);
                if(dbTarget.amount < 0){
                    dbTarget.amount = parseInt(0);
                }
                await dbTarget.save();
                newAmount = parseInt(dbTarget.amount);
                what = 'Reputation';
            }
            else if(what == 'sucrons'){
                const [dbTarget] = await Sucrons.findOrCreate({
                    where: { userID: target.id.toString() },
                    defaults: { userID: target.id.toString() },
                });
                dbTarget.eaten += parseInt(amount);
                if(dbTarget.eaten < 0){
                    dbTarget.eaten = parseInt(0);
                }
                await dbTarget.save();
                newAmount = parseInt(dbTarget.amount);
                what = 'Sucrons';
            }
            return message.reply(embed
                .setColor(member.displayHexColor)
                .setThumbnail(target.displayAvatarURL)
                .setDescription(`I just ${tookOrGave} ${target}'s ${amount} of ${what}.\nThey now have ${newAmount} of ${what}.`));
        }
    }
    catch(err){
        return utils.errorEmbed(message, 'I am unable to owner give.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};