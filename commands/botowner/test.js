const Discord = require('discord.js');
const utils = require('../../utils');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');

const help = {
    name: 'test',
    description: 'Test!',
    aliases: [''],
    usage: '',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: false,
    admin: false,
    botowner: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        console.log("==========");
        console.log(botconfig.botowners);
        console.log(Object.values(botconfig.botowners));
        if(Object.values(botconfig.botowners).includes(message.author.id)){
            message.channel.send('yay');
        }else{
            message.channel.send('no owner');
        }
    }
    catch(err){
        console.error(err);
        return utils.errorEmbed(message, 'I am unable to owner test.\n' + err, help.name);
    }
}

module.exports = {
    help, config, fox,
};
