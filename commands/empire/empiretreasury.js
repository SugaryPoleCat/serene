const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { EmpireTreasury, EmpireDonors, Currencies, Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'empire',
    description: 'This command will either let you check how much the Empire has, donate or check who donated the most!',
    aliases: ['empres', 'empireres'],
    usage: '<donate | donors | check> <cubes | sucrosium | sucrons>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const things = ['cubes', 'sucrosium', 'sucrons'];
        const whats = ['donors', 'donate', 'check'];
        const member = message.guild.members.find(u => u.id == client.user.id);
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setColor(member.displayHexColor)
            .setThumbnail(message.guild.iconURL)
            .setTitle('EMPIRE RESOURCES');
        const msg = await message.channel.send(embed
            .setColor(utils.randomHexColor())
            .setDescription('Fetching Empire info...'));

        const [dbEmpire] = await EmpireTreasury.findOrCreate({
            where: { empireID: message.guild.id.toString() },
            defaults: { empireID: message.guild.id.toString() },
        });
        let dbCurr;
        let dbSucrons;
        let dbDonors;

        const what = await args[0];
        let whatDonorsDonate;
        let amount;
        let description;
        const leaderboard = await new Discord.Collection();
        let ranking;
        switch(what){
            case 'donate':
                whatDonorsDonate = args[1];
                switch(whatDonorsDonate){
                    case 'cubes':
                        [dbCurr] = await Currencies.findOrCreate({
                            where: { userID: message.author.id.toString() },
                            defaults: { userID: message.author.id.toString() },
                        });
                        amount = parseInt(args[2]);
                        // await amountCheck(msg, message, embed, amount, dbCurr.strawberrycubes);
                        if(amount > dbCurr.strawberrycubes){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription(`You can not donate more than what you already have.\nYou have: **${dbCurr.strawberrycubes}**.`));
                        }
                        else if(amount > 100000){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('You can not donate more than **100000** at a time.'));
                        }
                        else if(amount < 1){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('You can not donate less than **1**.'));
                        }
                        dbCurr.strawberrycubes -= parseInt(amount);
                        dbEmpire.strawberrycubes += parseInt(amount);
                        await dbCurr.save();
                        await dbEmpire.save();
                        [dbDonors] = await EmpireDonors.findOrCreate({
                            where: { userID: message.author.id.toString(), empireID: message.guild.id.toString() },
                            defaults: { userID: message.author.id.toString(), empireID: message.guild.id.toString() },
                        });
                        dbDonors.strawberrycubes += parseInt(amount);
                        await dbDonors.save();
                        await msg.delete();
                        return message.reply(embed
                            .setThumbnail(message.author.displayAvatarURL)
                            .setColor(message.member.displayHexColor)
                            .setDescription(`You have successfully donated to the Empire **${amount}** Strawberrycubes!\n\nCongratulations!\nYou are poorer, but our Empire gets richer!\nYou are left with: **${dbCurr.strawberrycubes}** Strawberrycubes. Our Empire now has: **${dbEmpire.strawberrycubes}** Strawberrycubes.\n\nIn total you have donated: **${dbDonors.strawberrycubes}** Strawberrycubes.`));
                    case 'sucrosium':
                        [dbCurr] = await Currencies.findOrCreate({
                            where: { userID: message.author.id.toString() },
                            defaults: { userID: message.author.id.toString() },
                        });
                        amount = parseInt(args[2]);
                        if(amount > dbCurr.sucrosium){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription(`You can not donate more than what you already have.\nYou have: **${dbCurr.sucrosium}**.`));
                        }
                        else if(amount > 100000){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('You can not donate more than **100000** at a time.'));
                        }
                        else if(amount < 1){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('You can not donate less than **1**.'));
                        }
                        dbCurr.sucrosium -= parseInt(amount);
                        dbEmpire.sucrosium += parseInt(amount);
                        await dbCurr.save();
                        await dbEmpire.save();
                        [dbDonors] = await EmpireDonors.findOrCreate({
                            where: { userID: message.author.id.toString(), empireID: message.guild.id.toString() },
                            defaults: { userID: message.author.id.toString(), empireID: message.guild.id.toString() },
                        });
                        dbDonors.sucrosium += parseInt(amount);
                        await dbDonors.save();
                        await msg.delete();
                        return message.reply(embed
                            .setThumbnail(message.author.displayAvatarURL)
                            .setColor(message.member.displayHexColor)
                            .setDescription(`You have successfully donated to the Empire **${amount}** Sucrosium!\n\nCongratulations!\nYou are poorer, but our Empire gets richer!\nYou are left with: **${dbCurr.sucrosium}** Sucrosium. Our Empire now has: **${dbEmpire.sucrosium}** Sucrosium.\n\nIn total you have donated: **${dbDonors.sucrosium}** Sucrosium.`));
                    case 'sucrons':
                        [dbSucrons] = await Sucrons.findOrCreate({
                            where: { userID: message.author.id.toString() },
                            defaults: { userID: message.author.id.toString() },
                        });
                        amount = parseInt(args[2]);
                        if(amount > dbSucrons.eaten){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription(`You can not donate more than what power you can share!\nYou can share: **${dbCurr.eaten}** Sucrons.`));
                        }
                        else if(amount > 100000){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('You can not share more than **100000** at a time.'));
                        }
                        else if(amount < 1){
                            await msg.delete();
                            return message.reply(embed
                                .setThumbnail(message.author.displayAvatarURL)
                                .setColor(utils.randomHexColor('error'))
                                .setDescription('You can not share less than **1**.'));
                        }
                        dbEmpire.sucrons += await parseInt(amount);
                        dbSucrons.eaten -= await parseInt(amount);
                        dbSucrons.sucrons = await utils.sucronsCalc(message.author);
                        await dbSucrons.save();
                        await dbEmpire.save();
                        [dbDonors] = await EmpireDonors.findOrCreate({
                            where: { userID: message.author.id.toString(), empireID: message.guild.id.toString() },
                            defaults: { userID: message.author.id.toString(), empireID: message.guild.id.toString() },
                        });
                        dbDonors.sucrons += await parseInt(amount);
                        await dbDonors.save();
                        await msg.delete();
                        return message.reply(embed
                            .setThumbnail(message.author.displayAvatarURL)
                            .setColor(message.member.displayHexColor)
                            .setDescription(`You have successfully shared with the Empire **${amount}** Sucrons!\n\nCongratulations!\nYou feel weaker, but your name shall be remembered to the end of time!\nYou are left with: **${dbSucrons.eaten}** Sucrons you can share and **${dbSucrons.sucrons}** Sucrons in total. Our Empire now has: **${dbEmpire.sucrons}** Sucrons.\n\nIn total you have shared: **${dbDonors.sucrons}** Sucrons.`));
                    default:
                        await msg.delete();
                        return message.reply(embed
                            .setThumbnail(message.author.displayAvatarURL)
                            .setColor(message.member.displayHexColor)
                            .setDescription(`Sorry, but you have to specify **WHAT** you wish to donate.\n\nPlease try: \`${things.join('`, `')}\``));
                }
            case 'donors':
                whatDonorsDonate = args[1];
                switch(whatDonorsDonate){
                    case 'cubes':
                        ranking = await EmpireDonors.findAll({
                            where: { empireID: message.guild.id.toString() },
                        });
                        await ranking.forEach(b => leaderboard.set(b.userID, b));
                        await msg.delete();
                        return message.reply(leaderboard.sort((a, b) => b.strawberrycubes - a.strawberrycubes)
                            .filter(user => client.users.has(user.userID))
                            .first(10)
                            .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.strawberrycubes} Strawberrycubes`)
                            .join('\n'),
                            { code: true });
                    case 'sucrosium':
                        ranking = await EmpireDonors.findAll({
                            where: { empireID: message.guild.id.toString() },
                        });
                        await ranking.forEach(b => leaderboard.set(b.userID, b));
                        await msg.delete();
                        return message.reply(leaderboard.sort((a, b) => b.sucrosium - a.sucrosium)
                            .filter(user => client.users.has(user.userID))
                            .first(10)
                            .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.sucrosium} Sucrosium`)
                            .join('\n'),
                            { code: true });
                    case 'sucrons':
                        ranking = await EmpireDonors.findAll({
                            where: { empireID: message.guild.id.toString() },
                        });
                        await ranking.forEach(b => leaderboard.set(b.userID, b));
                        await msg.delete();
                        return message.reply(leaderboard.sort((a, b) => b.sucrons - a.sucrons)
                            .filter(user => client.users.has(user.userID))
                            .first(10)
                            .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.sucrons} Sucrons`)
                            .join('\n'),
                            { code: true });
                    default:
                        await msg.delete();
                        return message.reply(embed
                            .setThumbnail(message.author.displayAvatarURL)
                            .setColor(message.member.displayHexColor)
                            .setDescription(`Sorry, but you have to specify **WHICH** leaderboard you wish to see.\n\nPlease try: \`${things.join('`, `')}\``));
                }
            case 'check':
                if(dbEmpire.strawberrycubes < 1){
                    description = 'Our Empire appears to be **empty** on Strawberrycubes...';
                }
                else if(dbEmpire.strawberrycubes >= 1){
                    description = `Our Empire's impressive Strawberrycube treasury, currently holds: **${dbEmpire.strawberrycubes}** Strawberrycubes.`;
                }
                if(dbEmpire.sucrosium < 1){
                    description += '\n\nOur Empire appears to be **empty** on Sucrosium...';
                }
                else if(dbEmpire.sucrosium >= 1){
                    description += `\n\nOur Empire's impressive Sucrosium treasury, currently holds: **${dbEmpire.sucrosium}** Sucrosium.`;
                }
                if(dbEmpire.sucrons < 1){
                    description += '\n\nOur Empire appears to be **empty** on Sucrons...';
                }
                else if(dbEmpire.sucrons >= 1){
                    description += `\n\nOur Empire's impressive Sucrons treasury, currently holds: **${dbEmpire.sucrons}** Sucrons.`;
                }
                await msg.delete();
                return message.reply(embed
                    .setDescription(description));
            default:
                await msg.delete();
                return message.reply(embed
                    .setThumbnail(message.author.displayAvatarURL)
                    .setColor(message.member.displayHexColor)
                    .setDescription(`Sorry, but you have to specify **WHAT** you wish to do.\n\nPlease try: \`${whats.join('`, `')}\``));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};