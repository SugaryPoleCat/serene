const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Counters } = require('../../database/dbObjects');

const help = {
    name: 'action',
    description: 'To use this, please type one of the aliases instead, right after the prefix. This is made so, because, essentially these things do one and the same thing, for now. Maybe in the future they will be evolved.\nEXAMPLE: `!boop <@someone>`.',
    aliases: ['boop', 'cuddle', 'snuggle'],
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const target = message.mentions.users.first();
        const args2 = message.content.slice(botconfig.prefix.length).split(/ +/);
        // utils.log(args2);
        // utils.log(args2[0]);
        const thing = args2[0].toLowerCase();
        const embed = new Discord.RichEmbed()
            .setTitle(thing.toUpperCase())
            .setTimestamp()
            .setColor(utils.memberColor(message, target));

        if (thing == 'action') {
            return message.channel.send(embed
                .setColor(utils.randomHexColor('error'))
                .setTitle('ACTION')
                .setDescription(`Please read help for this command using \`!help action\` to understand what it does.\nYou are supposed to say \`!boop <@someone>\` in order to use this command. Look at 'aliases' in the help, to see which you can use.\n\nThe following commands are allowed: \`${help.aliases.join('`, `')}\`.`));
        }
        if(target == message.author.id) return message.reply(embed
            .setColor(utils.randomHexColor('warning'))
            .setThumbnail(message.author.displayAvatarURL)
            .setDescription(`As much as I want to let you do it, you can not **${thing}** yourself.`));
        // first we check if the thing is a thing that we allow to be done

        // now we fetch for the thing how often we performed this on someone else
        const [dbTarget] = await Counters.findOrCreate({
            where: { userID: target.id.toString(), fromID: message.author.id.toString(), thing: thing },
            defaults: { userID: target.id.toString(), fromID: message.author.id.toString(), thing: thing },
        });
        // and how often we performed this in general
        const [dbUser] = await Counters.findOrCreate({
            where: { userID: message.author.id.toString(), thing: thing },
            defaults: { userID: message.author.id.toString(), thing: thing },
        });

        // we increase both
        dbTarget.amount++;
        dbUser.amount++;

        let what;
        switch(thing){
            case 'boop':
                return what = 'booped';
            case 'cuddle':
                return what = 'cuddled';
            case 'snuggle':
                return what = 'snuggled';
            default:
                break;
        }
        // and save them to the DB
        await dbTarget.save();
        await dbUser.save();
        return message.reply(embed
            .setDescription(`You have done the **${thing}** to ${target}!\nYou've ${what} them **${dbTarget.amount}** times!\n\nIn total, you used __${thing}__ **${dbUser.amount}** times!`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};

 // let dbTarget = null;
        // const [booper] = utils.getUserHighlightFromMessage(message);
        // const [boopTarget, targetID] = utils.getUserHighlightFromStr(message.guild, args.join(' '));

        // const phrases = {
        //     bot: ['You are {$0}', 'mothafucka'],
        //     self: ['I am you', 'Soru is best'],
        //     target: ['You are target', 'we are target'],
        //     sugar: ['Well arent you special', 'oprah'],
        // };

        // let color = utils.randomHexColor('random');
        // let avatar = '';
        // if(targetID){
        //     [dbTarget] = await Users.findOrCreate({
        //         where: { user_id: targetID },
        //         defaults: { user_id: targetID },
        //     });
        //     avatar = message.guild.members.get(targetID).user.displayAvatarURL;
        //     color = message.guild.members.get(targetID).displayHexColor;
        // }
        // const embed = new Discord.RichEmbed()
        //     .setColor(color)
        //     .setThumbnail(avatar);

        // if(targetID == client.user.id){
        //     const yourStats = await dbUser.addAction('boop', targetID ? targetID : 'matrix');
        //     let theirStats = null;
        //     embed
        //         .setDescription(`**${booper} just booped me**\n\n**${utils.getPhrases(phrases.bot)}**`)
        //         .addField('You booped them:', yourStats, true);
        //     if(targetID){
        //         theirStats = await dbTarget.getActionTarget('boop');
        //         embed.addField('How often they got booped:', theirStats, true);
        //     }
        //     await message.channel.send(embed);
        // }
        // else if(targetID == message.author.id){
        //     embed.setDescription(`**${booper} just booped themself!**\n\n**${utils.getPhrases(phrases.self)}**`);
        //     await message.channel.send(embed);
        // }
        // else if(targetID == botconfig.botowners.ownerID){
        //     const yourStats = await dbUser.addAction('boop', targetID ? targetID : 'matrix');
        //     let theirStats = null;
        //     embed
        //         .setDescription(`**${booper} just booped ${boopTarget}**\n\n**${utils.getPhrases(phrases.sugar)}**`)
        //         .addField('You booped them:', yourStats, true);
        //     if(targetID){
        //         theirStats = await dbTarget.getActionTarget('boop');
        //         embed.addField('How often they got booped:', theirStats, true);
        //     }
        //     await message.channel.send(embed);
        // }
        // else{
        //     const yourStats = await dbUser.addAction('boop', targetID ? targetID : 'matrix');
        //     let theirStats = null;
        //     embed
        //         .setDescription(`**${booper} just booped ${boopTarget}**\n\n**${utils.getPhrases(phrases.target)}**`)
        //         .addField('You booped them:', yourStats, true);
        //     if(targetID){
        //         theirStats = await dbTarget.getActionTarget('boop');
        //         embed.addField('How often they got booped:', theirStats, true);
        //     }
        //     await message.channel.send(embed);
        // }