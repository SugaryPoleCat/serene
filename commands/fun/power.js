const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'power',
    description: 'Check how many Sucrons you have!',
    aliases: ['pow'],
    usage: '[@mention]\nCan be @ in the server, or the fancy <@ID>.',
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const target = message.mentions.users.first() || message.author;
        let member, displayName;
        if(message.channel.type != 'dm'){
            member = message.mentions.members.first() || message.member;
            displayName = member.displayName;
        }
        else{
            member = target;
            displayName = target.tag;
        }

        const embed = new Discord.RichEmbed()
            .setTitle('POWER')
            .setColor(utils.memberColor(message, target));

        const msg = await message.reply(embed
            .setDescription('Calculating....'));

        const [dbSucrons] = await Sucrons.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });

        const sucrons = await utils.sucronsCalc(target);
        dbSucrons.sucrons = await parseInt(sucrons);
        await dbSucrons.save();
        const eaten = parseInt(dbSucrons.eaten);
        const baseSucrons = await utils.nameCalc(target.username);

        await message.reply(embed
            .setTimestamp()
            .setThumbnail(target.displayAvatarURL)
            .setDescription(`${displayName}'s total Sucrons are: **${sucrons}**.\n\nMy my, aren't you mighty!\n\nYour raw Sucrons are: **${sucrons - eaten}** and you can share **${eaten}** Sucrons with others.\n\nYour base Sucrons are: **${baseSucrons}** Sucrons, without additional benefits.\nWith benefits, your raw Sucrons increase by: **${(sucrons - eaten) - baseSucrons}** Sucrons.\nWith benefits, your total Sucrons are increased by: **${sucrons - baseSucrons}** Sucrons.`));
        return msg.delete();
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};