const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Currencies, Levels, Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'ranking',
    description: 'Check who is at the top of the foodchain!',
    usage: '<cubes | sucrosium | exp | level | sucrons>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    delete: true,
    nsfw: false,
};

const typeMatch = ['money', 'cubes', 'balance'];
function findMatch(input){
    return typeMatch.find(input);
}

async function fox(message, args, client){
    try{
        const currency = new Discord.Collection();
        const level = new Discord.Collection();
        const sucrons = new Discord.Collection();
        let storeLMAO, levelLMAO, sucronsLMAO;
        const type = args[0];

        switch(type){
            case 'cubes':
                storeLMAO = await Currencies.findAll();
                storeLMAO.forEach(b => currency.set(b.userID, b));
                return message.reply(
                    currency.sort((a, b) => b.strawberrycubes - a.strawberrycubes)
                        .filter(user => client.users.has(user.userID))
                        .first(10)
                        .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.strawberrycubes} StrawberryCubes`)
                        .join('\n'),
                    { code: true });
            case 'sucrosium':
                return message.reply(
                    currency.sort((a, b) => b.sucrosium - a.sucrosium)
                        .filter(user => client.users.has(user.userID))
                        .first(10)
                        .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.sucrosium} Sucrosium`)
                        .join('\n'),
                    { code: true });
            case 'exp':
                levelLMAO = await Levels.findAll();
                levelLMAO.forEach(b => level.set(b.userID, b));
                return message.reply(
                    level.sort((a, b) => b.exp - a.exp)
                        .filter(user => client.users.has(user.userID))
                        .first(10)
                        .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.exp} Experience`)
                        .join('\n'),
                    { code: true });
            case 'level':
                levelLMAO = await Levels.findAll();
                levelLMAO.forEach(b => level.set(b.userID, b));
                return message.reply(
                    level.sort((a, b) => b.level - a.level)
                        .filter(user => client.users.has(user.userID))
                        .first(10)
                        .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.level} level`)
                        .join('\n'),
                    { code: true });
            case 'sucrons':
                sucronsLMAO = await Sucrons.findAll();
                sucronsLMAO.forEach(b => sucrons.set(b.userID, b));
                return message.reply(
                    sucrons.sort((a, b) => b.sucrons - a.sucrons)
                        .filter(user => client.users.has(user.userID))
                        .first(10)
                        .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}}: ${user.sucrons} sucrons`)
                        .join('\n'),
                    { code: true });
            default:
                return message.reply(new Discord.RichEmbed()
                    .setTitle('LEADERBOARD')
                    .setColor(utils.randomHexColor('error'))
                    .setTimestamp()
                    .setDescription(`I did not find **${type}** type of leaderboard.\n\nPlease try: \`cubes, sucrons, level, exp, sucrosium\``));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};