const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'ship',
    description: 'Calculate in % how compatible you or 2 other pones are!',
    usage: '<@someone else than you> [@another someone else]',
};
const config = {
    cooldown: 7,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

function trimAll(A){
    while(A.substring(0, 1) == ''){
        A = A.substring(1, A.length);
    }
    while(A.substring(A.length - 1, A.length) == ''){
        A = A.substring(0, A.length - 1);
    }
    return A;
}

function getNum(A){
    let outputNum = 0;
    for(let i = 0; i < A.length; i++){
        outputNum += A.charCodeAt(i);
    }
    return outputNum;
    }

function calc(name1, name2){
    let cnameone = trimAll(name1);
    let cnametwo = trimAll(name2);
    cnameone = cnameone.toLowerCase();
    cnametwo = cnametwo.toLowerCase();
    const totalNum = getNum(cnameone) * getNum(cnametwo);
    return parseInt(totalNum) % 100;
}

function uniq(a) {
    let seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

// let inputInfo = [];
// function createNames(target1, target2){
//         inputInfo.push(target1.username);
//         inputInfo.push(target2.username);

//         for(let i = 0; i < input)
// }

async function fox(message, args, client){
    try{
        let target1;
        let target2;
        if(args.length < 2){
            target1 = message.author;
            target2 = message.mentions.users.first();
        }
        else{
            target1 = message.mentions.users.first();
            target2 = message.mentions.users.last();
        }
        if(target1.id == target2.id){
            return message.reply('you can not mention yourself or someone else 2 times.');
        }

        return message.reply(new Discord.RichEmbed()
            .setTitle('SHIP')
            .setColor(utils.randomHexColor())
            .setTimestamp()
            .setDescription(`${target1} and ${target2} ship is calculated to be **${calc(target1.username, target2.username)}%** compatible.`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};