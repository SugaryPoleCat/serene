const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'avatar',
    description: 'This will fetch avatar of a user, that the bot has access to.',
    aliases: ['pfp'],
    usage: '[mention]\nMention can be either standard @ in the server or the more fancy <@ID> mention.',
};
const config = {
    cooldown: 5,
    delete: true,
    guildOnly: false,
    args: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const target = message.mentions.users.first() || message.author;
        return message.reply(new Discord.RichEmbed()
            .setTitle('AVATAR')
            .setTimestamp()
            .setColor(utils.memberColor(message, target))
            .setImage(target.displayAvatarURL));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};