const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'randomoc',
    description: 'Create a completeley random OC quickly!\nThis command is not fully finished yet, however, it does work.',
};
const config = {
    cooldown: 10,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

const OC = {
    name: '',
    age: 0,
    race: '',
    gender: '',
    eyeColor: '',
    hairColor: '',
    bodyColor: '',
    height: '',
    physiology: '',
    traits: '',
    defects: '',
    rareTraits: '',
    favStat: '',
    livingCon: '',
    technology: '',
    homeland: '',
    cutieMark: '',
    sexuality: '',
    masculinity: '',
    endowment: '',
    sexPreference: '',
    kink: '',
    creator: '',
};

const arr_racePick = ['Unicorn', 'Pegasus', 'Alicorn', 'Changeling', 'Earth Pony', 'Batpony', 'Dragon hybrid'];
const arr_genderPick = ['Stallion', 'Mare', 'Other'];
const arr_ageDesc = ['Young 4-11', 'Teen 12-17', 'Adult 18-35', 'Middle aged 36-60', 'Elder 60+'];
const arr_height = ['61cm - 74cm Stunted', '76cm - 84cm Short', '86cm - 112cm Average', '114cm - 137cm Tall', '140cm - 165cm Gigantic'];
const arr_phys = ['Cursed', 'Weak', 'Normal', 'Strong', 'Blessed'];
const arr_traits = ['Robust - Strong immune system.', 'Hoof-eye coordination - Heighened sense of balance.', 'Durable hooves - Increased stamina.', 'Thicker Limbs - Stronger than average.', 'Body Markings - Distinct Doscoloration of coat in a unique pattern.', 'Heterochromia.', 'Longer limbs (even horn and wings).', 'Thicker Coat - Resistant to cold and more fluff.', 'Ambidextrous - Skilled in using every limb', 'Double jointed - Really flexible'];
const arr_defects = ['Mangy coat - Less resitant to cold.', 'Frail - Prone to sickness.', 'Stunted - Reroll if already stunted.', 'Feral Tongue - Unable to speak.', 'Weak magic - Fliers struggle to fly, Unicorns find casting difficult, Earthponies struggle to grow things (make race specific).', 'Blind', 'Deaf', 'Nullification - Lacks magic and is unable to be affected by magic.', 'Thin limbs - Physically weaker body.', 'Albino - Sensitive to light.'];
const arr_rareTraits = ['Strong magic - Stronger flight, spells, connection to earth (make race specific).', 'Extraordinary senses - Able to passively sense magic.', 'Physically pristine - Stronger, faster, harder, better.', 'Able mind - Increased resistance to mind altering magic or potions.', 'Genius - Increased intellect, able to learn rapidly.'];
const arr_favStat = ['Strength', 'Dexterity', 'Constitution', 'Wisdom', 'Intelligence', 'Charisma'];
const arr_livingCon = ['Nomad', 'Exiled', 'Tribe', 'Civilization', 'Socially integrated - Ponies are a minority where you live.', 'Enslaved'];
const arr_tech = ['Tribal', 'Ancient', 'Medieval', 'Colonial', 'Industrial', 'Modern', 'Futuristic'];
const arr_homeland = ['Arctic', 'Arcane', 'Aquatic', 'Cavern', 'Coastal', 'Desert', 'Forest', 'Grassland', 'Islands', 'Jungle', 'Mountain', 'Ruins', 'Savannah', 'Swamp', 'Urban', 'Volcanic'];
const arr_cm = ['Soldier', 'Gatherer', 'Teacher', 'Healer', 'Inventor', 'Leader', 'Mystic', 'Adventurer', 'Crafter', 'Scribe'];
const arr_sexuality = ['Hetero', 'Bisexual', 'Homosexual', 'Pansexual'];
const arr_masculinity = ['1-49 Feminine', 'Androgynous', '51-100 Masculine'];
const arr_endowment = ['None', 'Tiny', 'Small', 'Average', 'Large', 'Well Endowed', 'Huge', 'Gigantic'];
const arr_sexPreference = ['Top', 'Bottom', 'Switch', 'Powerbottom'];
const arr_kinks = ['Hoof fetish', 'Costumes', 'Little play'];
const arr_names = ['name', 'name2'];

function randomiseOC(message){
    OC.name = message.member.displayName;
    OC.gender = arr_genderPick[Math.floor(Math.random() * arr_genderPick.length)];
    OC.age = arr_ageDesc[Math.floor(Math.random() * arr_ageDesc.length)];
    OC.race = arr_racePick[Math.floor(Math.random() * arr_racePick.length)];
    OC.height = arr_height[Math.floor(Math.random() * arr_height.length)];
    // IF PHYS  CURSED = 2 DEFECTS
    OC.physiology = arr_phys[Math.floor(Math.random() * arr_phys.length)];
    OC.favStat = arr_favStat[Math.floor(Math.random() * arr_favStat.length)];
    OC.livingCon = arr_livingCon[Math.floor(Math.random() * arr_livingCon.length)];
    OC.technology = arr_tech[Math.floor(Math.random() * arr_tech.length)];
    OC.homeland = arr_homeland[Math.floor(Math.random() * arr_homeland.length)];
    OC.cutieMark = arr_cm[Math.floor(Math.random() * arr_cm.length)];
    OC.sexuality = arr_sexuality[Math.floor(Math.random() * arr_sexuality.length)];
    // FIX MASCULINITY
    OC.masculinity = arr_masculinity[Math.floor(Math.random() * arr_masculinity.length)];

    switch(OC.physiology){
        case arr_phys[0]:
            for (let i = 0; i < 3; i++){
                OC.defects += arr_defects[Math.floor(Math.random() * arr_defects.length)];
            }
            break;
        case arr_phys[1]:
            for (let i = 0; i < 3; i++){
                OC.defects += arr_defects[Math.floor(Math.random() * arr_defects.length)];
            }
            OC.traits = arr_traits[Math.floor(Math.random() * arr_traits.length)];
            break;
        case arr_phys[2]:
            for (let i = 0; i < 3; i++){
                OC.traits += arr_traits[Math.floor(Math.random() * arr_traits.length)];
            }
            break;
        case arr_phys[3]:
            for (let i = 0; i < 3; i++){
                OC.traits += arr_traits[Math.floor(Math.random() * arr_traits.length)];
            }
            OC.rareTraits = arr_rareTraits[Math.floor(Math.random() * arr_rareTraits.length)];
            break;
        case arr_phys[4]:
            for (let i = 0; i < 3; i++){
                OC.rareTraits += arr_rareTraits[Math.floor(Math.random() * arr_rareTraits.length)];
            }
            OC.traits = arr_traits[Math.floor(Math.random() * arr_traits.length)];
            break;
    }
    // const membur = message.guild.members.find(u => u.id == message.author.id);
    OC.creator = message.author;
    OC.eyeColor = utils.randomHexColor('random');
    OC.hairColor = utils.randomHexColor('random');
    OC.bodyColor = utils.randomHexColor('random');
}

function randomiseOCnsfw(){
    OC.endowment = arr_endowment[Math.floor(Math.random() * arr_endowment.length)];
    OC.sexPreference = arr_sexPreference[Math.floor(Math.random() * arr_sexPreference.length)];
    OC.kink = arr_kinks[Math.floor(Math.random() * arr_kinks.length)];
}

const nameQuestion = response => {
    return OC.name = response.toString();
};

async function fox(message, args, client){
    try{
        let ocDesc = '';
        randomiseOC(message);
        const repl = `Name: ${OC.name}\nGender: ${OC.gender}\nAge: ${OC.age}\nRace: ${OC.race}\nHeight: ${OC.height}\nPhysiology: ${OC.physiology}\nDefects: ${OC.defects}\nTraits: ${OC.traits}\nRare Traits: ${OC.rareTraits}\nFavourite Stat: ${OC.favStat}\nLiving Condition: ${OC.livingCon}\nTechnology: ${OC.technology}\nHomeland: ${OC.homeland}\nCutie Mark: ${OC.cutieMark}\nSexuality: ${OC.sexuality}\nMasculinity: ${OC.masculinity}\nEye Color: ${OC.eyeColor}\nBody Color: ${OC.bodyColor}\nHair Color: ${OC.hairColor}\nCreated by: ${OC.creator}\n`;
        return message.channel.send(repl);
        // return utils.workingEmbed(message, help.name);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};