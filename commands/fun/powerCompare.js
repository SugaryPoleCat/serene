const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'powercompare',
    description: 'Compare your sucrons to someone else, or compare 2 different ponies\' power!',
    aliases: ['pwc'],
    usage: '<@someone other than you> [@another someone other than you]',
};
const config = {
    cooldown: 10,
    guildOnly: true,
    args: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let target1;
        let target2;
        if(args.length < 2){
            target1 = message.author;
            target2 = message.mentions.users.first();
        }
        else{
            target1 = message.mentions.users.first();
            target2 = message.mentions.users.last();
        }
        if(target1.id == target2.id){
            return message.reply('you can not mention yourself or someone else 2 times.');
        }
        const [dbTarget1] = await Sucrons.findOrCreate({
            where: { userID: target1.id.toString() },
            defaults: { userID: target1.id.toString() },
        });
        const [dbTarget2] = await Sucrons.findOrCreate({
            where: { userID: target2.id.toString() },
            defaults: { userID: target2.id.toString() },
        });
        dbTarget2.sucrons = await utils.sucronsCalc(target2);
        dbTarget1.sucrons = await utils.sucronsCalc(target1);
        await dbTarget1.save();
        await dbTarget2.save();
        const target1Sucorns = parseInt(dbTarget1.sucrons);
        const target2Sucrons = parseInt(dbTarget2.sucrons);
        let results;
        if(target1Sucorns > target2Sucrons){
            results = parseInt(utils.mathNumbers(target1Sucorns, target2Sucrons, '-'));
            return message.channel.send(target1 + '\nYou are stronger by: ' + results);
        }
        else if(target2Sucrons > target1Sucorns){
            results = parseInt(utils.mathNumbers(target2Sucrons, target1Sucorns, '-'));
            return message.channel.send(target2 + '\nYou are stronger by: ' + results);
        }
        else if(target1Sucorns == target2Sucrons){
            return message.channel.send('They are both equal in might...');
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};