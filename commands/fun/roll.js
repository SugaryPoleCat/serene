const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'roll',
    description: 'Roll a dice!\n... I mean, what else is there to say.',
    usage: '<dX or YdX>\nWhere X is the number of sides and Y is how many to roll.',
};
const config = {
    cooldown: 5,
    guildOnly: false,
    args: true,
    delete: true,
    nsfw: false,
};

let total = 0;
let arr_totals = [];

function getTotal(times, rolling){
    if(!times){
        times = 1;
    }
    for(let i = 0; i < times; i++){
        const rolled = getRoll(rolling);
        arr_totals.push(rolled);
        total += rolled;
    }
}
function getRoll(rolling){
    const ayyroll = Math.floor((Math.random() * rolling) + 1);
    return ayyroll;
}

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('ROLL')
            .setTimestamp()
            .setColor(utils.memberColor(message, message.author));
        // const curated = message.content.substring(message.content.indexOf(' ') + 1);
        const curated = args[0];
        const matches = curated.match(/^(\d*)d(\d+)$/);
        const amount = matches[2];
        const multiplier = matches[1];
        if(isNaN(matches[2])){
            return message.reply(embed
                .setDescription('It **__NEEDS__** to be in following format: `d20`. D, followed by a number. I can not process things, that are not numbers.'));
        }
        if(isNaN(matches[1]) && !matches[1]){
            return message.reply(embed
                .setDescription('It **__NEEDS__** to be in following format: `2d20`. D, followed by a number and preceeded with a number. I can not process things, that are not numbers.'));
        }
        if(amount > 1000){
            return message.reply(embed
                .setDescription('Please do not go over 1000.'));
        }
        if(amount < 2){
            return message.reply(embed
                .setDescription('Please do not go below 1.'));
        }
        if(multiplier > 100){
            return message.reply(embed
                .setDescription('Please do not go over 100.'));
        }
        getTotal(multiplier, amount);
        embed
            .setDescription(`Your roles\nTotal: **${total}**, rolls: **${arr_totals.join(', ')}**`);
        arr_totals.length = 0;
        total = 0;
        return message.reply(embed);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};