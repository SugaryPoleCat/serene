const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');
const { Currencies, Sucrons } = require('../../database/dbObjects');

const help = {
    name: 'eat',
    description: 'Eat some Strawberrycubes to increase your Sucrons!',
};
const config = {
    cooldown: 7,
    guildOnly: false,
    args: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('EAT');

        const target = message.author;
        const msg = await message.reply(embed
            .setDescription('Gathering food...')
            .setColor(utils.memberColor(message, target)));
        const [dbCurr] = await Currencies.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        const [dbSucrons] = await Sucrons.findOrCreate({
            where: { userID: target.id.toString() },
            defaults: { userID: target.id.toString() },
        });
        // let stealChance;
        // if(target.id != message.author.id){
        //     if(target.presence.status('idle')) stealChance = 1000;
        // }
        // ^ HAVE TO THINK OF STEALING.
        if(dbCurr.strawberrycubes == 0){
            await msg.delete();
            return message.reply(embed
                .setColor(utils.randomHexColor('error'))
                .setThumbnail(message.author.displayAvatarURL)
                .setDescription('You tried to eat some of your money, but failed miserably.\n\nMaybe because you have **0**__ Strawberrycubes__?'));
        }
        let amount = await Math.floor(Math.random() * (100 - 1 + 1)) + 1;
        if(amount > dbCurr.strawberrycubes){
            amount = await parseInt(dbCurr.strawberrycubes);
            dbCurr.strawberrycubes -= await parseInt(amount);
            await dbCurr.save();
            dbSucrons.eaten += await parseInt(amount);
            await dbSucrons.save();
            await msg.delete();
            return message.reply(embed
                .setThumbnail(message.author.displayAvatarURL)
                .setColor(utils.memberColor(message, target))
                .setDescription('You decided to eat the rest of your __Strawberrycubes__, you fat slob.\nYou now **0** __Strawberrycubes__ left.\n\nWas it worth it?\nSure, yes, you grow in power, but now you are poorino.'));
        }
        const superChance = await Math.floor(Math.random() * (100 - 1 + 1)) + 1;
        if(superChance > 75){
            amount = await Math.floor(Math.random() * (1000 - 100 + 1)) + 100;
            if(amount > dbCurr.strawberrycubes){
                amount = await parseInt(dbCurr.strawberrycubes);
                dbCurr.strawberrycubes -= await parseInt(amount);
                await dbCurr.save();
                dbSucrons.eaten += await parseInt(amount);
                await dbSucrons.save();
                await msg.delete();
                return message.reply(embed
                    .setThumbnail(message.author.displayAvatarURL)
                    .setColor(utils.memberColor(message, target))
                    .setDescription('You decided to eat the rest of your __Strawberrycubes__, you fat slob.\nYou now **0** __Strawberrycubes__ left.\n\nWas it worth it?\nSure you grow in power, but now you are poorino.'));
            }
            dbCurr.strawberrycubes -= await parseInt(amount);
            await dbCurr.save();
            dbSucrons.eaten += await parseInt(amount);
            await dbSucrons.save();
            await msg.delete();
            return message.reply(embed
                .setThumbnail(message.author.displayAvatarURL)
                .setColor(utils.memberColor(message, target))
                .setDescription(`You felt extra hungry today and decided to eat **${amount}** __Strawberrycubes__.\nAren't you a big fat slob...\n\nYour treasury is left with **${dbCurr.strawberrycubes}** __Strawberrycubes__.\n\nYour power grows...`));
        }
        else{
            dbCurr.strawberrycubes -= await parseInt(amount);
            await dbCurr.save();
            dbSucrons.eaten += await parseInt(amount);
            await dbSucrons.save();
            await message.reply(embed
                .setThumbnail(message.author.displayAvatarURL)
                .setColor(utils.memberColor(message, target))
                .setDescription(`You eat a delicious hefty amount of **${amount}** __Strawberrycubes__.\n\nYour treasury is left with **${dbCurr.strawberrycubes}** __Strawberrycubes__.\n\nYour power grows...`));
            return msg.delete();
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};