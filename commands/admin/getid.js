const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'getid',
    description: 'This will get ID of a role, channel, or user, as long as it\'s mentionable.',
    usage: '<r (role) | u (user) | c (channel)>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    admin: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        let target;
        if(args[0] == 'r' || args[0] == 'role'){
            target = message.mentions.roles.first();
        }
        else if(args[0] == 'u' || args[0] == 'user'){
            target = message.mentions.users.first();
        }
        else if(args[0] == 'c' || args[0] == 'channel'){
            target = message.mentions.channels.first();
        }
        else{
            return message.reply('you dummass read how to use it');
        }
        console.log('target ' + target + ' what ' + args[0] + ' target id ' + target.id);
        const embed = new Discord.RichEmbed()
            .setTitle('This is the ID')
            .setTimestamp()
            .setThumbnail(message.author.displayAvatarURL)
            .setDescription(target.id);
        return message.channel.send(embed);
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};