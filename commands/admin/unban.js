const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');

const help = {
    name: 'unban',
    description: 'This will unban a user.',
    usage: '<@someone (can be a direct @, or <@userID>, writing <@ manually.)> [reason]',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: true,
    admin: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('UNBAN')
            .setTimestamp();
        if(!args[0]) return message.reply(embed
            .setColor(utils.randomHexColor('error'))
            .setDescription('I need a mention.'));
        const target = utils.userMention(args[0], client);
        let reason = message.content.slice(7 + args[0].length);
        if(reason == '' || !reason) reason = 'No reason provided.';
        await message.guild.unban(target.id, reason);
        return message.reply(embed
            .setColor(utils.memberColor(message, target))
            .setDescription(`**${target}** has been unbanned.\n\n**REASON:** ${reason}`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};