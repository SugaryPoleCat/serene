const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');

const help = {
    name: 'sponsor',
    description: 'This lets admins quickly create a cool sponsor link for our sponsors!',
    usage: '<link> <description>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    admin: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        // TODO:
        // add image before the message is sent.
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('SPONSOR')
            .setColor(utils.memberColor(message));
        if(args[0] == '' || args[0] == null) return message.reply(embed
            .setColor(utils.randomHexColor('error'))
            .setThumbnail(message.author.displayAvatarURL)
            .setDescription('Please put in a link.'));
        if(args[1] == '' || args[1] == null) return message.reply(embed
            .setColor(utils.randomHexColor('error'))
            .setThumbnail(message.author.displayAvatarURL)
            .setDescription('Please put in some description text.'));
        const channel = message.guild.channels.find(ch => ch.name == 'sponsored-servers');
        const now = new Date().toUTCString();
        await channel.send(embed
            .setTitle('NEW SPONSOR')
            .setDescription(`${message.content.slice(7 + args[0].length)}`)
            .addBlankField()
            .addField('DATE', now, false)
            .addField('LINK', args[0], false));
        return channel.send(args[0] + '\n||until i solve the issue, to get the pic, i have to send the link as seperate message||');
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};