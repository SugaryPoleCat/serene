const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Banks, UserBank } = require('../../database/dbObjects');

const help = {
    name: 'adminbank',
    description: 'This handles banking system',
    aliases: ['abank'],
    usage: '<check | reset | list>\nRESET: <@someone>. Use reset only when someone can\'t access their bank, as in: it throws an error like "ID not found". So make sure to set up a log channel.\nCHECK: <@someone>\nLIST: no specifications.',
};
const config = {
    cooldown: 1,
    guildOnly: false,
    args: true,
    admin: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const bankList = new Discord.Collection();
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('ADMIN BANK');
        const what = args[0];
        const regex = /\{([^]+)\}/g;
        // Figure out regex.
        let name, openFee, upgradeCost, interestRate, security, target, id, bank, userBank, bankListSorted, addOptions;
        switch(what){
            case 'reset':
                // RESET SOMEONES BANK
                target = message.mentions.users.first();
                await UserBank.destroy({
                    where: { userID: target.id.toString() },
                });
                await target.send(embed
                    .setColor(utils.memberColor(message, target))
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription('Your bank has been reset, due to changes in banking system.\nPlease review the list of banks again.'));
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(message.author.displayAvatarURL)
                    .setDescription('The user\'s bank has been reset.'));
            case 'check':
                // CHECK IS TO CHECK WHAT BANK SOMEONE HAS
                target = message.mentions.users.first();
                userBank = await UserBank.findOne({
                    where: { userID: target.id.toString() },
                });
                if(!userBank){
                    return message.reply(embed
                        .setColor(utils.randomHexColor('error'))
                        .setThumbnail(target.displayAvatarURL)
                        .setDescription('They have no bank.'));
                }
                bank = await Banks.findOne({
                    where: { bankID: userBank.bankID },
                });
                return message.reply(embed
                    .setColor(utils.memberColor(message, target))
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription(`Their bankID is: **${userBank.bankID}** with the name **${bank.name}**\n
                    They have:
                    __Strawberrycubes__: **${userBank.strawberrycubes}**
                    __Sucrosium__: **${userBank.sucrosium}**`));
            case 'list':
                // this lists all banks.
                bank = await Banks.findAll();
                if(!bank) return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setThumbnail(target.displayAvatarURL)
                    .setDescription('There are no banks.'));

                bank.forEach(b => bankList.set(b.bankID, b));
                bankListSorted = bankList.sort((a, b) => a.bankID - b.bankID)
                    .map((banky, position) => `__ID__: **${position}**\n__NAME__: **${banky.name}**\nCost to open: **${banky.openFee}**`)
                    .join('\n\n');
                return message.reply(embed
                    .setColor(utils.memberColor(message, message.author))
                    .setDescription(bankListSorted));
            default:
                // Throw an error
                return message.reply(embed
                    .setColor(utils.randomHexColor('error'))
                    .setDescription('No such argument exists'));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};