const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { Warnings } = require('../../database/dbObjects');

const help = {
    name: 'warn',
    description: 'Warn a user, that they have done something bad.',
    usage: '<@mention> [reason | pardon | reset | list]\nFor reason you need to additionally provide a reason text.\n',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: false,
    admin: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const now = Math.floor(new Date().getTime() / 1000);
        // I want for mentions to also handle case, if I want to check the user's warns without having to @ them. I want to instead input <@userid> and check them from for example a hidden staff channel.
        const target = message.mentions.users.first();
        const member = message.mentions.members.first();
        const logChannel = message.guild.channels.find(ch => ch.name == settings.channels.logWarnings);
        // let args2 = message.content.slice(botconfig.prefix.length).split(/ +/g);
        if(target.id == message.author.id){
            return message.reply('You can not warn yourself');
        }
        if(target.bot == true){
            return message.reply('You can not mention bots.');
        }
        const msg = await message.channel.send('Warning...');
        let dbWarnings;

        const what = args[1];

        // oh boy
        let reasonText;
        let curatedContent;
        let cutLength;
        let warnCount;
        let hasBeenKicked = '';
        let pardonID;
        const listCollection = await new Discord.Collection();
        let list;
        switch(what){
            case 'reason':
                cutLength = 6 + target.id.length + 1 + what.length + 1 + 3;
                reasonText = message.content.slice(cutLength);
                await Warnings.create({
                    userID: target.id.toString(),
                    text: reasonText,
                    date: now,
                });
                warnCount = await Warnings.count({
                    where: { userID: target.id.toString() },
                });
                if(warnCount >= 5){
                    await member.kick(reasonText);
                    hasBeenKicked = '\n\nAdditionally, they have reached the **Warning** threshold and have been automatically kicked.';
                }
                return message.reply(`Target: ${target} and member: ${member}. Their warn count is now: ${warnCount}.\nAdded new warn now. Reason: ${reasonText}.${hasBeenKicked}`);
            case 'pardon':
                pardonID = parseInt(args[2]);
                if(!pardonID || isNaN(pardonID)){
                    return message.reply('I need an ID dumass');
                }
                await Warnings.destroy({
                    where: { warningID: pardonID },
                });
                warnCount = await Warnings.count({
                    where: { userID: target.id.toString() },
                });
                return message.reply(`Target: ${target} and member: ${member}. Their warn count is now: ${warnCount}.\nThey have been pardonend.`);
            case 'reset':
                await Warnings.destroy({
                    where: { userID: target.id.toString() },
                });
                warnCount = await Warnings.count({
                    where: { userID: target.id.toString() },
                });
                return message.reply(`Target: ${target} and member: ${member}. Their warn count is now: ${warnCount}.\nThey have been reset.`);
            case 'list':
                list = await Warnings.findAll({
                    where: { userID: target.id.toString() },
                });
                await list.forEach(b => listCollection.set(b.warningID, b));
                await msg.delete();
                return message.reply(listCollection.sort((a, b) => a.warningID - b.warningID)
                    .filter(user => client.users.has(user.userID))
                    .first(10)
                    .map((user, position) => `(${position + 1}) ${(client.users.get(user.userID).tag)}: ${user.strawberrycubes} Strawberrycubes`)
                    .join('\n'),
                    { code: true });
            default:
                reasonText = 'No reason provided.';
                await Warnings.create({
                    userID: target.id.toString(),
                    text: reasonText,
                    date: now,
                });
                warnCount = await Warnings.count({
                    where: { userID: target.id.toString() },
                });
                if(warnCount >= 5){
                    await member.kick(reasonText);
                    hasBeenKicked = '\n\nAdditionally, they have reached the **Warning** threshold and have been automatically kicked.';
                }
                return message.reply(`Target: ${target} and member: ${member}. Their warn count is now: ${warnCount}.\nAdded new warn now. Reason: ${reasonText}.${hasBeenKicked}`);
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};