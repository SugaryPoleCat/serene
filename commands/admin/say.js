const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'say',
    description: 'Say something in the mentioned channel!',
    usage: '<#channel> <c (code) | f (fancy) | n (normal)> <text>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: true,
    admin: true,
    delete: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const channelTarget = message.mentions.channels.first();
        const type = args[1].toLowerCase();
        const cutLength = 1 + help.name.length + 1 + type.length + 1 + channelTarget.id.length + 1 + 2;
        const sayThing = message.content.slice(cutLength);
        const embed = new Discord.RichEmbed()
                .setColor(message.member.displayHexColor)
                .setDescription(sayThing);
        if(type == 'c' || type == 'code'){
            await channelTarget.send(sayThing, { code: true });
        }
        else if(type == 'n' || type == 'normal'){
            await channelTarget.send(sayThing);
        }
        else if(type == 'f' || type == 'fancy'){
            await channelTarget.send(embed);
        }
        else{
            await message.channel.send(embed.setTitle('SAY').setColor(utils.randomHexColor('warning')).setDescription('You did not provide the correct format for what I should send. Try again and read the help.'));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};