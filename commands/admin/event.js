const Discord = require('discord.js');
const confgiJSON = require('../../config/botconfig.json');
const utils = require('../../utils');

const help = {
    name: 'event',
    description: 'This lets admins create a new event quickly.',
    usage: '<description>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: true,
    delete: false,
    nsfw: false,
};

async function fox(message, args, client){
    // TODO:
    // Add image before the mesage is sent.
    try{
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle('EVENT')
            .setColor(utils.memberColor(message));
        if(message.content.slice(7) == '' || message.content.slice(7) == null) return message.reply(embed
            .setColor(utils.randomHexColor('error'))
            .setThumbnail(message.author.displayAvatarURL)
            .setDescription('Please put in something, to paste into the event description. Max: **2048** letters, including spaces.'));
        const channel = message.guild.channels.find(ch => ch.name == 'events');
        const now = new Date().toUTCString();
        return channel.send(embed
            .setTitle('NEW EVENT')
            .addBlankField()
            .addField('DATE', now, false)
            .setDescription(`${message.content.slice(7)}`));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};