const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { dbSettings } = require('../../database/dbObjects');

const help = {
    name: 'ban',
    description: 'This will ban a user, write a log of **WHO** banned, why and when.',
    usage: '<@someone (can be a direct @, or <@userID>, writing <@ manually.)> [reason]',
};
const config = {
    cooldown: 1,
    guildOnly: true,
    args: true,
    admin: true,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        // Re-usable.
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setThumbnail(message.author.displayAvatarURL)
            .setColor(utils.randomHexColor('error'))
            .setTitle('BAN');

        // Check which user has been mentioned.
        const target = message.mentions.users.first();
        // Also get member.
        const member = message.mentions.members.first();
        // Try to find the STAFF id.
        const targetStaffRole = member.roles.find(r => r.id == dbSettings.adminRoleID);
        if(targetStaffRole == undefined || !targetStaffRole) return message.reply(embed
            .setDescription('You do not have the required role.'));
        // How much to cut the message.content.
        const cutLength = botconfig.prefix.length + help.name.length + 3 + target.id.toString().length + 2;
        // Define reason from the remainng message.content after slce.
        let banReason = message.content.slice(cutLength);
        // Check if we provded one.
        if(!banReason){
            banReason = 'No reason provided.';
        }

        // Could use SWITCH instead but felt IF was better.
        if(!target) return message.reply(embed
                .setDescription('You have to provide someone for me to ban.\n\nPlease provide the right mention next time. I hate when my time is being wasted.'));
        else if(target.id == message.author.id) return message.reply(embed
                .setDescription(`You can not ban yourself.\n\nIf you are this suicidal, ask <@${botconfig.botowners.ownerID}>, or <@${botconfig.botowners.coownerID}> to get banned.`));
        else if(target.id == botconfig.botowners.ownerID) return message.reply(embed
                .setDescription('You can not ban botowners.'));
        else if(target.id == message.guild.ownerID) return message.reply(embed
                .setDescription('You can not ban server owners.'));
        else if(targetStaffRole == settings.staffID && (message.author.id != message.guild.ownerID || !Object.values(botconfig.botowners).includes(message.author.id))) return message.reply(embed
                .setDescription(`You can not ban other staff members.\n\nIf you wish a staff member, to be banned, tell <@${message.guild.ownerID}> and tell them why you wish to ban ${target}`));
        else if(target.id == client.user.id) return message.reply(embed
                .setDescription('You can not ban me...\n\nAre you insane? Do you have a death wish?'));
        else{
            const logChannel = message.guild.channels.find(ch => ch.id == settings.channels.logGuild);
            await message.reply(embed
                .setColor(message.member.displayHexColor)
                .setThumbnail(target.displayAvatarURL)
                .setDescription(`${target} has been banned.\n\n**They were banned by: **${message.author}\n**Reason: **${banReason}`));
            await logChannel.send(embed
                .setColor(message.member.displayHexColor)
                .setThumbnail(target.displayAvatarURL)
                .setDescription(`${target.tag} has been banned.\n**Their ID: **${target.id}\n\n**They were banned by: **${message.author}\n**Their ID: **${message.author.id}\n\n**Reason: **${banReason}`));
            // return message.guild.ban(target.id, { days: 0, reason: banReason });
        }

        // this was supposed to be ban count.

        // else{
        //     [dbUser] = await Users.findOrCreate({
        //         where: { user_id: target.id },
        //         defaults: { user_id: target.id },
        //     });
        //     if(banReason == null){
        //         dbUser.addThing2();
        //     }
        // }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};