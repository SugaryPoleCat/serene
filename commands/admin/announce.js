const Discord = require('discord.js');
const settings = require('../../config/settings.json');
const utils = require('../../utils');

const help = {
    name: 'announce',
    description: 'This will create a new announcement on the server.',
    usage: '<n (normal) | f (fancy) | c (code)> <my text>',
};
const config = {
    cooldown: 10,
    guildOnly: true,
    args: true,
    admin: true,
    delete: false,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        // Find the right channel.
        const announce = message.guild.channels.find(ch => ch.name == settings.channels.announce);
        // AS in if its fancy or not.
        const type = args[0].toLowerCase();
        // Get the date.
        const now = new Date().toUTCString();
        // Create an embed to be re-used through the command.
        const embed = new Discord.RichEmbed()
            .setTimestamp()
            .setColor(message.member.displayHexColor)
            .setTitle(`NEW ANNOUNCEMENT \n${now}`);
        const failEmbed = new Discord.RichEmbed()
            .setTitle('ANNOUNCEMENT')
            .setColor(utils.randomHexColor('error'))
            .setTimestamp()
            .setDescription('I **NEED** some text to announce, I can not announce an empty message.');
        // Cut the message.content into a new string, that will be the announcement mesage.
        const curatedContent = message.content.slice(11 + type.length);
        // This will check whether the user provided the right thing.
        switch(type){
            case 'f':
                // This IF wil check if user provided any message.
                if(curatedContent == undefined || !curatedContent) return failEmbed;
                return announce.send(embed
                    .setDescription(curatedContent));
            case 'n':
                // This IF wil check if user provided any message.
                if(curatedContent == undefined || !curatedContent) return failEmbed;
                await announce.send(embed);
                return announce.send(curatedContent);
            case 'c':
                // This IF wil check if user provided any message.
                if(curatedContent == undefined || !curatedContent) return failEmbed;
                await announce.send(embed);
                return announce.send(curatedContent, { code: true });
            default:
                return message.reply(failEmbed
                    .setDescription('You did not provide the correct format for what I should send. Try again, or use `!help announce` to know how to use this command.'));
        }
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}
module.exports = {
    help, config, fox,
};