const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { ServerSettings } = require('../../database/dbObjects');

const help = {
    name: 'adminrole',
    description: 'With this, you can set which role has access to my admin commands, like BAN. Use `set` and then mention a role to set, use `reset` to remove the role. You do not need to use `reset` before setting a new admin role.',
    aliases: ['arole'],
    usage: '<set | reset>\nSET: <@mention the role>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
    owner: true,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setTitle('ADMIN ROLE')
            .setColor(utils.memberColor(message))
            .setTimestamp();
        let target, dbSettings, deletedRole, description;
        const what = args[0];
        switch(what){
            case 'set':
                target = message.mentions.roles.first();
                if(target == message.mentions.users.first() || target == message.mentions.channels.first() || target == message.mentions.members.first()) return message.reply(embed
                    .setColor(utils.randomHexColor('warning'))
                    .setDescription('You must mention a **ROLE**.'));
                [dbSettings] = await ServerSettings.findOrCreate({
                    where: { serverID: message.guild.id.toString() },
                    defaults: { serverID: message.guild.id.toString() },
                });
                dbSettings.adminRoleID = target.id.toString();
                await dbSettings.save();
                description = `I have set the admin role to ${target}.\n\nAnyone with that role will now be able to use my admin commands.`;
                break;
            case 'reset':
                dbSettings = await ServerSettings.findOne({
                    where: { serverID: message.guild.id.toString() },
                });
                if(dbSettings.adminRoleID == '' || !dbSettings.adminRoleID) return message.reply(embed
                    .setColor(utils.randomHexColor('warning'))
                    .setDescription('You have not set an admin role.\nBefore removing, please set it first.'));
                deletedRole = message.guild.roles.find(r => r.id == dbSettings.adminRoleID);
                dbSettings.adminRoleID = '';
                await dbSettings.save();
                description = `${deletedRole} has been removed from admin privilages.\n\nYou can set a different one, or remain without an admin role, enabling only you to use the admin commands.`;
                break;
            default:
                embed
                    .setColor(utils.randomHexColor('warning'));
                description = 'Please use `set` or `reset` and then mention the **Role** you wish to set.';
                break;
        }
        return message.reply(embed
            .setDescription(description));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};