const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { ServerChannels } = require('../../database/dbObjects');

const help = {
    name: 'adminchannel',
    description: 'LOG = general log. MESSAGES = message log. MEMBERS = Member changes log.\nIf no other cahnnels, than the log channel is set, the other log channels will use general log channel.',
    aliases: ['achannel', 'achl'],
    usage: '<set | reset | list> <log | messages | members | roles | emojis>(LOGS CHANNELS) | <news | welcome | booster | announcements>(INFORMATIONAL CHANNELS) <#channelmention>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
    owner: true,
};

const embed = new Discord.RichEmbed()
            .setTitle('ADMIN CHANNELS');

function checkTarget(message, target){
    target = message.mentions.channels.first();
    if(target == message.mentions.users.first() || target == message.mentions.roles.first() || target == message.mentions.members.first()) return message.reply(embed
        .setColor(utils.randomHexColor('error'))
        .setTimestamp()
        .setDescription('You must mention a **CHANNEL**.'));
    return target;
}

async function fox(message, args, client){
    try{
        embed
            .setTimestamp()
            .setColor(utils.memberColor(message));
        const what = args[0];
        const typeMatch = ['log', 'messages', 'members', 'roles', 'emojis', 'channels', 'news', 'welcome', 'booster', 'announcements'];
        let target, dbChannels, deletedChannel, description;
        let logGuild, logMessages, logMembers, logRoles, logEmojis, logChannels, newsCh, welcomeCh, boosterCh, announceCh;
        const what2 = args[1];
        switch(what){
            case 'set':
                switch(what2){
                    case 'log':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.logGuild = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **General Log** channel to ${target.name}\n\nAny log messages from enabled modules will display there.`;
                        break;
                    case 'messages':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.logMessages = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Messages Log** channel to ${target.name}\n\nDeleted messages will appear there.`;
                        break;
                    case 'members':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.logMembers = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Members Log** channel to ${target.name}\n\nIf a **MEMBER** of your server changes anything, the information will appear there, like their nickname, roles, or colours.`;
                        break;
                    case 'roles':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.logRoles = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Roles Log** channel to ${target.name}\n\nIf a **ROLE** of your server changes anything, the information will appear there, like their name, or colour.`;
                        break;
                    case 'emojis':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.logEmojis = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Emojis Log** channel to ${target.name}\n\nIf an **EMOJI** of your server changes anything, the information will appear there, like their name.`;
                        break;
                    case 'channels':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.logChannels = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Channels Log** channel to ${target.name}\n\nIf a **CHANNEL** of your server changes anything, the information will appear there, like their name.`;
                        break;
                    case 'news':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.news = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **News** channel to ${target.name}\n\nThings like __BOOSTS__, __WELCOMES__ and __ANNOUNCEMENTS__ will appear there. If you wish to have announcements in a different channel, please set the **Announcement** channel.`;
                        break;
                    case 'welcome':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.welcome = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Welcome** channel to ${target.name}\n\nNew members will be welcomed there.`;
                        break;
                    case 'booster':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.booster = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Booster** channel to ${target.name}\n\nMembers who boosted the server, will appear there.`;
                        break;
                    case 'announcements':
                        checkTarget(message, target);
                        [dbChannels] = await ServerChannels.findOrCreate({
                            where: { serverID: message.guild.id.toString() },
                            defaults: { serverID: message.guild.id.toString() },
                        });
                        dbChannels.announcement = target.id.toString();
                        await dbChannels.save();
                        description = `I have set the **Announcements** channel to ${target.name}\n\nAny announcement you post will appear there.`;
                        break;
                    default:
                        embed
                            .setColor(utils.randomHexColor('warning'));
                        description = `Please use either: \`${typeMatch.join('`, `')}\`.`;
                        break;
                }
                break;
            case 'reset':
                switch(what2){
                    case 'log':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.logGuild == '' || !dbChannels.logGuild) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **General Log**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.logGuild);
                        dbChannels.logGuild = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **General Log** channel.\n\nYou no longer have a channel for **General** things, such as errors.`;
                        break;
                    case 'messages':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.logMessages == '' || !dbChannels.logMessages) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Messages Log**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.logMessages);
                        dbChannels.logMessages = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Messages Log** channel.\n\nYou no longer have a channel for **Message** things, such as deleted messages by the bot (for now).\n\nIf you have a **General Log** set, **Messages Log** will appear there instead (if module is enabled.)`;
                        break;
                    case 'members':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.logMembers == '' || !dbChannels.logMembers) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Members Log**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.logMembers);
                        dbChannels.logMembers = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Members Log** channel.\n\nYou no longer have a channel for **Members** things, such as role changes, or nickname changes.\n\nIf you have a **General Log** set, **Messages Log** will appear there instead (if module is enabled.)`;
                        break;
                    case 'roles':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.logRoles == '' || !dbChannels.logRoles) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Roles Log**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.logRoles);
                        dbChannels.logRoles = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Roles Log** channel.\n\nYou no longer have a channel for **Role** things, such as name changes, or colour changes.\n\nIf you have a **General Log** set, **Roles Log** will appear there instead (if module is enabled.)`;
                        break;
                    case 'emojis':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.logEmojis == '' || !dbChannels.logEmojis) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Emojs Log**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.logEmojis);
                        dbChannels.logEmojis = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Emojis Log** channel.\n\nYou no longer have a channel for **Emoji** things, such as name changes.\n\nIf you have a **General Log** set, **Emojis Log** will appear there instead (if module is enabled.)`;
                        break;
                    case 'channels':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.logChannels == '' || !dbChannels.logChannels) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Emojs Log**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.logChannels);
                        dbChannels.logChannels = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Channels Log** channel.\n\nYou no longer have a channel for **Channel** things, such as name changes.\n\nIf you have a **General Log** set, **Channels Log** will appear there instead (if module is enabled.)`;
                        break;
                    case 'news':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.news == '' || !dbChannels.news) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **News Channel**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.news);
                        dbChannels.news = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **News Channel**.\n\nYou no longer have a channel for **News**, such as joined/left messages by Discord.`;
                        break;
                    case 'welcome':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.welcome == '' || !dbChannels.welcome) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Welcome Channel**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.welcome);
                        dbChannels.welcome = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Welcome Channel**.\n\nYou no longer have a channel for **Welcomes**.`;
                        break;
                    case 'booster':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.booster == '' || !dbChannels.booster) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Booster Channel**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.booster);
                        dbChannels.booster = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Booster Channel**.\n\nYou no longer have a channel for **Booster** things.`;
                        break;
                    case 'announcements':
                        dbChannels = await ServerChannels.findOne({
                            where: { serverID: message.guild.id.toString() },
                        });
                        if(dbChannels.announcement == '' || !dbChannels.announcement) return message.reply(embed
                            .setColor(utils.randomHexColor('warning'))
                            .setDescription('You have not set any channel for **Announcements Channel**.\nPlease set it before resetting.'));
                        deletedChannel = message.guild.channels.find(ch => ch.id == dbChannels.announcement);
                        dbChannels.announcement = '';
                        await dbChannels.save();
                        description = `${deletedChannel} has been removed as the **Announcements Channel**.\n\nYou no longer have a channel for **Announcements**.`;
                        break;
                    default:
                        embed
                            .setColor(utils.randomHexColor('warning'));
                        description = `Please use either: \`${typeMatch.join('`, `')}\`.`;
                        break;
                }
                break;
            case 'list':
                // this should list the channels set and which channel set.
                [dbChannels] = await ServerChannels.findOrCreate({
                    where: { serverID: message.guild.id.toString() },
                    defaults: { serverID: message.guild.id.toString() },
                });
                logGuild = dbChannels.logGuild;
                logMessages = dbChannels.logMessages;
                logMembers = dbChannels.logMembers;
                logRoles = dbChannels.logRoles;
                logEmojis = dbChannels.logEmojis;
                newsCh = dbChannels.news;
                welcomeCh = dbChannels.welcome;
                boosterCh = dbChannels.booster;
                announceCh = dbChannels.announcement;
                if(logGuild == '') logGuild = 'Not set.';
                if(logMessages == '') logMessages = 'Not set. Will use **General Log** if set and module enabled.\n';
                if(logMembers == '') logMembers = 'Not set. Will use **General Log** if set and module enabled.\n';
                if(logRoles == '') logRoles = 'Not set. Will use **General Log** if set and module enabled.\n';
                if(logEmojis == '') logEmojis = 'Not set. Will use **General Log** if set and module enabled.\n';
                if(logChannels == '') logChannels = 'Not set. Will use **General Log** if set and module enabled.\n';
                if(newsCh == '') newsCh = 'Not set.\n';
                if(welcomeCh == '') welcomeCh = 'Not set. Will use **News Channel** if set and module enabled.\n';
                if(boosterCh == '') boosterCh = 'Not set. Will use **News Channel** if set and module enabled.\n';
                if(announceCh == '') announceCh = 'Not set. Will use **News Channel** if set and module enabled.\n';
                description = `**General Log**: ${logGuild}\n**Deleted Messages Log**: ${logMessages}\n**Members Log**: ${logMembers}\n**Roles Log**: ${logRoles}\n**Emojis Log**: ${logEmojis}\n**Channels Log**: ${logChannels}\n**News Channel**: ${newsCh}\n**Welcome Channel**: ${welcomeCh}\n**Booster Channel**: ${boosterCh}\n**Announcements Channel**: ${announceCh}`;
                break;
            default:
                embed
                    .setColor(utils.randomHexColor('warning'));
                description = 'Please use `set`, or `reset` and then mention the **Channel** you wish to set.\nAlternatively you can also use `list` to see a list of set channels.';
                break;
        }
        return message.reply(embed
            .setDescription(description));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};