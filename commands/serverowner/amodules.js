const Discord = require('discord.js');
const botconfig = require('../../config/botconfig.json');
const settings = require('../../config/settings.json');
const utils = require('../../utils');
const { ServerModules } = require('../../database/dbObjects');

const help = {
    name: 'adminmodule',
    description: '',
    aliases: [''],
    usage: '<set | reset | list>',
};
const config = {
    cooldown: 5,
    guildOnly: true,
    args: false,
    admin: false,
    delete: true,
    nsfw: false,
};

async function fox(message, args, client){
    try{
        const embed = new Discord.RichEmbed()
            .setColor(utils.memberColor(message))
            .setTitle('ADMIN MODULES')
            .setTimestamp();
        const what = args[0];
        const what2 = args[1];
        let description, dbModules;
        switch(what){
            case 'set':
                break;
            case 'reset':
                break;
            case 'list':
                [dbModules] = await ServerModules.findOrCreate({
                    where: { serverID: message.guild.id.toString() },
                    default: { serverID: message.guild.id.toString() },
                });

                description = `**General Log Module**: ${dbModules.logGuild}\n\n**Deleted messages Log Module**: ${dbModules.logMessages}\n\n**Members Log Module**: ${dbModules.logMembers}\n\n**Roles Log Module**: ${dbModules.logRoles}\n\n**Emojis Log Module**: ${dbModules.logEmojis}\n\n**Channels Log Module**: ${dbModules.logChannels}\n\n**News Module**: ${dbModules.news}\n\n**Welcome Module**: ${dbModules.welcome}\n\n**Booster Module**: ${dbModules.booster}\n\n**Announcements Module**: ${dbModules.announce}`;
                break;
            default:
                embed
                    .setColor(utils.randomHexColor('warning'));
                description = 'Please use `set`, or `reset` and then mention the **Channel** you wish to set.\nAlternatively you can also use `list` to see a list of set channels.';
                break;
        }
        return message.reply(embed
            .setDescription(description));
    }
    catch(err){
        return utils.errorEmbed(message, err, help.name);
    }
}

module.exports = {
    help, config, fox,
};