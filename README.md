# SERENE
#### A demoness is trying to take over the world!

### CODE WRITING GUIDELINE

This is for everyone in the project, and we should try to follow this guideline so we have some consistency in the code.

**IT IS RECOMMENDED TO DOWNLOAD A LINTER AND USE THE `.eslintrc.json` TAHT IS ALREADY IN THE PROJECT! IT WILL HIGHLIGHT ANY INCONSISTENCIES!**

To install a linter, use `npm i --save-dev eslint` and it should install one automatically for you.

- **Use `const` instead of `let` if something doesn't need changing!**
This is easy to explain. If something isn't changed by something else in the code, use const instead. 

- **If something is an array, prefix it with `arr_`!**
An example would be a list of names. `const arr_names = ['Damian', 'Katrin', 'Soru'];`.

- **ALWAYS use `;`!**

- **Use `' '` for strings!!**

- **Mark any variables that are supposed to be strings, `s_`, much like we did with arrays!**

- **If something is a number and NEEDS to change, use `var` instead of `let`!**
This is self explanatory, becasue `var` stands for `variable` and---.. look, just use `var`. 

- **Try to divide the initial code into groups!**
What does it mean? Well, imagine you have..

`const Member = message.member;
const Author = message.author;
let numba = 19;
let numba1 = 69;`
It can be a bit confusing and terrible to read if you have hundreds of lines and comments. So instead, do something like...
`// === USERS INITIALISERS === //
const Member = message.member;
const Author = message.author;

// === VARIABLES TO BE CHANGED === //
let numba = 19;
let numba2 = 69;`
Because, the power of working together is stronger if everyone can read and INSTANTLY recognise your code. We are not sabotaging a company or anything, so we don't need to produce shit code.

- **Make everything nice and tight**
That means, to not use `if (something > else) {
    ///
};`
Instead, we use: `if(something > else){
    ///
};`
Not a whole lot of change, but really saves on pressing that one extra SPACE a lot of times

- **Remember to comment everything!**
Sugar is guilty of not doing it too, please reprimend him.

- **Please when doing commits, write something that explains what it is about!**
That includes you Sugar. 
It has to be something like... 
"I updated commands, now they use the new variables". For example.

# REFERENCES
- [DiscordJS.Guide](https://discordjs.guide/)
- [Discord.js](https://discord.js.org)